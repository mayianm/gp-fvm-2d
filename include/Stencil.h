/* File: Stencil.h
 * Author: Ian May
 * Purpose: Define all GP-WENO stencils and reconstruction methods
 */

#include <stdlib.h>
#include <stdbool.h>
#include <complex.h>
#include <math.h>

#include "Options.h"

#ifndef STENCIL_H
#define STENCIL_H

/* typedefs for structs defined herein */
typedef struct _p_KernFuncs KernFuncs;
typedef struct _p_GPStencil1D *GPStencil1D;
typedef struct _p_GPStencil2D *GPStencil2D;

/* 1D stencil function prototypes */
void CreateGPStencil1D(int,double,double,double,KernelType,GPStencil1D*);
void GPStencil1DCopy(GPStencil1D,GPStencil1D*);
void DestroyGPStencil1D(GPStencil1D*);
void GPStencil1DEvalPM(GPStencil1D,int,double*restrict,double*,double*);
void GPStencil1DEvalGqPM(GPStencil1D,int,double*restrict,double*,double*);
void GPStencil1DEvalCenter(GPStencil1D,int,double*restrict,double*);
void GPStencil1DEvalAvg(GPStencil1D,int,double*restrict,double*);

/* 2D stencil function prototypes */
void CreateGPStencil2D(int,double,double,double,StenShape,KernelType,GPStencil2D*);
void GPStencil2DCopy(GPStencil2D,GPStencil2D*);
void DestroyGPStencil2D(GPStencil2D*);
void GPStencil2DEvalX(GPStencil2D,int,double*restrict,double*,double*);
void GPStencil2DEvalY(GPStencil2D,int,double*restrict,double*,double*);
void GPStencil2DEvalGqX(GPStencil2D,int,double*restrict,double*,double*,double*,double*);
void GPStencil2DEvalGqY(GPStencil2D,int,double*restrict,double*,double*,double*,double*);

/* Kernel functions all have the same signature, so typedef it */
typedef double complex(*KFunc)(double complex,double,double);

/* Struct definitions */
struct _p_KernFuncs
{
  KFunc covPnt;
  KFunc covInt;
  KFunc smpAv2Pnt;
  KFunc smpAv2Deriv[2];
  KFunc smpAv2SecDeriv[3];
  KFunc smpPnt2Deriv;
  KFunc smpPnt2SecDeriv;
  KFunc smpElec[2];
};

struct _p_GPStencil1D
{
  /* Stencil radius, cells in stencil */
  int rad, nCells, nSub, deg, subSize[4], subStart[4];
  /* GP parameters */
  double l, eps, bpow;
  /* Kernel functions */
  KernFuncs kFuncs;
  /* Smoothness indicator values (WENO) */
  double gamma[4];
  /* Local prediction vectors (WENO) */
  double * restrict pdPlus[4], * restrict pdMinus[4], * restrict pdGqPlus[4], * restrict pdGqMinus[4];
  double * restrict pdCenter[4], * restrict pdDeriv[4], * restrict pdSecDeriv[4];
  double * restrict pdAvg[4], * restrict pdAvDeriv[4], * restrict pdAvSecDeriv[4];
};

struct _p_GPStencil2D
{
  /* Stencil radius, cells in stencil */
  int rad, nCells, nSub, deg;
  /* Stencil as offsets from anchor */
  int *xOff, *yOff;
  /* Substencil indices into global stencil */
  int *subSize, **subIdx;
  /* GP parameters */
  double l, eps, bpow;
  /* Kernel functions */
  KernFuncs kFuncs;
  /* Smoothness indicator values */
  double *gamma;
  /* Prediction vectors */
  double ** restrict pdNorth, ** restrict pdSouth, ** restrict pdEast, ** restrict pdWest;
  double ** restrict pdNorthGq, ** restrict pdSouthGq, ** restrict pdEastGq, ** restrict pdWestGq;
  double ** restrict pdNorthGm, ** restrict pdSouthGm, ** restrict pdEastGm, ** restrict pdWestGm;
  double ** restrict pdDerivX, ** restrict pdDerivY, ** restrict pdDerivXX, ** restrict pdDerivXY, ** restrict pdDerivYY;
};

#endif
