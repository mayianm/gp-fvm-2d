/* File: Riemann.h
 * Author: Ian May
 * Purpose: Define various Riemann solvers for the GP-WENO FVM
 * Notes: All solvers will provide the same interface such that the rest of the code
 *        can operate somewhat independent of the system dimension
 */

#include <stdlib.h>
#include <math.h>

#ifndef RIEMANN_H
#define RIEMANN_H

/* Advection equation */
double ADV_ExactX(double*,double*,double*,double*);
double ADV_ExactY(double*,double*,double*,double*);

/* Euler equations */
double RS_EulerLLFX(double*,double*,double*,double*);
double RS_EulerLLFY(double*,double*,double*,double*);
double RS_EulerHLLX(double*,double*,double*,double*);
double RS_EulerHLLY(double*,double*,double*,double*);
double RS_EulerHLLCX(double*,double*,double*,double*);
double RS_EulerHLLCY(double*,double*,double*,double*);

#endif
