/* File: LapackWrappers.h
 * Author: Ian May
 * Purpose: Provide easy interface to Lapack routines
 */

#ifndef LAPACKWRAPPERS_H
#define LAPACKWRAPPERS_H

extern void dgeev_(char*,char*,int*,double*,int*,double*,double*,double*,int*,double*,int*,double*,int*,int*);
static int c_dgeev(char JVL, char JVR, int N, double *A, int LDA, double *WR, double *WI,
            double *VL, int LDVL, double *VR, int LDVR, double *WORK, int LWORK)
{
  int info=0;
  dgeev_(&JVL, &JVR, &N, A, &LDA, WR, WI, VL, &LDVL, VR, &LDVR, WORK, &LWORK, &info);
  return info;
}

extern void dgesv_(int*,int*,double*,int*,int*,double*,int*,int*);
static int c_dgesv(int N, int NRHS, double *A, int LDA, int *IPIV, double *B, int LDB)
{
  int info=0;
  dgesv_(&N,&NRHS,A,&LDA,IPIV,B,&LDB,&info);
  return info;
}

extern void zsysv_(char*,int*,int*,double*,int*,int*,double*,int*,double*,int*,int*);
static int c_zsysv(char uplo, int N, int NRHS, double *A, int LDA, int *IPIV, double *B,
            int LDB, double *WORK, int LWORK)
{
  int info=0;
  zsysv_(&uplo,&N,&NRHS,A,&LDA,IPIV,B,&LDB,WORK,&LWORK,&info);
  return info;
}

extern void dgemv_(char*,int*,int*,double*,double*,int*,double*,int*,double*,double*,int*);
static void c_dgemv(char trs, int M, int N, double alpha, double *A, int lda, double *x,
             int incx, double beta, double *y, int incy)
{
  dgemv_(&trs,&M,&N,&alpha,A,&lda,x,&incx,&beta,y,&incy);
}

extern void dgemm_(char*,char*,int*,int*,int*,double*,double*,int*,double*,int*,double*,double*,int*);
static void c_dgemm(char trA, char trB, int M, int N, int K, double alpha, double *A, int lda,
                    double *B, int ldb, double beta, double *C, int ldc)
{
  dgemm_(&trA,&trB,&M,&N,&K,&alpha,A,&lda,B,&ldb,&beta,C,&ldc);
}

extern void dsyev_(char*,char*,int*,double*,int*,double*,double*,int*,int*);
static int c_dsyev(char jobz, char uplo, int N, double *A, int LDA, double *W, double *work, int lwork)
{
  int info=0;
  dsyev_(&jobz,&uplo,&N,A,&LDA,W,work,&lwork,&info);
  return info;
}

extern void dgels_(char*,int*,int*,int*,double*,int*,double*,int*,double*,int*,int*);
static int c_dgels(char trs, int M, int N, int NRHS, double *A, int LDA, double *B, int LDB, double *work, int lwork)
{
  int info=0;
  dgels_(&trs, &M, &N, &NRHS, A, &LDA, B, &LDB, work, &lwork, &info);
  return info;
}

#endif
