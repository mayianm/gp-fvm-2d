/* File: Mesh.h
 * Author: Ian May
 * Purpose: Define mesh data structures suitable for GP-WENO FVM methods
 * Notes: The dynamic approach here is overkill for a single uniform grid,
 *        but will allow easier extension to AMR stuff later. At least, it
 *        will enforce the right type of interface.
 */

#include <stdlib.h>
#include <stdbool.h>

#include "Options.h"

#ifndef MESH_H
#define MESH_H

/* Struct typedefs to pass externally */
typedef struct _p_Mesh *Mesh;

/* Function prototypes */
void CreateMesh(int,int,int,
                double,double,double,double,
                BCType,BCType,BCType,BCType,
                BCType*,BCType*,BCType*,BCType*,
                double,double,double,double,Mesh*);
void DestroyMesh(Mesh*);
int MeshLexic(Mesh,int,int);

/* Struct definitions */
struct _p_Mesh
{
  /* cells in x and y directions, num halo layers, number of interior cells, total cells */
  int nX, nY, nHalo, nXT, nInt, nTot;
  /* Upper and lower corners of domain, x and y grid spacing */
  double xLo, xUp, yLo, yUp, dx, dy;
  /* Boundary conditions around box */
  BCType bcWest, bcEast, bcNorth, bcSouth;
  double bcWestTr, bcEastTr, bcNorthTr, bcSouthTr;
  BCType bcWestSub[2], bcEastSub[2], bcNorthSub[2], bcSouthSub[2];
};

#endif
