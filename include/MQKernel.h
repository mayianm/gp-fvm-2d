/* File: MQKernel.h
 * Author: Ian May
 * Purpose:
 */

#include <complex.h>

/* Covariance functions */
/* Kernel for pointwise data */
double complex MQ_pntCov(double complex,double,double);
/* Kernel for 1D cell-averaged or face-averaged data */
double complex MQ_intCov1D(double complex,double,double);
/* Kernel for 2D cell-averaged data */
double complex MQ_intCov2D(double complex,double,double);

/* 1D Sample functions */
/* Sample 1D cell-averaged data onto point wise predictions */
/* By symmetry this also does the pnt -> av conversion */
double complex MQ_sample1DAv2Pnt(double complex,double,double);
/* Sample 1D cell-averaged data onto pointwise first derivative */
double complex MQ_sample1DAv2Deriv(double complex,double,double);
/* Sample 1D cell-averaged data onto pointwise second derivative */
double complex MQ_sample1DAv2SecDeriv(double complex,double,double);
/* Sample 1D pointwise data onto point wise first derivative */
double complex MQ_sample1DPntDeriv(double complex,double,double);
/* Sample 1D pointwise data onto pointwise second derivative */
double complex MQ_sample1DPntSecDeriv(double complex,double,double);

/* 2D sample functions */
/* Sample 2D cell-averages onto pointwise predictions */
double complex MQ_sample2DAv2Pnt(double complex,double,double);
/* Sample 2D cell-averages onto pointwise derivative in x */
double complex MQ_sample2DAv2DerivX(double complex,double,double);
/* Sample 2D cell-averages onto pointwise derivative in y */
double complex MQ_sample2DAv2DerivY(double complex,double,double);
/* Sample 2D cell-averages onto pointwise second derivative in x */
double complex MQ_sample2DAv2SecDerivXX(double complex,double,double);
/* Sample 2D cell-averages onto pointwise second derivative in y */
double complex MQ_sample2DAv2SecDerivYY(double complex,double,double);
/* Sample 2D cell-averages onto pointwise second mixed derivative */
double complex MQ_sample2DAv2SecDerivXY(double complex,double,double);

/* Specialized sample functions */
/* Electric field */
double complex MQ_sampleElecX(double complex,double,double);
double complex MQ_sampleElecY(double complex,double,double);

