/* File: definition.h
 * Author: Ian May
 * Purpose: Define variables by position in the state and auxiliary vectors
 */

/* State (conserved) variables */
#define VAR_DENS 0
#define VAR_MOMX 1
#define VAR_MOMY 2
#define VAR_MOMZ 3
#define VAR_ETOT 4
#define VAR_MAGX 5
#define VAR_MAGY 6
#define VAR_MAGZ 7

/* Auxiliary variables */
#define AUX_VELX 0
#define AUX_VELY 1
#define AUX_VELZ 2
#define AUX_PRES 3
#define AUX_EINT 4
#define AUX_SPSD 5
#define AUX_PRSG 6
#define AUX_PRSB 7
#define AUX_DIVB 8

/* Fluid properties */
#define PROP_GAM 0
