/* File: EOS.h
 * Author: Ian May
 * Purpose: Hold various equation of state relationships between variables
 */

#ifndef EOS_H
#define EOS_H

double igl_pres(double,double*);
double igl_soundspeed(double,double*);
double igl_eint(double*);

#endif
