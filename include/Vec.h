/* File: Vec.h
 * Author: Ian May
 * Purpose: Define vectors supported on cells, faces, and vertices of a given mesh
 * Notes: Like the mesh, these are essentially wrappers of the trivial version,
 *        but having them leaves room for later AMR type complications
 */

#include <stdlib.h>

#include "Mesh.h"

#ifndef VEC_H
#define VEC_H

/* Struct typedefs to pass externally */
typedef struct _p_CellVec *CellVec;   /* Cell centered/averaged values */
typedef struct _p_FaceVecC *FaceVecC; /* Single-valued face values for cont. fcns */
typedef struct _p_FaceVecD *FaceVecD; /* Multi-valued face values for disc. fcns */

/* Function prototypes */
void CreateCellVec(Mesh,int,CellVec*);
void CellVecDuplicate(CellVec,CellVec*);
void DestroyCellVec(CellVec*);
void CellVecPrint(CellVec,int);
void CreateFaceVecC(Mesh,int,FaceVecC*);
void DestroyFaceVecC(FaceVecC*);
void CreateFaceVecD(Mesh,int,FaceVecD*);
void DestroyFaceVecD(FaceVecD*);
void FaceVecDPrint(FaceVecD,int);

/* Struct definitions */
struct _p_CellVec
{
  /* Cells interior to mesh, Cells including the halo */
  int nInt, nTot, nX, nY, nHalo, nC;
  double * restrict v;
};

struct _p_FaceVecC
{
  /* Cells interior to mesh, strip length on sides, number of components */
  int nX, nY, nHalo, nC;
  /* Faces with x and y normals */
  double * restrict vX, * restrict vY;
};

struct _p_FaceVecD
{
  /* Cells interior to mesh, strip length on sides, number of components */
  int nInt, nX, nY, nHalo, nC;
  /* Up, down, right, and left faces of each interior cell with one extra layer */
  double * restrict _p_vN, * restrict vS, * restrict _p_vE, * restrict vW;
  /* Aliases for upper and right faces to allow negative indices */
  double * restrict vN, * restrict vE;
};

static inline int cellLex(CellVec vec, int pX, int pY)
{
  return vec->nC*((pY+vec->nHalo)*(vec->nX+2*vec->nHalo)+pX+vec->nHalo);
}

static inline int faceLexCE(FaceVecC vec, int pX, int pY)
{
  return vec->nC*((pX+vec->nHalo+1)*(vec->nY+2*vec->nHalo) + pY+vec->nHalo);
}
static inline int faceLexCN(FaceVecC vec, int pX, int pY)
{
  return vec->nC*((pY+vec->nHalo+1)*(vec->nX+2*vec->nHalo) + pX+vec->nHalo);
}

static inline int faceLexCW(FaceVecC vec, int pX, int pY)
{
  return faceLexCE(vec,pX-1,pY);
}
static inline int faceLexCS(FaceVecC vec, int pX, int pY)
{
  return faceLexCN(vec,pX,pY-1);
}

static inline int faceLexEW(FaceVecD vec, int pX, int pY)
{
  return vec->nC*(pX*(2*vec->nHalo+vec->nY)+pY+vec->nHalo);
}
static inline int faceLexNS(FaceVecD vec, int pX, int pY)
{
  return vec->nC*(pY*(2*vec->nHalo+vec->nX)+pX+vec->nHalo);
}

#endif
