/* File: Options.h
 * Author: Ian May
 * Purpose: Define struct(s) with all relevant settings for the GP-WENO runs, and
 *          provide means of reading input files/CLI arguments
 */

#include <stdbool.h>

#ifndef OPTIONS_H
#define OPTIONS_H

/* Maximum number of fluid properties */
#define MAXFPROP 5

/* Enums for named settings */
typedef enum {ADVECT,EULER} EQType;
typedef enum {GPDD,GPDIAMOND,GPCIRCLE} StenShape;
typedef enum {GPSE,GPMQ,GPIMQ,GPIQ} KernelType;
typedef enum {PERIODIC,INFLOW,OUTFLOW,REFLECTING,MIXED} BCType;
typedef enum {ADVEX,LLF,HLL,HLLC} RSType;
typedef enum {RK2,RK3,RK4,RK7} TSType;
typedef enum {
  /* Scalar advection */
  SCLADVECT,
  /* Euler */
  GAUSS,TILTGAUSS,ISENVORT,BUCHHELZ,KELVHELM,SOD,SODVERT,SEDOV,NOH,
  SHUOSHER,RICHTMESHLH,RICHTMESHHL,SHOCKVORT,SHOCKVORTWAVE,DOUBLEMACH,
  RIEM3,RIEM4,RIEM5,RIEM6,RIEM8,RIEM11,RIEM12,RIEM13,RIEM17,RIEM3ROT,
  IMPLOSION,RAYLEIGHTAYLOR,SHOCKBUBBLELH,SHOCKBUBBLEHL,ASTROJET,
  /* Catch-all for temporary experiments */
  OTHERIBC
} IBCFunc;

typedef struct 
{
  /* Equation type */
  EQType eqType;
  /* GP settings */
  int gp_rad;
  double gp_lFac;
  double gp_eps, gp_betaPow;
  StenShape gp_shape;
  KernelType gp_kernel;
  /* grid settings */
  int gr_nx, gr_ny;
  double gr_xlo, gr_xup, gr_ylo, gr_yup;
  /* Boundary condition types */
  BCType bc_n, bc_e, bc_s, bc_w;
  /* If above is mixed, these hold the pieces and where they transition */
  double bc_northTr, bc_eastTr, bc_southTr, bc_westTr;
  BCType bc_nSub[2], bc_eSub[2], bc_sSub[2], bc_wSub[2];
  /* Solver settings */
  int so_nComp, so_nProp, so_nStep;
  double so_dt, so_tf, so_cfl, so_fProp[MAXFPROP];
  bool so_fluxRec, so_gaussFlux, so_polyWeno, so_bhFlux;
  RSType so_rsType;
  TSType so_tsType;
  IBCFunc so_ibc;
  double (*so_rsx)(double*,double*,double*,double*);
  double (*so_rsy)(double*,double*,double*,double*);
  void (*so_ccx)(double*,double*,double*,double*);
  void (*so_ccy)(double*,double*,double*,double*);
  void (*so_bcN)(double,double,double,double*,double*);
  void (*so_bcE)(double,double,double,double*,double*);
  void (*so_bcS)(double,double,double,double*,double*);
  void (*so_bcW)(double,double,double,double*,double*);
  void (*so_ic)(double,double,double*,double*);
  void (*so_src)(double,double,double*,double*);
  /* Post-processing settings */
  int pp_verbose;
  bool pp_compLast;
  int pp_plotFreq;
  char pp_fBase[100];
  char pp_convFile[100];
} Options;

void OptionsSetup(char*,int,int,char**,Options*);
void OptionsPrint(Options*);

#endif
