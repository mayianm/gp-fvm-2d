/* File: IBCs.h
 * Author: Ian May
 * Purpose: Hold initial and boundary conditions for the various included problems
 */

#include <stdlib.h>
#include <math.h>

#include "EOS.h"
#include "definition.h"

#ifndef IBCS_H
#define IBCS_H


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Scalar Advection */
void ic_Advect(double x, double y, double *fProp, double *U)
{
  (void) y;
  U[0] = sin(2.*M_PI*fProp[0]*x);
}

/* Gaussian Advection */
void ic_Gauss(double x, double y, double *fProp, double *U)
{
  (void) y;
  U[VAR_DENS] = 1 + exp(-100*(x-0.5)*(x-0.5));
  U[VAR_MOMX] = U[VAR_DENS];
  U[VAR_MOMY] = 0;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]*(fProp[PROP_GAM]-1.)) + U[VAR_DENS]/2.;
}

/* Gaussian Advection along diagonal */
void ic_TiltGauss(double x, double y, double *fProp, double *U)
{
  U[VAR_DENS] = 1 + exp(-50*(x+y)*(x+y)) + exp(-50*(x+y-1.)*(x+y-1.)) + exp(-50*(x+y-2.)*(x+y-2.));
  U[VAR_MOMX] = U[VAR_DENS];
  U[VAR_MOMY] = U[VAR_DENS];
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]*(fProp[PROP_GAM]-1.)) + U[VAR_DENS];
}

/* Isentropic vortex */
static inline double ivOmega(double x, double y, double xc, double yc, double R, double sig, double beta)
{
  return beta*exp(-((x-xc)*(x-xc) + (y-yc)*(y-yc))/(2.*sig*sig*R*R));
}

static inline void ivAddPert(double x, double y, double xc, double yc, double R,
                             double Om, double *fProp, double *Ub)
{
  const double rhoB = Ub[VAR_DENS];
  const double presB = igl_pres(fProp[PROP_GAM],Ub);
  Ub[VAR_DENS] = pow(Ub[VAR_DENS] + (1.-fProp[PROP_GAM])*Om*Om/2., 1./(fProp[PROP_GAM]-1));
  Ub[VAR_MOMX] = Ub[VAR_DENS]*(Ub[VAR_MOMX]/rhoB - (y-yc)*Om/R);
  Ub[VAR_MOMY] = Ub[VAR_DENS]*(Ub[VAR_MOMY]/rhoB + (x-xc)*Om/R);
  Ub[VAR_MOMZ] = 0;
  Ub[VAR_ETOT] = pow(presB + (1.-fProp[PROP_GAM])*Om*Om/2., fProp[PROP_GAM]/(fProp[PROP_GAM]-1))/(fProp[PROP_GAM]*(fProp[PROP_GAM]-1)) + (Ub[VAR_MOMX]*Ub[VAR_MOMX] + Ub[VAR_MOMY]*Ub[VAR_MOMY])/(2.*Ub[VAR_DENS]);
}

void ic_IsenVort(double x, double y, double *fProp, double *U)
{
  /* Background flow */
  U[VAR_DENS] = 1;
  U[VAR_MOMX] = 1; U[VAR_MOMY] = 1; U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 1.;
  /* Perturbation */
  const double Om = ivOmega(x, y, 0., 0., 1., 1., 5.*sqrt(2.)*exp(0.5)/(4.*M_PI));
  ivAddPert(x, y, 0, 0, 1, Om, fProp, U);
}

/* Buchmuller-Helzel */
void ic_BuchHelz(double x, double y, double *fProp, double *U)
{
  U[VAR_DENS] = 1.+sin(M_PI*(x+y))/2.;
  U[VAR_MOMX] = U[VAR_DENS]*cos(M_PI*(x+2.*y));
  U[VAR_MOMY] = U[VAR_DENS]*(1.-sin(M_PI*(2.*x+y))/2.);
  U[VAR_MOMZ] = 0;
  const double p = 1.+sin(M_PI*(x-y))/2.;
  U[VAR_ETOT] = p/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
}

/* Sod problem */
void ic_Sod(double x, double y, double *fProp, double *U)
{
  (void) y;
  /* Background */
  U[VAR_DENS] = 0.125;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 0.1/(fProp[PROP_GAM]-1.);
  if(x<.5) {
    U[VAR_DENS] = 1.;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

void bcw_Sod(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 1.0;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
}

/* Sod problem y-direction */
void ic_SodVert(double x, double y, double *fProp, double *U)
{
  (void) x;
  /* Background */
  U[VAR_DENS] = 0.125;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 0.1/(fProp[PROP_GAM]-1.);
  if(y<.5) {
    U[VAR_DENS] = 1.;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

void bcs_SodVert(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 1.0;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
}

/* Sedov problem */
void ic_Sedov(double x, double y, double *fProp, double *U)
{
  (void) fProp;
  /* Rad is 3dx for dx=1/100 */
  const double dr=0.03, ei=0.0673185/(M_PI*dr*dr);
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = ei/1.e4;
  if(x*x+y*y<dr*dr) {
    U[VAR_ETOT] = ei;
  }
}

/* Shu-Osher problem */
void ic_ShuOsher(double x, double y, double *fProp, double *U)
{
  (void) y;
  if(x<-4.) {
    U[VAR_DENS] = 3.857143;
    U[VAR_MOMX] = 2.629369*U[VAR_DENS];
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 10.33333/(fProp[PROP_GAM]-1.) + 3.857143*2.629369*2.629369/2.;
  } else {
    U[VAR_DENS] = 1. + 0.2*sin(5.*x);
    U[VAR_MOMX] = 0;
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

void bcw_ShuOsher(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 3.857143;
  U[VAR_MOMX] = 2.629369*U[VAR_DENS];
  U[VAR_MOMY] = 0;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 10.33333/(fProp[PROP_GAM]-1.) + 3.857143*2.629369*2.629369/2.;
}

/* Richtmeyer-Meshkov problem, helium shock into argon */
void ic_RichtMeshLH(double x, double y, double *fProp, double *U)
{
  if(x<-0.8) {
    U[VAR_DENS] = 0.536068;
    U[VAR_MOMX] = 5.846477*U[VAR_DENS];
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 10.9984996/(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
  } else {
    U[VAR_DENS] = x<0.05*sin(8.*M_PI*y)-0.4 ? 0.1786 : 1.784;
    U[VAR_MOMX] = 0;
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

/* Richtmeyer-Meshkov problem, argon shock into helium */
void ic_RichtMeshHL(double x, double y, double *fProp, double *U)
{
  if(x<-0.8) {
    U[VAR_DENS] = 5.354678;
    U[VAR_MOMX] = 1.849854*U[VAR_DENS];
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 10.9984996/(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
  } else {
    U[VAR_DENS] = x>0.05*sin(8.*M_PI*y)-0.4 ? 0.1786 : 1.784;
    U[VAR_MOMX] = 0;
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

/* Shock-vortex problem, stationary mach 2 shock */
void ic_ShockVort(double x, double y, double *fProp, double *U)
{
  (void) y;
  if(x<-1.) {
    U[VAR_DENS] = 1.;
    U[VAR_MOMX] = 2.*sqrt(fProp[PROP_GAM]);
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
    /* Add vortex */
    const double Om = ivOmega(x, y, -3., 0., 0.5, 1., 5.*sqrt(2.)*exp(0.5)/(4.*M_PI));
    ivAddPert(x, y, -3., 0, 0.5, Om, fProp, U);
  } else {
    U[VAR_DENS] = 2.66666666;
    U[VAR_MOMX] = 2.*sqrt(fProp[PROP_GAM]);
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 4.5/(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
  }
}

void bcw_ShockVort(double x, double y, double t, double *fProp, double *U)
{
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = 2.*sqrt(fProp[PROP_GAM]);
  U[VAR_MOMY] = 0;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) +  + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
  /* Add vortex */
  const double Om = ivOmega(x, y, -3., 0., 0.5, 1., 5.*sqrt(2.)*exp(0.5)/(4.*M_PI));
  ivAddPert(x, y, 2.*sqrt(fProp[PROP_GAM])*t-3., 0, 0.5, Om, fProp, U);
}

/* Mach 3 shock in helium hitting bubble of argon */
void ic_ShockBubbleLH(double x, double y, double *fProp, double *U)
{
  if(x<0.2) {
    U[VAR_DENS] = 0.537;
    U[VAR_MOMX] = 971.3*U[VAR_DENS];
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1114575./(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
  } else {
    /* Circle */
    U[VAR_DENS] = ((x-0.4)*(x-0.4)+y*y<0.01) ? 1.784 : 0.1786;
    /* Triangle */
    /* U[VAR_DENS] = (y<(x-0.4) && x<0.5) ? 1.784 : 0.1786; */
    U[VAR_MOMX] = 0;
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 101325./(fProp[PROP_GAM]-1.);
  }
}

/* Mach 3 shock in argon hitting bubble of helium */
void ic_ShockBubbleHL(double x, double y, double *fProp, double *U)
{
  if(x<0.2) {
    U[VAR_DENS] = 5.352;
    U[VAR_MOMX] = 307.67*U[VAR_DENS];
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1114575./(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2.*U[VAR_DENS]);
  } else {
    /* Circle */
    U[VAR_DENS] = ((x-0.4)*(x-0.4)+y*y<0.01) ? 0.1786 : 1.784;
    /* Triangle */
    /* U[VAR_DENS] = (y<(x-0.4) && x<0.5) ? 0.1786 : 1.784; */
    U[VAR_MOMX] = 0;
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 101325./(fProp[PROP_GAM]-1.);
  }
}

/* Mach 80 astrophysical jet into ambient medium */
void ic_AstroJet(double x, double y, double *fProp, double *U)
{
  (void) x;
  (void) y;
  U[VAR_DENS] = 5.;
  U[VAR_MOMX] = 0;
  U[VAR_MOMY] = 0;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 0.4127/(fProp[PROP_GAM]-1.);
}

void bcw_AstroJet(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) t;
  double YYYY = (y-0.5)*(y-0.5)*(y-0.5)*(y-0.5);
  U[VAR_DENS] = 5.;
  U[VAR_MOMX] = 10.*exp(-1.e6*YYYY);
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = 0.4127/(fProp[PROP_GAM]-1.) + U[VAR_MOMX]*U[VAR_MOMX]/(2*U[VAR_DENS]);
}

/* Double mach reflection */
void ic_DoubleMach(double x, double y, double *fProp, double *U)
{
  (void) y;
  (void) fProp;
  const double r3 = sqrt(3.);
  if(y>r3*(x-1./6.)) {
    U[VAR_DENS] = 8.;
    U[VAR_MOMX] = 8.*7.1447096;
    U[VAR_MOMY] = -8.*4.125;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 116.5/(fProp[PROP_GAM]-1.) + 4.*(7.1447096*7.1447096 + 4.125*4.125);
  } else {
    U[VAR_DENS] = 1.4;
    U[VAR_MOMX] = 0.;
    U[VAR_MOMY] = 0.;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

void bcn_DoubleMach(double x, double y, double t, double *fProp, double *U)
{
  (void) y;
  if(x<8.66025*fProp[PROP_GAM]*t+0.744017) {
    U[VAR_DENS] = 8.;
    U[VAR_MOMX] = 8.*7.1447096;
    U[VAR_MOMY] = -8.*4.125;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 116.5/(fProp[PROP_GAM]-1.) + 4.*(7.1447096*7.1447096 + 4.125*4.125);
  } else {
    U[VAR_DENS] = 1.4;
    U[VAR_MOMX] = 0;
    U[VAR_MOMY] = 0;
    U[VAR_MOMZ] = 0;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

void bcw_DoubleMach(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 8.;
  U[VAR_MOMX] = 8.*7.1447096;
  U[VAR_MOMY] = -8.*4.125;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 116.5/(fProp[PROP_GAM]-1.) + 4.*(7.1447096*7.1447096 + 4.125*4.125);
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem3(double x, double y, double *fProp, double *U)
{
  if(y>0.8) {
    if(x>0.8) {
      U[VAR_DENS] = 1.5;
      U[VAR_MOMX] = 0.;
      U[VAR_MOMY] = 0.;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1.5/(fProp[PROP_GAM]-1.);
    } else {
      U[VAR_DENS] = 33./62.;
      U[VAR_MOMX] = (33./62.)*(4./sqrt(11.));
      U[VAR_MOMY] = 0.;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.3/(fProp[PROP_GAM]-1.) + (33./124.)*(16./11.);
    }
  } else {
    if(x>0.8) {
      U[VAR_DENS] = 33./62.;
      U[VAR_MOMX] = 0.;
      U[VAR_MOMY] = (33./62.)*(4./sqrt(11.));
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.3/(fProp[PROP_GAM]-1.) + (33./124.)*(16./11.);
    } else {
      U[VAR_DENS] = 77./558.;
      U[VAR_MOMX] = (77./558.)*(4./sqrt(11.));
      U[VAR_MOMY] = (77./558.)*(4./sqrt(11.));
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = (9./310.)/(fProp[PROP_GAM]-1.) + (77./558.)*(16./11.);
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem4(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 1.1;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1.1/(fProp[PROP_GAM]-1.);
    } else {
      U[VAR_DENS] = 0.5065;
      U[VAR_MOMX] = 0.5065*0.8939;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.35/(fProp[PROP_GAM]-1.) + 0.5065*0.8939*0.8939/2.;
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 0.5065;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 0.5065*0.8939;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.35/(fProp[PROP_GAM]-1.) + 0.5065*0.8939*0.8939/2.;
    } else {
      U[VAR_DENS] = 1.1;
      U[VAR_MOMX] = 1.1*0.8939;
      U[VAR_MOMY] = 1.1*0.8939;
      U[VAR_MOMZ] = 0.;
      U[VAR_ETOT] = 1.1/(fProp[PROP_GAM]-1.) + 1.1*0.8939*0.8939;
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem5(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = -0.75;
      U[VAR_MOMY] = -0.5;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5)/2.;
    } else {
      U[VAR_DENS] = 2.;
      U[VAR_MOMX] = -0.75;
      U[VAR_MOMY] = 0.5;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5);
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 2.;
      U[VAR_MOMX] = 0.75;
      U[VAR_MOMY] = -0.5;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5);
    } else {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0.75;
      U[VAR_MOMY] = 0.5;
      U[VAR_MOMZ] = 0.;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5)/2.;
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem6(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0.75;
      U[VAR_MOMY] = -0.5;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5)/2.;
    } else {
      U[VAR_DENS] = 2.;
      U[VAR_MOMX] = 0.75;
      U[VAR_MOMY] = 0.5;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5);
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 2.;
      U[VAR_MOMX] = -0.75;
      U[VAR_MOMY] = -0.5;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5);
    } else {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = -0.75;
      U[VAR_MOMY] = 0.5;
      U[VAR_MOMZ] = 0.;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.75*0.75+0.5*0.5)/2.;
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem8(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 0.5197;
      U[VAR_MOMX] = 0.5197*0.1;
      U[VAR_MOMY] = 0.5197*0.1;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 0.5197*0.1*0.1;
    } else {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = -0.6259;
      U[VAR_MOMY] = 0.1;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.1*0.1+0.6259*0.6259)/2.;
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0.1;
      U[VAR_MOMY] = -0.6259;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (0.1*0.1+0.6259*0.6259)/2.;
    } else {
      U[VAR_DENS] = 0.8;
      U[VAR_MOMX] = 0.08;
      U[VAR_MOMY] = 0.08;
      U[VAR_MOMZ] = 0.;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.8*0.1*0.1;
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem11(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0.1;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.1*0.1/2.;
    } else {
      U[VAR_DENS] = 0.5313;
      U[VAR_MOMX] = 0.5313*0.8276;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 0.5313*0.8276*0.8276/2.;
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 0.5313;
      U[VAR_MOMX] = 0.5313*0.1;
      U[VAR_MOMY] = 0.5313*0.7276;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 0.5313*(0.1*0.1+0.7276*0.7276)/2.;
    } else {
      U[VAR_DENS] = 0.8;
      U[VAR_MOMX] = 0.08;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 0.8*0.1*0.1/2.;
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem12(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 0.5313;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.);
    } else {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0.7276;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.7276*0.7276/2.;
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 0.7276;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.7276*0.7276/2.;
    } else {
      U[VAR_DENS] = 0.8;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem13(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = -0.3;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.3*0.3/2.;
    } else {
      U[VAR_DENS] = 2.;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 2.*0.3;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.3*0.3;
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 0.5313;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 0.4276;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 0.5313*0.4276*0.4276/2.;
    } else {
      U[VAR_DENS] = 1.0625;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 1.0625*0.8145;
      U[VAR_MOMZ] = 0.;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 1.0625*0.8145*0.8145/2.;
    }
  }
}

/* third configuration of the standard 2D riemann problems */
void ic_Riem17(double x, double y, double *fProp, double *U)
{
  if(y>0.5) {
    if(x>0.5) {
      U[VAR_DENS] = 1.;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = -0.4;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.4*0.4/2.;
    } else {
      U[VAR_DENS] = 2.;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = -0.6;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 0.3*0.3;
    }
  } else {
    if(x>0.5) {
      U[VAR_DENS] = 0.5197;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = -0.5197*1.1259;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 0.5197*1.1259*1.1259/2.;
    } else {
      U[VAR_DENS] = 1.0625;
      U[VAR_MOMX] = 0;
      U[VAR_MOMY] = 1.0625*0.2145;
      U[VAR_MOMZ] = 0.;
      U[VAR_ETOT] = 0.4/(fProp[PROP_GAM]-1.) + 1.0625*0.2145*0.2145/2.;
    }
  }
}

/* third configuration 2D Riemann rotated */
void ic_Riem3Rot(double x, double y, double *fProp, double *U)
{
  if(y>-x) {
    if(y<x) {
      U[VAR_DENS] = 1.5;
      U[VAR_MOMX] = 0.;
      U[VAR_MOMY] = 0.;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 1.5/(fProp[PROP_GAM]-1.);
    } else {
      U[VAR_DENS] = 0.5323;
      U[VAR_MOMX] = 1.206*0.5323*sqrt(2.)/2.;
      U[VAR_MOMY] = -U[VAR_MOMX];
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.3/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
    }
  } else {
    if(y<x) {
      U[VAR_DENS] = 0.5323;
      U[VAR_MOMX] = 1.206*0.5323*sqrt(2.)/2.;
      U[VAR_MOMY] = U[VAR_MOMX];
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.3/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
    } else {
      U[VAR_DENS] = 0.138;
      U[VAR_MOMX] = 1.206*0.138*sqrt(2.);
      U[VAR_MOMY] = 0;
      U[VAR_MOMZ] = 0;
      U[VAR_ETOT] = 0.029/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
    }
  }
}

/* Kelvin-Helmholtz instability test */
void ic_KelvHelm(double x, double y, double *fProp, double *U)
{
  const double rhoF = 1.;
  const double p = 10;
  const double uf = 1.;
  const double a=0.05, sig=0.2, A=0.01;
  const double y1=0.5, y2=1.5;
  
  U[VAR_DENS] = 1. + rhoF*(tanh((y-y1)/a) - tanh((y-y2)/a))/2.;
  U[VAR_MOMX] = U[VAR_DENS]*uf*(tanh((y-y1)/a) - tanh((y-y2)/a) - 1);
  U[VAR_MOMY] = U[VAR_DENS]*A*sin(2.*M_PI*x)*(exp(-(y-y1)*(y-y1)/(sig*sig)) + exp(-(y-y2)*(y-y2)/(sig*sig)));
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = p/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
}

/* Implosion symmetry test */
static void implosion(double x, double y, double *fProp, double *U)
{
  const double l1rad = 0.15;
  double l1 = fabs(x)+fabs(y);
  
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0.;
  if(l1 < l1rad) {
    U[VAR_DENS] = 0.125;
    U[VAR_ETOT] = 0.14/(fProp[PROP_GAM]-1.);
  } else {
    U[VAR_DENS] = 1.;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
  }
}

void ic_Implosion(double x, double y, double *fProp, double *U)
{
  double uSym[8][5];
  implosion(x,y,fProp,uSym[0]);
  implosion(-x,y,fProp,uSym[1]);
  implosion(x,-y,fProp,uSym[2]);
  implosion(-x,-y,fProp,uSym[3]);
  implosion(y,x,fProp,uSym[4]);
  implosion(y,-x,fProp,uSym[5]);
  implosion(-y,x,fProp,uSym[6]);
  implosion(-y,-x,fProp,uSym[7]);
  for(int nC=0; nC<5; nC++) {
    U[nC] = 0;
    for(int nS=0; nS<8; nS++) {
      U[nC] += uSym[nS][nC];
    }
    U[nC] /= 8.;
  }
}
  

/* Rayleigh-Taylor instability */
void ic_RayleighTaylor(double x, double y, double *fProp, double *U)
{
  U[VAR_MOMX] = 0.;
  U[VAR_MOMZ] = 0.;
  if(y <= 0.5) {
    double c = sqrt(fProp[0]*(2.*y+1.)/2.);
    U[VAR_DENS] = 2.;
    if(x < 0.125) {
      U[VAR_MOMY] = U[VAR_DENS]*(-0.025*c*cos(8.*M_PI*x));
    } else {
      U[VAR_MOMY] = U[VAR_DENS]*(-0.025*c*cos(8.*M_PI*(0.25-x)));
    }
    U[VAR_ETOT] = (2.*y+1.)/(fProp[PROP_GAM]-1.) + (U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
  } else {
    double c = sqrt(fProp[0]*(y+1.5));
    U[VAR_DENS] = 1.;
    if(x < 0.125) {
      U[VAR_MOMY] = U[VAR_DENS]*(-0.025*c*cos(8.*M_PI*x));
    } else {
      U[VAR_MOMY] = U[VAR_DENS]*(-0.025*c*cos(8.*M_PI*(0.25-x)));
    }
    U[VAR_ETOT] = (y+1.5)/(fProp[PROP_GAM]-1.) + (U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
  }
}

void bcn_RayleighTaylor(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  (void) U;
  
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = 2.5/(fProp[PROP_GAM]-1.);
}

void bcs_RayleighTaylor(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  (void) U;
  
  U[VAR_DENS] = 2.;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.);
}

void src_RayleighTaylor(double x, double y, double *U, double *S)
{
  (void) x;
  (void) y;
  S[VAR_MOMY] = U[VAR_DENS];
  S[VAR_ETOT] = U[VAR_MOMY];
}

/* Noh problem */
void ic_Noh(double x, double y, double *fProp, double *U)
{
  double R = sqrt(x*x+y*y);
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = isnan(x/R) ? 0 : -x/R;
  U[VAR_MOMY] = isnan(y/R) ? 0 : -y/R;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = (1.e-6)/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY])/2.;
}

void bcn_Noh(double x, double y, double t, double *fProp, double *U)
{
  (void) t;
  double R = sqrt(x*x+y*y);
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = isnan(x/R) ? 0 : -x/R;
  U[VAR_MOMY] = isnan(y/R) ? 0 : -y/R;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = (1.e-6)/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY])/2.;
}

void bce_Noh(double x, double y, double t, double *fProp, double *U)
{
  (void) t;
  double R = sqrt(x*x+y*y);
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = isnan(x/R) ? 0 : -x/R;
  U[VAR_MOMY] = isnan(y/R) ? 0 : -y/R;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = (1.e-6)/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY])/2.;
}

/* Noh problem */
void ic_ShockVortWave(double x, double y, double *fProp, double *U)
{
  const double cr=sqrt(fProp[PROP_GAM]), pr=1., thr=M_PI/6., kr=2.*M_PI;
  U[VAR_MOMZ] = 0.;
  if(x<-1.) {
    U[VAR_DENS] = 5.56521739;
    U[VAR_MOMX] = 3.08163664;
    U[VAR_MOMY] = 0.;
    U[VAR_ETOT] = 74.5/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX])/2.;
  } else {
    U[VAR_DENS] = 1.;
    U[VAR_MOMX] = -(cr/pr)*sin(thr)*cos(x*kr*cos(thr)+y*kr*sin(thr));
    U[VAR_MOMY] = (cr/pr)*cos(thr)*cos(x*kr*cos(thr)+y*kr*sin(thr));
    U[VAR_ETOT] = pr/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY])/2.;
  }
}

void bcw_ShockVortWave(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 5.56521739;
  U[VAR_MOMX] = 3.08163664;
  U[VAR_MOMY] = 0.;
  U[VAR_ETOT] = 74.5/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX])/2.;
}

void bce_ShockVortWave(double x, double y, double t, double *fProp, double *U)
{
  (void) t;
  const double cr=sqrt(fProp[PROP_GAM]), pr=1., thr=M_PI/6., kr=2.*M_PI;
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = -(cr/pr)*sin(thr)*cos(x*kr*cos(thr)+y*kr*sin(thr));
  U[VAR_MOMY] = (cr/pr)*cos(thr)*cos(x*kr*cos(thr)+y*kr*sin(thr));
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = pr/(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY])/2.;
}

/* MHD Problems */
/* Brio-Wu shock tube problem */
void ic_brioWu(double x, double y, double *fProp, double *U)
{
  (void) y;
  /* Background */
  U[VAR_DENS] = 0.125;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0.;
  U[VAR_ETOT] = 0.1/(fProp[PROP_GAM]-1.) + 25./32.;
  U[VAR_MAGX] = 0.75;
  U[VAR_MAGY] = -1.;
  U[VAR_MAGZ] = 0.;
  if(x<.5) {
    U[VAR_DENS] = 1.;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 25./32.;
    U[VAR_MAGY] = 1.;
  }
}
void ic_brioWuVert(double x, double y, double *fProp, double *U)
{
  (void) x;
  /* Background */
  U[VAR_DENS] = 0.125;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 0.1/(fProp[PROP_GAM]-1.) + 25./32.;
  U[VAR_MAGX] = -1.;
  U[VAR_MAGY] = 0.75;
  U[VAR_MAGZ] = 0.;
  if(y<.5) {
    U[VAR_DENS] = 1.;
    U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 25./32.;
    U[VAR_MAGX] = 1.;
  }
}

void bcw_brioWu(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 25./32.;
  U[VAR_MAGX] = 0.75;
  U[VAR_MAGY] = 1.;
  U[VAR_MAGZ] = 0.;
}

/* Brio-Wu shock tube problem, y-direction */
void bcs_brioWuVert(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = 0.;
  U[VAR_MOMY] = 0.;
  U[VAR_MOMZ] = 0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + 25./32.;
  U[VAR_MAGX] = 1.;
  U[VAR_MAGY] = 0.75;
  U[VAR_MAGZ] = 0.;
}

void ic_mhdVortex(double x, double y, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) fProp;
  (void) U;
}

void ic_orszTang(double x, double y, double *fProp, double *U)
{
  const double D0 = 1., V0=1., pG=1./fProp[0];
  const double B0 = pG;
  U[VAR_DENS] = D0;
  U[VAR_MOMX] = -D0*V0*sin(2.*M_PI*y);
  U[VAR_MOMY] = D0*V0*sin(2.*M_PI*x);
  U[VAR_MOMZ] = 0.;
  U[VAR_MAGX] = -B0*sin(2.*M_PI*y);
  U[VAR_MAGY] = B0*sin(4.*M_PI*x);
  U[VAR_MAGZ] = 0.;
  U[VAR_ETOT] = pG/(fProp[0]-1.) + (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY])/(2.*D0) + (U[VAR_MAGX]*U[VAR_MAGX]+U[VAR_MAGY]*U[VAR_MAGY])/2.;
}

void ic_rotor(double x, double y, double *fProp, double *U)
{
  const double r0=0.1, r1=0.13, pG=1., u0=2.0;
  const double r = sqrt(x*x + y*y);
  const double fr = (r1-r)/(r1-r0);
  if(r<=r0) {
    U[VAR_DENS] = 10.;
    U[VAR_MOMX] = -U[VAR_DENS]*(fr*u0*y)/r0;
    U[VAR_MOMY] = U[VAR_DENS]*(fr*u0*x)/r0;
  } else if(r<r1) {
    U[VAR_DENS] = 1. + 9.*fr;
    U[VAR_MOMX] = -U[VAR_DENS]*(fr*u0*y)/r;
    U[VAR_MOMY] = U[VAR_DENS]*(fr*u0*x)/r;
  } else {
    U[VAR_DENS] = 1.;
    U[VAR_MOMX] = 0.;
    U[VAR_MOMY] = 0.;
  }
  U[VAR_MOMZ] = 0.;
  U[VAR_MAGX] = 5./(2.*sqrt(M_PI));
  U[VAR_MAGY] = 0.;
  U[VAR_MAGZ] = 0.;
  U[VAR_ETOT] = pG/(fProp[0]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]) +
    (U[VAR_MAGX]*U[VAR_MAGX]+U[VAR_MAGY]*U[VAR_MAGY]+U[VAR_MAGZ]*U[VAR_MAGZ])/2.;
}

void ic_fieldLoop(double x, double y, double *fProp, double *U)
{
  const double u0=sqrt(5.), theta=atan(0.5), R=0.3, pG=1., A0=0.001;
  const double r = sqrt(x*x + y*y);
  U[VAR_DENS] = 1.;
  U[VAR_MOMX] = u0*cos(theta);
  U[VAR_MOMY] = u0*sin(theta);
  U[VAR_MOMZ] = 0.;

  U[VAR_MAGX] = 0.;
  U[VAR_MAGY] = 0.;
  U[VAR_MAGZ] = 0.;
  if(r<R) {
    U[VAR_MAGX] = y*A0/r;
    U[VAR_MAGY] = -x*A0/r;
  }

  U[VAR_ETOT] = pG/(fProp[0]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]) + (U[VAR_MAGX]*U[VAR_MAGX] + U[VAR_MAGY]*U[VAR_MAGY])/2.;
}


void ic_circAlfven(double x, double y, double *fProp, double *U)
{
  const double th = atan(2.);
  double xi=x*cos(th)+y*sin(th); // eta=-x*sin(th)+y*cos(th);
  U[VAR_DENS] = 1.0;

  double uXi = 0.0;
  double uEta = 0.1*sin(2*M_PI*xi);
  
  U[VAR_MOMX] = uXi*cos(th) - uEta*sin(th);
  U[VAR_MOMY] = uXi*sin(th) + uEta*cos(th);
  U[VAR_MOMZ] = 0.1*cos(2*M_PI*xi);

  double bXi = 1.0;
  double bEta = 0.1*sin(2*M_PI*xi);
  
  U[VAR_MAGX] = bXi*cos(th) - bEta*sin(th);
  U[VAR_MAGY] = bXi*sin(th) + bEta*cos(th);
  U[VAR_MAGZ] = 0.1*cos(2*M_PI*xi);

  U[VAR_ETOT] = 0.1/(fProp[0]-1.) +
    (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY] + U[VAR_MOMZ]*U[VAR_MOMZ])/(2.*U[VAR_DENS]) +
    (U[VAR_MAGX]*U[VAR_MAGX] + U[VAR_MAGY]*U[VAR_MAGY] + U[VAR_MAGZ]*U[VAR_MAGZ])/2.;
}

/* Kelvin-Helmholtz instability test */
void ic_KelvHelmMag(double x, double y, double *fProp, double *U)
{
  const double rhoF = 1.;
  const double p = 10;
  const double uf = 1.;
  const double a=0.05, sig=0.2, A=0.01;
  const double y1=0.5, y2=1.5;
  
  U[VAR_DENS] = 1. + rhoF*(tanh((y-y1)/a) - tanh((y-y2)/a))/2.;
  U[VAR_MOMX] = U[VAR_DENS]*uf*(tanh((y-y1)/a) - tanh((y-y2)/a) - 1);
  U[VAR_MOMY] = U[VAR_DENS]*A*sin(2.*M_PI*x)*(exp(-(y-y1)*(y-y1)/(sig*sig)) + exp(-(y-y2)*(y-y2)/(sig*sig)));
  U[VAR_MOMZ] = 0;

  U[VAR_MAGX] = 0.01;
  U[VAR_MAGY] = 0.;
  U[VAR_MAGZ] = 0.;
  
  U[VAR_ETOT] = p/(fProp[PROP_GAM]-1.) +
    (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]) +
    (U[VAR_MAGX]*U[VAR_MAGX] + U[VAR_MAGY]*U[VAR_MAGY] + U[VAR_MAGZ]*U[VAR_MAGZ])/2.;
}

/* Magnetorotational instability */
/* !! Needs to be updated to non-pi units */
/* void ic_magRot(double x, double y, double *fProp, double *U) */
/* { */
/*   (void) y; */
/*   const double R=100., GM=1., p0=1.e-5, beta=4000.; */
/*   const double By = sqrt(8.*M_PI*p0/beta); */
/*   const double rc=(R+x)*(R+x)*(R+x), om=sqrt(GM/rc); */
/*   static bool first = true; */
/*   if(first) { */
/*     first = false; */
/*     srand(98273); */
/*   } */
/*   double rnd = ((double) rand())/((double) RAND_MAX); */
/*   rnd = 2*rnd - 1.; */

/*   U[VAR_DENS] = 1.; */
/*   U[VAR_MOMX] = 0.; */
/*   U[VAR_MOMY] = 0.; */
/*   U[VAR_MOMZ] = 1.5*om*x; */
/*   U[VAR_MAGX] = 0; */
/*   U[VAR_MAGY] = (-0.2<x && x<0.2) ? By : 0.; */
/*   U[VAR_MAGZ] = 0.;//By/10.; */
/*   U[VAR_ETOT] = (p0+1.e-8*rnd)/(fProp[0]-1.) + */
/*     (U[VAR_MOMX]*U[VAR_MOMX] + U[VAR_MOMY]*U[VAR_MOMY] + U[VAR_MOMZ]*U[VAR_MOMZ])/(2.*U[VAR_DENS]) + */
/*     (U[VAR_MAGX]*U[VAR_MAGX] + U[VAR_MAGY]*U[VAR_MAGY] + U[VAR_MAGZ]*U[VAR_MAGZ])/(8.*M_PI); */
/* } */

/* void src_magRot(double x, double y, double *U, double *S) */
/* { */
/*   (void) y; */
/*   const double R=100., GM=1.; */
/*   const double rc=(R+x)*(R+x)*(R+x), om=sqrt(GM/rc); */
/*   S[VAR_MOMX] = 2.*om*(1.5*U[VAR_DENS]*om*x - U[VAR_MOMZ]); */
/*   S[VAR_MOMZ] = 2.*om*U[VAR_MOMX]; */
/*   S[VAR_MAGZ] = 1.5*om*U[VAR_MAGX]; */
/* } */

/* Other, catchall for experiments not worth naming */
/* Currently has tilted Kelvin-Helmholtz */
void ic_Other(double x, double y, double *fProp, double *U)
{
  const double densLo = 0.1;
  const double vRef = 1.;
  const double th = atan(2.);
  double xi=x*cos(th)+y*sin(th); //, eta=-x*sin(th)+y*cos(th);
  double uXi = 0.0;
  double uEta = vRef*(sin(2.*M_PI*xi) + sin(6.*M_PI*xi)/3. + sin(10.*M_PI*xi)/5.);
  static bool first = true;
  if(first) {
    first = false;
    srand(98273);
  }
  U[VAR_DENS] = sin(2.*M_PI*xi)>0. ? 1.0 : densLo;
  //U[VAR_DENS] += (densLo/2.)*((double) rand())/((double) RAND_MAX);
  U[VAR_MOMX] = U[VAR_DENS]*(uXi*cos(th)-uEta*sin(th));
  U[VAR_MOMY] = U[VAR_DENS]*(uXi*sin(th)+uEta*cos(th));
  U[VAR_MOMZ] = 0.0;
  U[VAR_ETOT] = 1./(fProp[PROP_GAM]-1.) + (U[VAR_MOMX]*U[VAR_MOMX] +U[VAR_MOMY]*U[VAR_MOMY])/(2.*U[VAR_DENS]);
}

void bcn_Other(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  (void) fProp;
  (void) U;
}

void bce_Other(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  (void) fProp;
  (void) U;
}

void bcs_Other(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  (void) fProp;
  (void) U;
}

void bcw_Other(double x, double y, double t, double *fProp, double *U)
{
  (void) x;
  (void) y;
  (void) t;
  (void) fProp;
  (void) U;
}

#endif
