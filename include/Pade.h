/* File: Pade.h
 * Author: Ian May
 * Purpose: Find prediction vectors given stencil layout, covariance
 *          function, and sample function
 */

void findPredictor(int,double,double,double,double[*],double[*],double*,
                   double complex(*)(double complex,double,double),
                   double complex(*)(double complex,double,double),
                   const char*);
