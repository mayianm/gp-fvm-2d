/* File: Solver.h
 * Author: Ian May
 * Purpose: Combine mesh, stencil, and riemann solvers to evolve a solution through
 *          time.
 */

#include <stdlib.h>
#include <stdio.h>

#include "Options.h"
#include "Vec.h"
#include "Mesh.h"
#include "Stencil.h"

#ifndef SOLVER_H
#define SOLVER_H

/* Struct typedefs to pass externally */
typedef struct _p_GPSolver *GPSolver;

/* Function prototypes */
void CreateGPSolver(Mesh,EQType,double,double,int,double*,
                    bool,bool,bool,bool,GPStencil1D,GPStencil2D,
                    double(*)(double*,double*,double*,double*),
                    double(*)(double*,double*,double*,double*),
                    void(*)(double*,double*,double*,double*),
                    void(*)(double*,double*,double*,double*),
                    void(*)(double,double,double*,double*),
                    TSType,GPSolver*);
void DestroyGPSolver(GPSolver*);
void GPSolverSetIC(GPSolver,void(*)(double,double,double*,double*));
void GPSolverSetNorthBC(GPSolver,void(*)(double,double,double,double*,double*));
void GPSolverSetSouthBC(GPSolver,void(*)(double,double,double,double*,double*));
void GPSolverSetEastBC(GPSolver,void(*)(double,double,double,double*,double*));
void GPSolverSetWestBC(GPSolver,void(*)(double,double,double,double*,double*));
bool GPSolverRK2Step(GPSolver,double);
bool GPSolverRK3Step(GPSolver,double);
bool GPSolverRK4Step(GPSolver,double);
bool GPSolverRK7Step(GPSolver,double);

/* Struct definitions */
struct _p_GPSolver
{
  EQType eqType;
  int nC, nAux, maxThreads;
  double dt, t, tF, h, *fProp;
  Mesh mesh;
  bool dimByDim, fluxRec, gaussFlux, polyWeno, bhFlux;
  GPStencil1D sten1D;
  GPStencil2D sten2D;
  /* Riemann solvers */
  double (*riemX)(double*,double*,double*,double*);
  double (*riemY)(double*,double*,double*,double*);
  /* Characteristic projection */
  void (*charSysX)(double*,double*,double*,double*);
  void (*charSysY)(double*,double*,double*,double*);
  void (*srcFunc)(double,double,double*,double*);
  /* Boundary conditions */
  void (*northBC)(double,double,double,double*,double*);
  void (*southBC)(double,double,double,double*,double*);
  void (*eastBC)(double,double,double,double*,double*);
  void (*westBC)(double,double,double,double*,double*);
  CellVec U, auxVar, K, K1, K2, K3, K4, K7s[11], Kbody;
  /* Fpnt and Ufp become Gauss quad plus when needed */
  FaceVecC Favg, Fpnt, FpntGm;
  FaceVecD Ufa, Ufp, UfpGm;
};

#endif
