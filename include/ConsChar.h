/* File: ConsChar.h
 * Author: Ian May
 * Purpose: Provide conversion between conservative and characteristic variables
 */

#ifndef CONSCHAR_H
#define CONSCHAR_H

void CC_AdvectX(double*,double*,double*,double*);
void CC_AdvectY(double*,double*,double*,double*);
void CC_EulerX(double*,double*,double*,double*);
void CC_EulerY(double*,double*,double*,double*);

#endif
