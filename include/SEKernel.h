/* File: SEKernel.h
 * Author: Ian May
 * Purpose:
 */

#include <stdio.h>
#include <complex.h>

#include <gsl/gsl_fft_halfcomplex.h>
#include <cerf.h>

#include "Options.h"

/* Covariance functions */
/* Kernel for pointwise data */
double complex SE_pntCov(double complex,double,double);
/* Kernel for 1D cell-averaged or face-averaged data */
double complex SE_intCov1D(double complex,double,double);
/* Kernel for 2D cell-averaged data */
double complex SE_intCov2D(double complex,double,double);

/* 1D Sample functions */
/* Sample 1D cell-averaged data onto point wise predictions */
/* By symmetry this also does the pnt -> av conversion */
double complex SE_sample1DAv2Pnt(double complex,double,double);
/* Sample 1D cell-averaged data onto pointwise first derivative */
double complex SE_sample1DAv2Deriv(double complex,double,double);
/* Sample 1D cell-averaged data onto pointwise second derivative */
double complex SE_sample1DAv2SecDeriv(double complex,double,double);
/* Sample 1D pointwise data onto point wise first derivative */
double complex SE_sample1DPntDeriv(double complex,double,double);
/* Sample 1D pointwise data onto pointwise second derivative */
double complex SE_sample1DPntSecDeriv(double complex,double,double);

/* 2D sample functions, separability of SE kernel makes these simple */
/* Sample 2D cell-averages onto pointwise predictions */
double complex SE_sample2DAv2Pnt(double complex,double,double);
/* Sample 2D cell-averages onto pointwise derivative in x */
double complex SE_sample2DAv2DerivX(double complex,double,double);
/* Sample 2D cell-averages onto pointwise derivative in y */
double complex SE_sample2DAv2DerivY(double complex,double,double);
/* Sample 2D cell-averages onto pointwise second derivative in x */
double complex SE_sample2DAv2SecDerivXX(double complex,double,double);
/* Sample 2D cell-averages onto pointwise second derivative in y */
double complex SE_sample2DAv2SecDerivYY(double complex,double,double);
/* Sample 2D cell-averages onto pointwise second mixed derivative */
double complex SE_sample2DAv2SecDerivXY(double complex,double,double);

/* Specialized sample functions */
/* Electric field */
double complex SE_sampleElecX(double complex,double,double);
double complex SE_sampleElecY(double complex,double,double);

