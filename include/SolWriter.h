/* File: SolWriter.h
 * Author: Ian May
 * Purpose: Open and write HDF5 files throughout the run
 */

#include <stdlib.h>

#include <hdf5.h>

#include "Mesh.h"
#include "Vec.h"

#ifndef SOLWRITER_H
#define SOLWRITER_H

/* Struct typedefs to pass externally */
typedef struct _p_SolWriter *SolWriter;

/* Function prototypes */
void CreateSolWriter(Mesh,int,int,const char*,const char**,const char**,SolWriter*);
void DestroySolWriter(SolWriter*);
void SolWriterOutput(SolWriter,Mesh,CellVec,CellVec,int,double);

/* Struct definitions */
struct _p_SolWriter
{
  /* File and variable names */
  char baseName[100], **varNames, **auxNames;
  /* Coordinates and solution info */
  int nComps, nAux, nCoords[2];
  hsize_t nVars[2], nDims;
  double extent[4];
  double *grX, *grY, *solTmp;  
};

#endif
