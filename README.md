# Dependencies
* gsl
* hdf5
* libcerf
* LAPACK/BLAS
* openmp

# Building
Install all dependencies listed above. Then building the code should be as
simple as calling:

mkdir build

cd build

cmake ..

From there, you should be able to solve the Sod problem by calling

mkdir -p data/Sod

bin/EulerDriver -infile ../inputFiles/sod.init

