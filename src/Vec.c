/* File: Vec.c
 * Author: Ian May
 * Purpose: Define vectors supported on cells, faces, and vertices of a given mesh
 */

#include <stdio.h>

#include "Vec.h"

/* Cell centered vector */
void CreateCellVec(Mesh mesh, int a_nC, CellVec *a_vec)
{
  CellVec vec = malloc(sizeof(*vec));
  vec->nInt = mesh->nInt;
  vec->nTot = mesh->nTot;
  vec->nX = mesh->nX;
  vec->nY = mesh->nY;
  vec->nHalo = mesh->nHalo;
  vec->nC = a_nC;
  vec->v = calloc(vec->nC*vec->nTot,sizeof(*vec->v));
  
  *a_vec = vec;
}

void CellVecDuplicate(CellVec src, CellVec *a_dup)
{
  if(src) {
    CellVec vec = malloc(sizeof(*vec));
    vec->nInt = src->nInt;
    vec->nTot = src->nTot;
    vec->nX = src->nX;
    vec->nY = src->nY;
    vec->nHalo = src->nHalo;
    vec->nC = src->nC;
    vec->v = malloc(vec->nC*vec->nTot*sizeof(*vec->v));
    for(int n=0; n<vec->nC*vec->nTot; n++) { vec->v[n] = src->v[n]; } 
    *a_dup = vec;
  } else {
    *a_dup = NULL;
  }
}

void DestroyCellVec(CellVec *a_vec)
{
  if(*a_vec) {
    CellVec vec = *a_vec;
    if(vec->v) { free(vec->v); }
    free(*a_vec);
  }
}

void CellVecPrint(CellVec vec, int nC)
{
  for(int nY=vec->nY+vec->nHalo-1; nY>=-vec->nHalo; nY--) {
    for(int nX=-vec->nHalo; nX<vec->nX+vec->nHalo; nX++) {
      printf("%1.3f  ", vec->v[cellLex(vec,nX,nY)+nC]);
    }
    printf("\n");
  }
}

void CreateFaceVecC(Mesh mesh, int a_nC, FaceVecC *a_vec)
{
  FaceVecC vec = malloc(sizeof(*vec));
  vec->nX = mesh->nX;
  vec->nY = mesh->nY;
  vec->nHalo = mesh->nHalo;
  vec->nC = a_nC;  
  int nEW = vec->nC*((2*vec->nHalo+vec->nY)*(2*vec->nHalo+vec->nX+1));
  int nSN = vec->nC*((2*vec->nHalo+vec->nX)*(2*vec->nHalo+vec->nY+1));
  vec->vY = calloc(nSN,sizeof(*vec->vY));
  vec->vX = calloc(nEW,sizeof(*vec->vX));

  *a_vec = vec;
}

void DestroyFaceVecC(FaceVecC *a_vec)
{
  if(*a_vec) {
    FaceVecC vec = *a_vec;
    if(vec->vX) { free(vec->vY); }
    if(vec->vY) { free(vec->vX); }
    free(*a_vec);
  }
}

void CreateFaceVecD(Mesh mesh, int a_nC, FaceVecD *a_vec)
{
  FaceVecD vec = malloc(sizeof(*vec));
  vec->nInt = mesh->nInt;
  vec->nX = mesh->nX;
  vec->nY = mesh->nY;
  vec->nHalo = mesh->nHalo;
  vec->nC = a_nC;
  int nSN = vec->nC*((vec->nY+1)*(2*vec->nHalo+vec->nX));
  int nEW = vec->nC*((vec->nX+1)*(2*vec->nHalo+vec->nY));
  vec->_p_vN = calloc(nSN,sizeof(*vec->_p_vN));
  vec->vN = &vec->_p_vN[vec->nC*(2*vec->nHalo+vec->nX)];
  vec->vS = calloc(nSN,sizeof(*vec->vS));
  vec->_p_vE = calloc(nEW,sizeof(*vec->_p_vE));
  vec->vE = &vec->_p_vE[vec->nC*(2*vec->nHalo+vec->nY)];
  vec->vW = calloc(nEW,sizeof(*vec->vW));

  *a_vec = vec;
}

void DestroyFaceVecD(FaceVecD *a_vec)
{
  if(*a_vec) {
    FaceVecD vec = *a_vec;
    if(vec->_p_vN) { free(vec->_p_vN); }
    if(vec->vS) { free(vec->vS); }
    if(vec->_p_vE) { free(vec->_p_vE); }
    if(vec->vW) { free(vec->vW); }
    free(*a_vec);
  }
}

void FaceVecDPrint(FaceVecD vec, int nC)
{
  printf("North Face:\n");
  for(int nY=vec->nY-1; nY>=-1; nY--) {
    for(int nX=-vec->nHalo; nX<vec->nX+vec->nHalo; nX++) {
      printf("%1.3f  ", vec->vN[faceLexNS(vec,nX,nY)+nC]);
    }
    printf("\n");
  }
  printf("South Face:\n");
  for(int nY=vec->nY; nY>=0; nY--) {
    for(int nX=-vec->nHalo; nX<vec->nX+vec->nHalo; nX++) {
      printf("%1.3f  ", vec->vS[faceLexNS(vec,nX,nY)+nC]);
    }
    printf("\n");
  }
  printf("East Face:\n");
  for(int nY=vec->nY+vec->nHalo-1; nY>=-vec->nHalo; nY--) {
    for(int nX=-1; nX<vec->nX; nX++) {
      printf("%1.3f  ", vec->vE[faceLexEW(vec,nX,nY)+nC]);
    }
    printf("\n");
  }
  printf("West Face:\n");
  for(int nY=vec->nY+vec->nHalo-1; nY>=-vec->nHalo; nY--) {
    for(int nX=0; nX<=vec->nX; nX++) {
      printf("%1.3f  ", vec->vW[faceLexEW(vec,nX,nY)+nC]);
    }
    printf("\n");
  }
}
