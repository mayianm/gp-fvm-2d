/* File: ConsChar.h
 * Author: Ian May
 * Purpose: Provide conversion between conservative and characteristic variables
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include "LapackWrappers.h"
#include "ConsChar.h"
#include "EOS.h"
#include "definition.h"

/* Scalar advection is simple */
void CC_AdvectX(double *U, double *R, double *L, double *fProp)
{
  (void) U;
  (void) fProp;
  L[0] = 1.;
  R[0] = 1.;
}

void CC_AdvectY(double *U, double *R, double *L, double *fProp)
{
  (void) U;
  (void) fProp;
  L[0] = 1.;
  R[0] = 1.;
}

/* Return the right and left eigenvectors */
void CC_EulerX(double *U, double *R, double *L, double *fProp)
{
  const double gm = fProp[PROP_GAM]-1;
  const double u=U[VAR_MOMX]/U[VAR_DENS], v=U[VAR_MOMY]/U[VAR_DENS], w=U[VAR_MOMZ]/U[VAR_DENS];
  const double ekin = (u*u+v*v+w*w)/2.;
  const double a = igl_soundspeed(fProp[PROP_GAM],U);
  const double aagm = a*a/gm;
  const double lp = gm/(2.*a*a);
  /* left e-vecs */
  L[0]= lp*(ekin+u*a/gm);  L[5]=lp*(-u-a/gm); L[10]=-lp*v;      L[15]=-lp*w;      L[20]=lp;
  L[1]= 2.*lp*(aagm-ekin); L[6]=2.*lp*u;      L[11]=2.*lp*v;    L[16]=2.*lp*w;    L[21]=-2.*lp;
  L[2]=-2.*lp*v*aagm;      L[7]=0.;           L[12]=2.*lp*aagm; L[17]=0;          L[22]=0.;
  L[3]=-2.*lp*w*aagm;      L[8]=0.;           L[13]=0;          L[18]=2.*lp*aagm; L[23]=0.;
  L[4]= lp*(ekin-u*a/gm);  L[9]=lp*(-u+a/gm); L[14]=-lp*v;      L[19]=-lp*w;      L[24]=lp;
  /* Right e-vecs */
  R[0]=1.;            R[5]=1;    R[10]=0.; R[15]=0.; R[20]=1;
  R[1]=u-a;           R[6]=u;    R[11]=0.; R[16]=0.; R[21]=u+a;
  R[2]=v;             R[7]=v;    R[12]=1.; R[17]=0.; R[22]=v;
  R[3]=w;             R[8]=w;    R[13]=0.; R[18]=1.; R[23]=w;
  R[4]=aagm+ekin-u*a; R[9]=ekin; R[14]=v;  R[19]=w;  R[24]=aagm+ekin+u*a;
}

void CC_EulerY(double *U, double *R, double *L, double *fProp)
{
  const double gm = fProp[PROP_GAM]-1;
  const double u=U[VAR_MOMX]/U[VAR_DENS], v=U[VAR_MOMY]/U[VAR_DENS], w=U[VAR_MOMZ]/U[VAR_DENS];
  const double ekin = (u*u+v*v+w*w)/2.;
  const double a = igl_soundspeed(fProp[PROP_GAM],U);
  const double aagm = a*a/gm;
  const double lp = gm/(2.*a*a);
  /* left e-vecs */
  L[0]= lp*(ekin+v*a/gm);  L[5]=-lp*u;      L[10]=lp*(-v-a/gm); L[15]=-lp*w;      L[20]=lp;
  L[1]=-2.*lp*u*aagm;      L[6]=2.*lp*aagm; L[11]=0.;           L[16]=0.;         L[21]=0.;
  L[2]= 2.*lp*(aagm-ekin); L[7]=2.*lp*u;    L[12]=2.*lp*v;      L[17]=2.*lp*w;    L[22]=-2.*lp;
  L[3]=-2.*lp*w*aagm;      L[8]=0.;         L[13]=0.;           L[18]=2.*lp*aagm; L[23]=0.;
  L[4]= lp*(ekin-v*a/gm);  L[9]=-lp*u;      L[14]=lp*(-v+a/gm); L[19]=-lp*w;      L[24]=lp;
  /* Right e-vecs */
  R[0]=1.;            R[5]=0.; R[10]=1.;   R[15]=0.; R[20]=1;
  R[1]=u;             R[6]=1.; R[11]=u;    R[16]=0.; R[21]=u;
  R[2]=v-a;           R[7]=0;  R[12]=v;    R[17]=0;  R[22]=v+a;
  R[3]=w;             R[8]=0.; R[13]=w;    R[18]=1.; R[23]=w;
  R[4]=aagm+ekin-v*a; R[9]=u;  R[14]=ekin; R[19]=w;  R[24]=aagm+ekin+v*a;
}
