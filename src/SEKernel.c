/* File: SEKernel.c
 * Author: Ian May
 * Purpose:
 */

#include <stdio.h>
#include <math.h>
#include <complex.h>

#include <cerf.h>

#include "SEKernel.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Covariance functions */
/* Kernel for pointwise data */
double complex SE_pntCov(double complex ce, double xdkh, double ydkh)
{
  double sq = (xdkh*xdkh+ydkh*ydkh);
  double complex cesq = ce*ce;
  return cexp(-cesq*sq);
}
/* Kernel for 1D cell-averaged or face-averaged data */
double complex SE_intCov1D(double complex ce, double dkh, double unused)
{
  (void) unused;
  const double rpi=sqrt(M_PI);
  double dp=dkh+1., dm=dkh-1.;
  double complex cesq=ce*ce;

  return rpi*(
    ce*dp*cerf(ce*dp) +
    ce*dm*cerf(ce*dm) +
    (cexp(-cesq*dp*dp) + cexp(-cesq*dm*dm))/rpi -
    2.*(ce*dkh*cerf(ce*dkh) + cexp(-cesq*dkh*dkh)/rpi)
    )/(2.*cesq);
}
/* Kernel for 2D cell-averaged data */
double complex SE_intCov2D(double complex ce, double xdkh, double ydkh)
{
  return SE_intCov1D(ce,xdkh,0.)*SE_intCov1D(ce,ydkh,0.);
}

/* 1D Sample functions */
/* Sample 1D cell-averaged data onto point wise predictions */
/* By symmetry this also does the pnt -> av conversion */
double complex SE_sample1DAv2Pnt(double complex ce, double dkh, double unused)
{
  (void) unused;
  const double complex pf = sqrt(M_PI)/(2.*ce);
  return pf*(cerf(ce*(dkh+0.5))-cerf(ce*(dkh-0.5)));
}
/* Sample 1D cell-averaged data onto pointwise first derivative */
double complex SE_sample1DAv2Deriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex cesq = ce*ce;
  double dp=dkh+0.5, dm=dkh-0.5;
  return cexp(-cesq*dp*dp) - cexp(-cesq*dm*dm);
}
/* Sample 1D cell-averaged data onto pointwise second derivative */
double complex SE_sample1DAv2SecDeriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex cesq=ce*ce;
  double dp=dkh+0.5, dm=dkh-0.5;
  return 2.*cesq*(dm*cexp(-cesq*dm*dm) - dp*cexp(-cesq*dp*dp));
}
/* Sample 1D pointwise data onto point wise first derivative */
double complex SE_sample1DPntDeriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex cesq=ce*ce;
  return -2.*cesq*dkh*cexp(-cesq*dkh*dkh);
}
/* Sample 1D pointwise data onto pointwise second derivative */
double complex SE_sample1DPntSecDeriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex cesq=ce*ce;
  return 2.*cesq*(2.*cesq*dkh*dkh - 1)*cexp(-cesq*dkh*dkh);
}

/* 2D sample functions, separability of SE kernel makes these simple */
/* Sample 2D cell-averages onto pointwise predictions */
double complex SE_sample2DAv2Pnt(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Pnt(ce,xdkh,0.)*SE_sample1DAv2Pnt(ce,ydkh,0.);
}
/* Sample 2D cell-averages onto pointwise derivative in x */
double complex SE_sample2DAv2DerivX(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Deriv(ce,xdkh,0.)*SE_sample1DAv2Pnt(ce,ydkh,0.);
}
/* Sample 2D cell-averages onto pointwise derivative in y */
double complex SE_sample2DAv2DerivY(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Pnt(ce,xdkh,0.)*SE_sample1DAv2Deriv(ce,ydkh,0.);
}
/* Sample 2D cell-averages onto pointwise second derivative in x */
double complex SE_sample2DAv2SecDerivXX(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2SecDeriv(ce,xdkh,0.)*SE_sample1DAv2Pnt(ce,ydkh,0.);
}
/* Sample 2D cell-averages onto pointwise second derivative in y */
double complex SE_sample2DAv2SecDerivYY(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Pnt(ce,xdkh,0.)*SE_sample1DAv2SecDeriv(ce,ydkh,0.);
}
/* Sample 2D cell-averages onto pointwise second mixed derivative */
double complex SE_sample2DAv2SecDerivXY(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Deriv(ce,xdkh,0.)*SE_sample1DAv2Deriv(ce,ydkh,0.);
}

/* Specialized sample functions */
/* Electric field */
double complex SE_sampleElecX(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Pnt(ce,xdkh,0.)*(SE_pntCov(ce,ydkh-0.5,0.) - SE_pntCov(ce,ydkh+0.5,0.));
}

double complex SE_sampleElecY(double complex ce, double xdkh, double ydkh)
{
  return SE_sample1DAv2Pnt(ce,ydkh,0.)*(SE_pntCov(ce,xdkh+0.5,0.) - SE_pntCov(ce,xdkh-0.5,0.));
}
