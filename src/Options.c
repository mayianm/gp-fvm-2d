/* File: Options.c
 * Author: Ian May
 * Purpose: Functions to set the options struct from input files/CLI args
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "Options.h"
#include "IBCs.h"
#include "Riemann.h"
#include "ConsChar.h"

/* helper functions */
void defaultOpt(Options*);
void parseOpt(char*,char*,Options*);

void OptionsSetup(char *infile, int startArg, int argc, char **argv, Options *opt)
{
  /* Set opt to defaults */
  defaultOpt(opt);
  /* Open input file and parse */
  FILE *input = fopen(infile,"r");
  if(!input) { printf("Input file failed to open!!!!"); return; }
  char line[100], *arg, *val;
  /* Go ahead and make dangerous format assumptions */
  while(fgets(line, sizeof line, input)!=NULL) {
    arg = strtok(line," \n");
    val = strtok(NULL," \n");
    parseOpt(arg,val,opt);
  }
  fclose(input);
  /* Override input file with CLI options */
  for(int nA=startArg; nA<argc; nA+=2) {
    parseOpt(argv[nA],argv[nA+1],opt);
  }
}

static char* bcEnumToName(BCType bc)
{
  if(bc == PERIODIC) { return "periodic"; }
  if(bc == REFLECTING) { return "reflecting"; }
  if(bc == INFLOW) { return "inflow"; }
  if(bc == OUTFLOW) { return "outflow"; }
  return "mixed";
}

static char* eqEnumToName(EQType eq)
{
  if(eq == ADVECT) { return "advection"; }
  return "euler";
}

static char* rsEnumToName(RSType rs)
{
  if(rs == LLF) { return "llf"; }
  else if(rs == HLL) { return "hll"; }
  else if(rs == ADVEX) { return "advex"; }
  return "hllc";
}

static char* tsEnumToName(TSType ts)
{
  if(ts == RK2) { return "rk2"; }
  else if(ts == RK3) { return "rk3"; }
  else if(ts == RK4) { return "rk4"; }
  return "rk7";
}

static char* ibcEnumToName(IBCFunc ibc)
{
  if(ibc == GAUSS) { return "gauss"; }
  if(ibc == TILTGAUSS) { return "tiltgauss"; }
  if(ibc == ISENVORT) { return "isenvort"; }
  if(ibc == BUCHHELZ) { return "buchhelz"; }
  if(ibc == SOD) { return "sod"; }
  if(ibc == SODVERT) { return "sodvert"; }
  if(ibc == SEDOV) { return "sedov"; }
  if(ibc == NOH) { return "noh"; }
  if(ibc == SHUOSHER) { return "shuosher"; }
  if(ibc == RICHTMESHLH) { return "richtmeshlh"; }
  if(ibc == RICHTMESHHL) { return "richtmeshhl"; }
  if(ibc == SHOCKVORT) { return "shockvort"; }
  if(ibc == SHOCKVORTWAVE) { return "shockvortwave"; }
  if(ibc == SHOCKBUBBLELH) { return "shockbubblelh"; }
  if(ibc == SHOCKBUBBLEHL) { return "shockbubblehl"; }
  if(ibc == ASTROJET) { return "astrojet"; }
  if(ibc == DOUBLEMACH) { return "doublemach"; }
  if(ibc == RIEM3) { return "riemann3"; }
  if(ibc == RIEM4) { return "riemann4"; }
  if(ibc == RIEM5) { return "riemann5"; }
  if(ibc == RIEM6) { return "riemann6"; }
  if(ibc == RIEM8) { return "riemann8"; }
  if(ibc == RIEM11) { return "riemann11"; }
  if(ibc == RIEM12) { return "riemann12"; }
  if(ibc == RIEM13) { return "riemann13"; }
  if(ibc == RIEM17) { return "riemann17"; }
  if(ibc == RIEM3ROT) { return "riemann3rot"; }
  if(ibc == KELVHELM) { return "kelvhelm"; }
  if(ibc == IMPLOSION) { return "implosion"; }
  if(ibc == RAYLEIGHTAYLOR) { return "rayleightaylor"; }
  return "other";
}

static char* gpShapeToName(StenShape shape)
{
  if(shape == GPDIAMOND) { return "diamond"; }
  else if(shape == GPCIRCLE) { return "circle"; }
  else { return "dd"; }
}

static char* gpKernelToName(KernelType type)
{
  if(type == GPSE) { return "squared exponential"; }
  else if(type == GPIMQ) { return "inverse multiquadric"; }
  else if(type == GPIQ) { return "inverse quadratic"; }
  else { return "multiquadric"; }
}

void OptionsPrint(Options *opt)
{
  printf("--- Equation ---\n");
  printf("  eq type: %s\n",eqEnumToName(opt->eqType));
  printf("--- GP Settings ---\n");
  printf("  gp shape: %s\n",gpShapeToName(opt->gp_shape));
  printf("  gp kernel: %s\n",gpKernelToName(opt->gp_kernel));
  printf("  gp rad: %d\n",opt->gp_rad);
  printf("  gp lFac: %2.2f\n",opt->gp_lFac);
  printf("  gp eps: %e\n",opt->gp_eps);
  printf("  gp betapow: %1.2f\n",opt->gp_betaPow);
  printf("--- Grid Settings ---\n");
  printf("  grid nx: %d\n",opt->gr_nx);
  printf("  grid ny: %d\n",opt->gr_ny);
  printf("  grid xlo: %1.2f\n",opt->gr_xlo);
  printf("  grid xup: %1.2f\n",opt->gr_xup);
  printf("  grid ylo: %1.2f\n",opt->gr_ylo);
  printf("  grid yup: %1.2f\n",opt->gr_yup);
  printf("--- BC Types ---\n");
  printf("  bc n: %s\n",bcEnumToName(opt->bc_n));
  printf("  bc e: %s\n",bcEnumToName(opt->bc_e));
  printf("  bc s: %s\n",bcEnumToName(opt->bc_s));
  printf("  bc w: %s\n",bcEnumToName(opt->bc_w));
  printf("--- Solver Settings ---\n");
  printf("  solver nComp: %d\n",opt->so_nComp);
  printf("  solver nStep: %d\n",opt->so_nStep);
  printf("  solver nProp: %d\n",opt->so_nProp);
  printf("  solver dt: %e\n",opt->so_dt);
  printf("  solver tf: %1.2f\n",opt->so_tf);
  printf("  solver cfl: %1.2f\n",opt->so_cfl);
  printf("  solver fProp: %1.2f\n",opt->so_fProp[0]);
  printf(opt->so_fluxRec?"  so fluxrec: true\n":"  so fluxrec: false\n");
  printf(opt->so_gaussFlux?"  so gaussflux: true\n":"  so gaussflux: false\n");
  printf(opt->so_polyWeno?"  so polyweno: true\n":"  so polyweno: false\n");
  printf(opt->so_bhFlux?"  so bhflux: true\n":"  so bhflux: false\n");
  printf("  rs type: %s\n",rsEnumToName(opt->so_rsType));
  printf("  ts type: %s\n",tsEnumToName(opt->so_tsType));
  printf("  ibc type: %s\n",ibcEnumToName(opt->so_ibc));
  printf("--- Post-Proc Settings ---\n");
  printf("  pp verbose: %d\n",opt->pp_verbose);
  printf(opt->pp_compLast?"  pp complast: true\n":"  pp complast: false\n");
  printf("  pp plotfreq: %d\n",opt->pp_plotFreq);
  printf("  pp fBase: %s\n",opt->pp_fBase);
  printf("  pp convFile: %s\n",opt->pp_convFile);
}


void parseOpt(char *arg, char *val, Options *opt)
{
  /* Equation */
  if(strcmp(arg,"equation")==0) {
    if(strcmp(val,"advect")==0) {
      opt->eqType = ADVECT;
      opt->so_nComp = 1;
      opt->so_ccx = &CC_AdvectX;
      opt->so_ccy = &CC_AdvectY;
    }
    if(strcmp(val,"euler")==0) {
      opt->eqType = EULER;
      opt->so_nComp = 5;
      opt->so_ccx = &CC_EulerX;
      opt->so_ccy = &CC_EulerY;
    }
  }
  /* GP settings */
  if(strcmp(arg,"gp_rad")==0) { opt->gp_rad = atoi(val);}
  if(strcmp(arg,"gp_lfac")==0) { opt->gp_lFac = atof(val);}
  if(strcmp(arg,"gp_eps")==0) { opt->gp_eps = atof(val);}
  if(strcmp(arg,"gp_betapow")==0) { opt->gp_betaPow = atof(val);}
  if(strcmp(arg,"gp_shape")==0) {
    if(strcmp(val,"diamond")==0) { opt->gp_shape = GPDIAMOND; }
    else if(strcmp(val,"circle")==0) { opt->gp_shape = GPCIRCLE; }
    else { opt->gp_shape = GPDD; }
  }
  if(strcmp(arg,"gp_kernel")==0) {
    if(strcmp(val,"se")==0) { opt->gp_kernel = GPSE; }
    else if(strcmp(val,"iq")==0) { opt->gp_kernel = GPIQ; }
    else if(strcmp(val,"imq")==0) { opt->gp_kernel = GPIMQ; }
    else { opt->gp_kernel = GPMQ; }
  }
  /* Grid settings */
  if(strcmp(arg,"gr_nx")==0) { opt->gr_nx = atoi(val);}
  if(strcmp(arg,"gr_ny")==0) { opt->gr_ny = atoi(val);}
  if(strcmp(arg,"gr_xlo")==0) { opt->gr_xlo = atof(val);}
  if(strcmp(arg,"gr_xup")==0) { opt->gr_xup = atof(val);}
  if(strcmp(arg,"gr_ylo")==0) { opt->gr_ylo = atof(val);}
  if(strcmp(arg,"gr_yup")==0) { opt->gr_yup = atof(val);}
  /* Solver settings */
  if(strcmp(arg,"so_nprop")==0) { opt->so_nProp = atoi(val);}
  if(strcmp(arg,"so_nstep")==0) { opt->so_nStep = atoi(val);}
  if(strcmp(arg,"so_dt")==0) { opt->so_dt = atof(val);}
  if(strcmp(arg,"so_tf")==0) { opt->so_tf = atof(val);}
  if(strcmp(arg,"so_cfl")==0) { opt->so_cfl = atof(val);}
  if(strcmp(arg,"so_fprop")==0) { opt->so_fProp[0] = atof(val);}
  if(strcmp(arg,"so_fluxrec")==0) {
    opt->so_fluxRec = (strcmp(val,"true")==0);
  }
  if(strcmp(arg,"so_gaussflux")==0) {
    opt->so_gaussFlux = (strcmp(val,"true")==0);
  }
  if(strcmp(arg,"so_polyweno")==0) {
    opt->so_polyWeno = (strcmp(val,"true")==0);
  }
  if(strcmp(arg,"so_bhflux")==0) {
    opt->so_bhFlux = (strcmp(val,"true")==0);
  }
  if(strcmp(arg,"so_rstype")==0) {
    if(strcmp(val,"llf")==0) {
      opt->so_rsType = LLF;
      opt->so_rsx=&RS_EulerLLFX;
      opt->so_rsy=&RS_EulerLLFY;
    }
    if(strcmp(val,"hll")==0) {
      opt->so_rsType = HLL;
      opt->so_rsx=&RS_EulerHLLX;
      opt->so_rsy=&RS_EulerHLLY;
    }
    if(strcmp(val,"hllc")==0) {
      opt->so_rsType = HLLC;
      opt->so_rsx=&RS_EulerHLLCX;
      opt->so_rsy=&RS_EulerHLLCY;
    }
    if(strcmp(val,"advex")==0) {
      opt->so_rsType = ADVEX;
      opt->so_rsx=&ADV_ExactX;
      opt->so_rsy=&ADV_ExactY;
    }
  }
  if(strcmp(arg,"so_tstype")==0) {
    if(strcmp(val,"rk2")==0) { opt->so_tsType=RK2; }
    if(strcmp(val,"rk3")==0) { opt->so_tsType=RK3; }
    if(strcmp(val,"rk4")==0) { opt->so_tsType=RK4; }
    if(strcmp(val,"rk7")==0) { opt->so_tsType=RK7; }
  }
  if(strcmp(arg,"so_ibc")==0) {
    /* BC funcs default to NULL, only need to set when not NULL */
    if(strcmp(val,"advect")==0) {
      opt->so_ibc = SCLADVECT;
      opt->so_ic = &ic_Advect;
    } else if(strcmp(val,"gauss")==0) {
      opt->so_ibc = GAUSS;
      opt->so_ic = &ic_Gauss;
    } else if(strcmp(val,"tiltgauss")==0) {
      opt->so_ibc = TILTGAUSS;
      opt->so_ic = &ic_TiltGauss;
    } else if(strcmp(val,"isenvort")==0) {
      opt->so_ibc = ISENVORT;
      opt->so_ic = &ic_IsenVort;
    } else if(strcmp(val,"buchhelz")==0) {
      opt->so_ibc = BUCHHELZ;
      opt->so_ic = &ic_BuchHelz;
    } else if(strcmp(val,"sod")==0) {
      opt->so_ibc = SOD;
      opt->so_bcW = &bcw_Sod;
      opt->so_ic = &ic_Sod;
    } else if(strcmp(val,"sodvert")==0) {
      opt->so_ibc = SODVERT;
      opt->so_bcS = &bcs_SodVert;
      opt->so_ic = &ic_SodVert;
    } else if(strcmp(val,"sedov")==0) {
      opt->so_ibc = SEDOV;
      opt->so_ic = &ic_Sedov;
    } else if(strcmp(val,"noh")==0) {
      opt->so_ibc = NOH;
      opt->so_ic = &ic_Noh;
      opt->so_bcN = &bcn_Noh;
      opt->so_bcE = &bce_Noh;
    } else if(strcmp(val,"shuosher")==0) {
      opt->so_ibc = SHUOSHER;
      opt->so_bcW = &bcw_ShuOsher;
      opt->so_ic = &ic_ShuOsher;
    } else if(strcmp(val,"richtmeshlh")==0) {
      opt->so_ibc = RICHTMESHLH;
      opt->so_ic = &ic_RichtMeshLH;
    } else if(strcmp(val,"richtmeshhl")==0) {
      opt->so_ibc = RICHTMESHHL;
      opt->so_ic = &ic_RichtMeshHL;
    } else if(strcmp(val,"shockvort")==0) {
      opt->so_ibc = SHOCKVORT;
      opt->so_bcW = &bcw_ShockVort;
      opt->so_ic = &ic_ShockVort;
    } else if(strcmp(val,"shockbubblehl")==0) {
      opt->so_ibc = SHOCKBUBBLEHL;
      opt->so_ic = &ic_ShockBubbleHL;
    } else if(strcmp(val,"shockbubblelh")==0) {
      opt->so_ibc = SHOCKBUBBLELH;
      opt->so_ic = &ic_ShockBubbleLH;
    } else if(strcmp(val,"astrojet")==0) {
      opt->so_ibc = ASTROJET;
      opt->so_ic = &ic_AstroJet;
      opt->so_bcW = &bcw_AstroJet;
    } else if(strcmp(val,"shockvortwave")==0) {
      opt->so_ibc = SHOCKVORTWAVE;
      opt->so_bcW = &bcw_ShockVortWave;
      opt->so_bcE = &bce_ShockVortWave;
      opt->so_ic = &ic_ShockVortWave;
    } else if(strcmp(val,"doublemach")==0) {
      opt->so_ibc = DOUBLEMACH;
      opt->so_bcN = &bcn_DoubleMach;
      opt->so_bcW = &bcw_DoubleMach;
      opt->bc_southTr = 1./6.;
      opt->bc_sSub[0] = OUTFLOW;
      opt->bc_sSub[1] = REFLECTING;
      opt->so_ic = &ic_DoubleMach;
    } else if(strcmp(val,"riem3")==0) {
      opt->so_ibc = RIEM3;
      opt->so_ic = &ic_Riem3;
    } else if(strcmp(val,"riem4")==0) {
      opt->so_ibc = RIEM4;
      opt->so_ic = &ic_Riem4;
    } else if(strcmp(val,"riem5")==0) {
      opt->so_ibc = RIEM5;
      opt->so_ic = &ic_Riem5;
    } else if(strcmp(val,"riem6")==0) {
      opt->so_ibc = RIEM6;
      opt->so_ic = &ic_Riem6;
    } else if(strcmp(val,"riem8")==0) {
      opt->so_ibc = RIEM8;
      opt->so_ic = &ic_Riem8;
    } else if(strcmp(val,"riem11")==0) {
      opt->so_ibc = RIEM11;
      opt->so_ic = &ic_Riem11;
    } else if(strcmp(val,"riem12")==0) {
      opt->so_ibc = RIEM12;
      opt->so_ic = &ic_Riem12;
    } else if(strcmp(val,"riem13")==0) {
      opt->so_ibc = RIEM13;
      opt->so_ic = &ic_Riem13;
    } else if(strcmp(val,"riem17")==0) {
      opt->so_ibc = RIEM17;
      opt->so_ic = &ic_Riem17;
    } else if(strcmp(val,"riem3rot")==0) {
      opt->so_ibc = RIEM3ROT;
      opt->so_ic = &ic_Riem3Rot;
    } else if(strcmp(val,"kelvhelm")==0) {
      opt->so_ibc = KELVHELM;
      opt->so_ic = &ic_KelvHelm;
    } else if(strcmp(val,"implosion")==0) {
      opt->so_ibc = IMPLOSION;
      opt->so_ic = &ic_Implosion;
    } else if(strcmp(val,"rayleightaylor")==0) {
      opt->so_ibc = RAYLEIGHTAYLOR;
      opt->so_ic = &ic_RayleighTaylor;
      opt->so_bcN = &bcn_RayleighTaylor;
      opt->so_bcS = &bcs_RayleighTaylor;
      opt->so_src = &src_RayleighTaylor;
    } else if(strcmp(val,"other")==0) {
      opt->so_ibc = OTHERIBC;
      opt->so_bcN = &bcn_Other;
      opt->so_bcE = &bce_Other;
      opt->so_bcS = &bcs_Other;
      opt->so_bcW = &bcw_Other;
      opt->so_ic = &ic_Other;
    }
  }
  /* Boundary conditions */
  if(strcmp(arg,"bc_n")==0) {
    if(strcmp(val,"periodic")==0) { opt->bc_n = PERIODIC; }
    if(strcmp(val,"reflecting")==0) { opt->bc_n = REFLECTING; }
    if(strcmp(val,"inflow")==0) { opt->bc_n = INFLOW; }
    if(strcmp(val,"outflow")==0) { opt->bc_n = OUTFLOW; }
    if(strcmp(val,"mixed")==0) { opt->bc_n = MIXED; }
  }
  if(strcmp(arg,"bc_e")==0) {
    if(strcmp(val,"periodic")==0) { opt->bc_e = PERIODIC; }
    if(strcmp(val,"reflecting")==0) { opt->bc_e = REFLECTING; }
    if(strcmp(val,"inflow")==0) { opt->bc_e = INFLOW; }
    if(strcmp(val,"outflow")==0) { opt->bc_e = OUTFLOW; }
    if(strcmp(val,"mixed")==0) { opt->bc_e = MIXED; }
  }
  if(strcmp(arg,"bc_s")==0) {
    if(strcmp(val,"periodic")==0) { opt->bc_s = PERIODIC; }
    if(strcmp(val,"reflecting")==0) { opt->bc_s = REFLECTING; }
    if(strcmp(val,"inflow")==0) { opt->bc_s = INFLOW; }
    if(strcmp(val,"outflow")==0) { opt->bc_s = OUTFLOW; }
    if(strcmp(val,"mixed")==0) { opt->bc_s = MIXED; }
  }
  if(strcmp(arg,"bc_w")==0) {
    if(strcmp(val,"periodic")==0) { opt->bc_w = PERIODIC; }
    if(strcmp(val,"reflecting")==0) { opt->bc_w = REFLECTING; }
    if(strcmp(val,"inflow")==0) { opt->bc_w = INFLOW; }
    if(strcmp(val,"outflow")==0) { opt->bc_w = OUTFLOW; }
    if(strcmp(val,"mixed")==0) { opt->bc_w = MIXED; }
  }
  /* PP settings */
  if(strcmp(arg,"pp_verbose")==0) { opt->pp_verbose = atoi(val); }
  if(strcmp(arg,"pp_plotfreq")==0) { opt->pp_plotFreq = atoi(val); }
  if(strcmp(arg,"pp_complast")==0) {
    opt->pp_compLast = (strcmp(val,"true")==0);
  }
  if(strcmp(arg,"pp_fbase")==0) { sprintf(opt->pp_fBase,"%s",val); printf("Read fbase: %s\n",opt->pp_fBase); }
  if(strcmp(arg,"pp_convfile")==0) { sprintf(opt->pp_convFile,"%s",val); }
}

void defaultOpt(Options *opt)
{
  opt->eqType = EULER;
  
  opt->gp_rad = 2;
  opt->gp_lFac = 12.;
  opt->gp_eps = 1.e-36; opt->gp_betaPow = 1.;
  opt->gp_shape = GPDIAMOND;
  opt->gp_kernel = GPMQ;

  opt->gr_nx = 64; opt->gr_ny = 64;
  opt->gr_xlo = 0.; opt->gr_xup = 1.;
  opt->gr_ylo = 0.; opt->gr_yup = 1.;

  opt->bc_n = PERIODIC; opt->bc_e = PERIODIC;
  opt->bc_s = PERIODIC; opt->bc_w = PERIODIC;

  opt->bc_nSub[0] = PERIODIC; opt->bc_eSub[0] = PERIODIC;
  opt->bc_sSub[0] = PERIODIC; opt->bc_wSub[0] = PERIODIC;

  opt->bc_nSub[1] = PERIODIC; opt->bc_eSub[1] = PERIODIC;
  opt->bc_sSub[1] = PERIODIC; opt->bc_wSub[1] = PERIODIC;

  opt->bc_northTr = 0.; opt->bc_southTr = 0.;
  opt->bc_eastTr = 0.; opt->bc_westTr = 0.;

  opt->so_ibc = ISENVORT;
  opt->so_nComp = 5; opt->so_nProp = 1; opt->so_nStep = 1000;
  opt->so_dt=1.e-8; opt->so_tf=0.4; opt->so_cfl = 0.8; opt->so_fProp[0] = 1.4;
  opt->so_fluxRec=true; opt->so_gaussFlux=false; opt->so_polyWeno=false; opt->so_bhFlux=false;
  opt->so_rsType = HLLC;
  opt->so_src = NULL;
  opt->so_rsx = &RS_EulerHLLCX; opt->so_rsy = &RS_EulerHLLCY;
  opt->so_tsType = RK3;
  opt->so_bcN = NULL; opt->so_bcE = NULL; opt->so_bcS = NULL; opt->so_bcW = NULL;
  opt->so_ic = &ic_IsenVort;

  opt->pp_verbose = 3;
  opt->pp_compLast = false;
  opt->pp_plotFreq = 50;
  sprintf(opt->pp_fBase,"%s","data/IsenVort/IsenVort");
  sprintf(opt->pp_convFile,"%s","gpConv");
}
