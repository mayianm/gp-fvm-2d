/* File: EOS.c
 * Author: Ian May
 * Purpose: Hold various equation of state relationships between variables
 * Notes: These blindly relate state variables to eachother, passing in cell-averaged
 *        values will degrade accuracy to second order, so use wisely.
 */

#include <stdlib.h>
#include <math.h>
#include "EOS.h"
#include "definition.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Helpers */
static inline double ekin(double *U)
{
  return (U[VAR_MOMX]*U[VAR_MOMX]+U[VAR_MOMY]*U[VAR_MOMY]+U[VAR_MOMZ]*U[VAR_MOMZ])/(2.*U[VAR_DENS]);
}

/* Euler equations */
double igl_eint(double *U)
{
  return (U[VAR_ETOT]-ekin(U))/U[VAR_DENS];
}

double igl_pres(double gam, double *U)
{
  return (gam-1.)*U[VAR_DENS]*igl_eint(U);
}

double igl_soundspeed(double gam, double *U)
{
  return sqrt(gam*igl_pres(gam,U)/U[VAR_DENS]);
}
