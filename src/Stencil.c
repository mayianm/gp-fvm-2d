/* File: Stencil.c
 * Author: Ian May
 * Purpose: Define all GP-WENO stencils and reconstruction methods
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "LapackWrappers.h"
#include "Stencil.h"
#include "SEKernel.h"
#include "IQKernel.h"
#include "IMQKernel.h"
#include "MQKernel.h"
#include "Pade.h"

#define MAX(a,b) ((a)<(b) ? (b) : (a))
#define MIN(a,b) ((a)<(b) ? (a) : (b))
#define FABMIN(a,b) (fabs(a)<fabs(b) ? (a) : (b))

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Kernel function containers */
/* Squared exponential */
static KernFuncs k1dSE = {
  .covPnt = SE_pntCov,
  .covInt = SE_intCov1D,
  .smpAv2Pnt = SE_sample1DAv2Pnt,
  .smpAv2Deriv = {SE_sample1DAv2Deriv, NULL},
  .smpAv2SecDeriv = {SE_sample1DAv2SecDeriv, NULL, NULL},
  .smpPnt2Deriv = SE_sample1DPntDeriv,
  .smpPnt2SecDeriv = SE_sample1DPntSecDeriv,
  .smpElec = {NULL, NULL}
};

static KernFuncs k2dSE = {
  .covPnt = SE_pntCov,
  .covInt = SE_intCov2D,
  .smpAv2Pnt = SE_sample2DAv2Pnt,
  .smpAv2Deriv = {SE_sample2DAv2DerivX, SE_sample2DAv2DerivY},
  .smpAv2SecDeriv = {SE_sample2DAv2SecDerivXX, SE_sample2DAv2SecDerivXY, SE_sample2DAv2SecDerivYY},
  .smpPnt2Deriv = NULL,
  .smpPnt2SecDeriv = NULL,
  .smpElec = {SE_sampleElecX,SE_sampleElecY}
};

/* Multiquadric */
static KernFuncs k1dMQ = {
  .covPnt = MQ_pntCov,
  .covInt = MQ_intCov1D,
  .smpAv2Pnt = MQ_sample1DAv2Pnt,
  .smpAv2Deriv = {MQ_sample1DAv2Deriv, NULL},
  .smpAv2SecDeriv = {MQ_sample1DAv2SecDeriv, NULL, NULL},
  .smpPnt2Deriv = MQ_sample1DPntDeriv,
  .smpPnt2SecDeriv = MQ_sample1DPntSecDeriv,
  .smpElec = {NULL, NULL}
};

static KernFuncs k2dMQ = {
  .covPnt = MQ_pntCov,
  .covInt = MQ_intCov2D,
  .smpAv2Pnt = MQ_sample2DAv2Pnt,
  .smpAv2Deriv = {MQ_sample2DAv2DerivX, MQ_sample2DAv2DerivY},
  .smpAv2SecDeriv = {MQ_sample2DAv2SecDerivXX, MQ_sample2DAv2SecDerivXY, MQ_sample2DAv2SecDerivYY},
  .smpPnt2Deriv = NULL,
  .smpPnt2SecDeriv = NULL,
  .smpElec = {MQ_sampleElecX,MQ_sampleElecY}
};

/* Inverse multiquadric */
static KernFuncs k1dIMQ = {
  .covPnt = IMQ_pntCov,
  .covInt = IMQ_intCov1D,
  .smpAv2Pnt = IMQ_sample1DAv2Pnt,
  .smpAv2Deriv = {IMQ_sample1DAv2Deriv, NULL},
  .smpAv2SecDeriv = {IMQ_sample1DAv2SecDeriv, NULL, NULL},
  .smpPnt2Deriv = IMQ_sample1DPntDeriv,
  .smpPnt2SecDeriv = IMQ_sample1DPntSecDeriv,
  .smpElec = {NULL, NULL}
};

static KernFuncs k2dIMQ = {
  .covPnt = IMQ_pntCov,
  .covInt = IMQ_intCov2D,
  .smpAv2Pnt = IMQ_sample2DAv2Pnt,
  .smpAv2Deriv = {IMQ_sample2DAv2DerivX, IMQ_sample2DAv2DerivY},
  .smpAv2SecDeriv = {IMQ_sample2DAv2SecDerivXX, IMQ_sample2DAv2SecDerivXY, IMQ_sample2DAv2SecDerivYY},
  .smpPnt2Deriv = NULL,
  .smpPnt2SecDeriv = NULL,
  .smpElec = {IMQ_sampleElecX,IMQ_sampleElecY}
};

/* Inverse quadratic */
static KernFuncs k1dIQ = {
  .covPnt = IQ_pntCov,
  .covInt = IQ_intCov1D,
  .smpAv2Pnt = IQ_sample1DAv2Pnt,
  .smpAv2Deriv = {IQ_sample1DAv2Deriv, NULL},
  .smpAv2SecDeriv = {IQ_sample1DAv2SecDeriv, NULL, NULL},
  .smpPnt2Deriv = IQ_sample1DPntDeriv,
  .smpPnt2SecDeriv = IQ_sample1DPntSecDeriv,
  .smpElec = {NULL, NULL}
};

static KernFuncs k2dIQ = {
  .covPnt = IQ_pntCov,
  .covInt = IQ_intCov2D,
  .smpAv2Pnt = IQ_sample2DAv2Pnt,
  .smpAv2Deriv = {IQ_sample2DAv2DerivX, IQ_sample2DAv2DerivY},
  .smpAv2SecDeriv = {IQ_sample2DAv2SecDerivXX, IQ_sample2DAv2SecDerivXY, IQ_sample2DAv2SecDerivYY},
  .smpPnt2Deriv = NULL,
  .smpPnt2SecDeriv = NULL,
  .smpElec = {IQ_sampleElecX,IQ_sampleElecY}
};

/* 1D stencil helpers */
void findPredVecs1D(GPStencil1D,int);
void findSubStencils1D(GPStencil1D);
void findGammas1D(GPStencil1D);
/* 1D Stencil functions */
void CreateGPStencil1D(int a_rad, double a_l, double a_eps, double a_bpow, KernelType a_ktype, GPStencil1D *a_sten)
{
  GPStencil1D sten = malloc(sizeof(*sten));
  sten->rad = a_rad;
  sten->nCells = 2*sten->rad+1;
  sten->l = a_l;
  sten->eps = a_eps;
  sten->bpow = a_bpow;
  /* Set kernel functions */
  switch(a_ktype) {
  case GPSE:
    sten->kFuncs = k1dSE; break;
  case GPIMQ:
    sten->kFuncs = k1dIMQ; break;
  case GPIQ:
    sten->kFuncs = k1dIQ; break;
  default:
    sten->kFuncs = k1dMQ;
  }
  findSubStencils1D(sten);
  /* Find local prediction vectors */
  for(int nS=0; nS<sten->nSub; nS++) {
    sten->pdPlus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdPlus[nS]));
    sten->pdMinus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdMinus[nS]));
    sten->pdGqPlus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdGqPlus[nS]));
    sten->pdGqMinus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdGqMinus[nS]));
    sten->pdCenter[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdCenter[nS]));
    sten->pdDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDeriv[nS]));
    sten->pdSecDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSecDeriv[nS]));
    sten->pdAvg[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdAvg[nS]));
    sten->pdAvDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdAvDeriv[nS]));
    sten->pdAvSecDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdAvSecDeriv[nS]));
    findPredVecs1D(sten,nS);
  }
  /* Set linear weights */
  findGammas1D(sten);
  /* Hand back stencil */
  *a_sten = sten;
}

void GPStencil1DCopy(GPStencil1D src, GPStencil1D *a_sten)
{
  GPStencil1D sten = malloc(sizeof(*sten));
  sten->rad = src->rad;
  sten->nCells = src->nCells;
  sten->l = src->l;
  sten->eps = src->eps;
  sten->bpow = src->bpow;
  sten->nSub = src->nSub;
  /* Copy all info */
  for(int nS=0; nS<sten->nSub; nS++) {
    sten->subSize[nS] = src->subSize[nS];
    sten->subStart[nS] = src->subStart[nS];
    sten->pdPlus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdPlus[nS]));
    sten->pdMinus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdMinus[nS]));
    sten->pdGqPlus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdGqPlus[nS]));
    sten->pdGqMinus[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdGqMinus[nS]));
    sten->pdCenter[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdCenter[nS]));
    sten->pdDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDeriv[nS]));
    sten->pdSecDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSecDeriv[nS]));
    sten->pdAvg[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdAvg[nS]));
    sten->pdAvDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdAvDeriv[nS]));
    sten->pdAvSecDeriv[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdAvSecDeriv[nS]));
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      sten->pdPlus[nS][nC] = src->pdPlus[nS][nC];
      sten->pdMinus[nS][nC] = src->pdMinus[nS][nC];
      sten->pdGqPlus[nS][nC] = src->pdGqPlus[nS][nC];
      sten->pdGqMinus[nS][nC] = src->pdGqMinus[nS][nC];
      sten->pdCenter[nS][nC] = src->pdCenter[nS][nC];
      sten->pdDeriv[nS][nC] = src->pdDeriv[nS][nC];
      sten->pdSecDeriv[nS][nC] = src->pdSecDeriv[nS][nC];
      sten->pdAvg[nS][nC] = src->pdAvg[nS][nC];
      sten->pdAvDeriv[nS][nC] = src->pdAvDeriv[nS][nC];
      sten->pdAvSecDeriv[nS][nC] = src->pdAvSecDeriv[nS][nC];
    }
    /* Allocate room for all substencil and smoothness stuff */
    sten->gamma[nS] = src->gamma[nS];
  }
  /* Hand back stencil */
  *a_sten = sten;
}

void DestroyGPStencil1D(GPStencil1D *a_sten)
{
  if(*a_sten) {
    GPStencil1D sten = *a_sten;
    for(int nS=0; nS<sten->nSub; nS++) {
      if(sten->pdPlus[nS]) { free(sten->pdPlus[nS]); }
      if(sten->pdMinus[nS]) { free(sten->pdMinus[nS]); }
      if(sten->pdGqPlus[nS]) { free(sten->pdGqPlus[nS]); }
      if(sten->pdGqMinus[nS]) { free(sten->pdGqMinus[nS]); }
      if(sten->pdCenter[nS]) { free(sten->pdCenter[nS]); }
      if(sten->pdDeriv[nS]) { free(sten->pdDeriv[nS]); }
      if(sten->pdSecDeriv[nS]) { free(sten->pdSecDeriv[nS]); }
      if(sten->pdAvg[nS]) { free(sten->pdAvg[nS]); }
      if(sten->pdAvDeriv[nS]) { free(sten->pdAvDeriv[nS]); }
      if(sten->pdAvSecDeriv[nS]) { free(sten->pdAvSecDeriv[nS]); }
    }
    free(*a_sten);
  }
}

static void GPStencil1DIndicP2A(GPStencil1D sten, int nComp, double * restrict vals, double * restrict omega)
{
  /* Evaluate smoothness indicators */
  double tau[nComp]; memset(tau,0,nComp*sizeof(*tau));
  double beta[sten->nSub*nComp];
  for(int nS=0; nS<sten->nSub; nS++) {
    double fd[nComp], sd[nComp];
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdDeriv[nS],1,0.,fd,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdSecDeriv[nS],1,0.,sd,1);
    for(int nC=0; nC<nComp; nC++) {
      beta[nS*nComp+nC] = fd[nC]*fd[nC] + sd[nC]*sd[nC];
      if(nS==1) { tau[nC] = fabs(beta[nC] - beta[nS*nComp+nC]); }
    }
  }
  /* Scale tau */
  for(int nC=0; nC<nComp; nC++) { tau[nC] /= 3.; }
  /* Form WENO weights */
  double tot[nComp]; memset(tot,0,nComp*sizeof(*tot));
  for(int nS=0; nS<sten->nSub; nS++) {
    for(int nC=0; nC<nComp; nC++) {
      omega[nS*nComp+nC] = sten->gamma[nS]*(1. + pow(tau[nC]/(beta[nS*nComp+nC]+sten->eps),sten->bpow));
      tot[nC] += omega[nS*nComp+nC];
    }
  }
  /* Modify weights to promote order switching */
  for(int nC=0; nC<nComp; nC++) { omega[nC] /= (tot[nC]*sten->gamma[0]); }
  for(int nS=1; nS<sten->nSub; nS++) {
    for(int nC=0; nC<nComp; nC++) {
      omega[nS*nComp+nC] = omega[nS*nComp+nC]/tot[nC] - omega[nC]*sten->gamma[nS];
    }
  }
}

static void GPStencil1DIndicA2P(GPStencil1D sten, int nComp, double * restrict vals, double * restrict omega)
{
  /* Evaluate smoothness indicators */
  double tau[nComp]; memset(tau,0,nComp*sizeof(*tau));
  double beta[sten->nSub*nComp];
  for(int nS=0; nS<sten->nSub; nS++) {
    double fd[nComp], sd[nComp];
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdAvDeriv[nS],1,0.,fd,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdAvSecDeriv[nS],1,0.,sd,1);
    for(int nC=0; nC<nComp; nC++) {
      beta[nS*nComp+nC] = fd[nC]*fd[nC] + sd[nC]*sd[nC];
      if(nS==1) { tau[nC] = fabs(beta[nC] - beta[nS*nComp+nC]); }
    }
  }
  /* Scale tau */
  for(int nC=0; nC<nComp; nC++) { tau[nC] /= 3.; }
  /* Form WENO weights */
  double tot[nComp]; memset(tot,0,nComp*sizeof(*tot));
  for(int nS=0; nS<sten->nSub; nS++) {
    for(int nC=0; nC<nComp; nC++) {
      omega[nS*nComp+nC] = sten->gamma[nS]*(1. + pow(tau[nC]/(beta[nS*nComp+nC]+sten->eps),sten->bpow));
      tot[nC] += omega[nS*nComp+nC];
    }
  }
  /* Modify weights to promote order switching */
  for(int nC=0; nC<nComp; nC++) { omega[nC] /= (tot[nC]*sten->gamma[0]); }
  for(int nS=1; nS<sten->nSub; nS++) {
    for(int nC=0; nC<nComp; nC++) {
      omega[nS*nComp+nC] = omega[nS*nComp+nC]/tot[nC] - omega[nC]*sten->gamma[nS];
    }
  }
}

static inline void diag_gemv(int nComp, double omega[nComp], double tmp[nComp], double pred[nComp])
{
  for(int nC=0; nC<nComp; nC++) { pred[nC] += omega[nC]*tmp[nC]; }
}

void GPStencil1DEvalPM(GPStencil1D sten, int nComp, double * restrict vals, double *plusVal, double *minusVal)
{
  double omega[sten->nSub*nComp];
  GPStencil1DIndicA2P(sten, nComp, vals, omega);
  /* Zero out */
  memset(plusVal,0,nComp*sizeof(*plusVal));
  memset(minusVal,0,nComp*sizeof(*minusVal));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<sten->nSub; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdPlus[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,plusVal);
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdMinus[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,minusVal);
  }
}

void GPStencil1DEvalGqPM(GPStencil1D sten, int nComp, double * restrict vals, double *plusVal, double *minusVal)
{
  double omega[sten->nSub*nComp];
  GPStencil1DIndicA2P(sten, nComp, vals, omega);
  /* Zero out */
  memset(plusVal,0,nComp*sizeof(*plusVal));
  memset(minusVal,0,nComp*sizeof(*minusVal));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<sten->nSub; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdGqPlus[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,plusVal);
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdGqMinus[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,minusVal);
  }
}

void GPStencil1DEvalCenter(GPStencil1D sten, int nComp, double * restrict vals, double *centerVal)
{
  double omega[sten->nSub*nComp];
  GPStencil1DIndicA2P(sten, nComp, vals, omega);
  /* Zero out */
  memset(centerVal,0,nComp*sizeof(*centerVal));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<sten->nSub; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdCenter[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,centerVal);
  }
}

void GPStencil1DEvalAvg(GPStencil1D sten, int nComp, double * restrict vals, double *avgVal)
{
  double omega[sten->nSub*nComp];
  GPStencil1DIndicP2A(sten, nComp, vals, omega);
  /* Zero out */
  memset(avgVal,0,nComp*sizeof(*avgVal));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<sten->nSub; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,&vals[nComp*sten->subStart[nS]],nComp,sten->pdAvg[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,avgVal);
  }
}

void findPredVecs1D(GPStencil1D sten, int nS)
{
  int nCells = sten->subSize[nS];
  /* Convert stencil to double prec. coords */
  double x[nCells], y[nCells];
  for(int n=0; n<nCells; n++) {
    x[n] = ((double) n+sten->subStart[nS]-sten->rad);
    y[n] = 0.;
  }
  /* Find prediction vectors: Av2Pt */
  findPredictor(nCells,sten->l,0.5,0.,x,y,sten->pdPlus[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,-0.5,0.,x,y,sten->pdMinus[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,1./(2.*sqrt(3.)),0.,x,y,sten->pdGqPlus[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,-1./(2.*sqrt(3.)),0.,x,y,sten->pdGqMinus[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdCenter[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  /* Find prediction vector: Pt2Av */
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdAvg[nS],
                sten->kFuncs.covPnt,sten->kFuncs.smpAv2Pnt,NULL);
  /* Find prediction vectors: First derivatives */
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdAvDeriv[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Deriv[0],NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdDeriv[nS],
                sten->kFuncs.covPnt,sten->kFuncs.smpPnt2Deriv,NULL);
  /* Find prediction vectors: Second derivatives */
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdAvSecDeriv[nS],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2SecDeriv[0],NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdSecDeriv[nS],
                sten->kFuncs.covPnt,sten->kFuncs.smpPnt2SecDeriv,NULL);
}

void findSubStencils1D(GPStencil1D sten)
{
  if(sten->rad == 0) {
    sten->nSub = 1;
    sten->subSize[0] = 1;
    sten->subStart[0] = 0;
  } else if(sten->rad == 1) {
    sten->nSub = 3;
    sten->subSize[0] = 3;
    sten->subSize[1] = 2;
    sten->subSize[2] = 2;
    sten->subStart[0] = 0;
    sten->subStart[1] = 0;
    sten->subStart[2] = 1;
  } else {
    sten->nSub = 4;
    /* Substencil sizes */
    sten->subSize[0] = 2*sten->rad+1;
    /* Biased are both rad+1 */
    sten->subSize[1] = 3; sten->subSize[3] = 3;
    /* Small central depends on even/odd rad */
    sten->subSize[2] = 3;
    sten->subStart[0] = 0;
    sten->subStart[1] = sten->rad-2;
    sten->subStart[2] = sten->rad-1;
    sten->subStart[3] = sten->rad;
  }
}

void findGammas1D(GPStencil1D sten)
{
  double gLo=0.9, gHi=0.9;
  if(sten->rad == 0) {
    sten->gamma[0] = 1.;
  } else if(sten->rad == 1) {
    sten->gamma[0] = gHi;
    sten->gamma[1] = (1.-gHi)/2.;
    sten->gamma[2] = (1.-gHi)/2.;
  } else {
    /* Set just the CWENO weights of the low order scheme */
    sten->gamma[1] = (1.-gLo)/2.;
    sten->gamma[2] = gLo;
    sten->gamma[3] = (1.-gLo)/2.;
    /* Large central stencil */
    sten->gamma[0] = gHi;
    for(int nS=1; nS<sten->nSub; nS++) {
      sten->gamma[nS] *= (1-gHi);
    }
  }
}

/* 2D stencil helpers */
static void findPredVecs2D(GPStencil2D,int);
static void findDiamondSubStencils2D(GPStencil2D);
static void findCircleSubStencils2D(GPStencil2D);
static void findGammas2D(GPStencil2D);
/* 2D Stencil functions */
void CreateGPStencil2D(int a_rad, double a_l, double a_eps, double a_bpow, StenShape a_shape, KernelType a_ktype, GPStencil2D *a_sten)
{
  GPStencil2D sten = malloc(sizeof(*sten));
  sten->rad = a_rad;
  sten->nSub = 6;
  sten->l = a_l;
  sten->eps = a_eps;
  sten->bpow = a_bpow;
  /* Set kernel functions */
  switch(a_ktype) {
  case GPSE:
    sten->kFuncs = k2dSE; break;
  case GPIMQ:
    sten->kFuncs = k2dIMQ; break;
  case GPIQ:
    sten->kFuncs = k2dIQ; break;
  default:
    sten->kFuncs = k2dMQ;
  }
  /* Substencils counters */
  sten->subSize = calloc(sten->nSub,sizeof(*sten->subSize));
  sten->subIdx = malloc(4*sizeof(*sten->subIdx));
  /* No central smaller stencil for radius 1 */
  if(sten->rad==1) { sten->subSize[1] = 0; }
  /* Fill in offsets, arrange so that central substencils are first */
  if(a_shape == GPDIAMOND) {
    sten->nCells = 2*sten->rad*sten->rad + 2*sten->rad + 1;
    sten->xOff = malloc(sten->nCells*sizeof(*sten->xOff));
    sten->yOff = malloc(sten->nCells*sizeof(*sten->yOff));
    sten->xOff[0] = 0;
    sten->yOff[0] = 0;
    int nC = 1;
    for(int nR=1; nR<=sten->rad; nR++) {
      for(int i=-nR; i<=nR; i++) {
        for(int j=-nR; j<=nR; j++) {
          if(abs(i)+abs(j) == nR) {
            sten->xOff[nC] = i;
            sten->yOff[nC] = j;
            nC++;
          }
        }
      }
      if(nR==1 && sten->rad>1) {
        sten->subSize[1] = nC;
      } else if(nR==sten->rad) {
        sten->subSize[0] = nC;
      }
    }
    /* Find remaining substencils */
    findDiamondSubStencils2D(sten);
  } else {
    sten->nCells = 0;
    double MRad = (sten->rad + 0.5)*(sten->rad + 0.5);
    for(int i=-sten->rad; i<=sten->rad; i++) {
      for(int j=-sten->rad; j<=sten->rad; j++) {
        double cRad = (double) i*i+j*j;
        if(cRad<MRad) {
          sten->nCells++;
        }
      }
    }
    sten->xOff = malloc(sten->nCells*sizeof(*sten->xOff));
    sten->yOff = malloc(sten->nCells*sizeof(*sten->yOff));
    int nC = 0;
    for(int nR=0; nR<=sten->rad; nR++) {
      double iRad = (nR - 0.5)*(nR - 0.5);
      double oRad = (nR + 0.5)*(nR + 0.5);
      for(int i=-nR; i<=nR; i++) {
        for(int j=-nR; j<=nR; j++) {
          double cRad = (double) i*i+j*j;
          if((iRad<cRad && cRad<oRad) || nR==0) {
            sten->xOff[nC] = i;
            sten->yOff[nC] = j;
            nC++;
          }
        }
      }
      if(nR==1 && sten->rad>1) {
        sten->subSize[1] = nC;
      } else if(nR==sten->rad) {
        sten->subSize[0] = nC;
      }
    }
    /* Find remaining substencils */
    findCircleSubStencils2D(sten);
  }
  printf("Stencil2D nCells = %d\n",sten->nCells);
  /* Find prediction vectors */
  sten->pdNorth = malloc(sten->nSub*sizeof(*sten->pdNorth));
  sten->pdSouth = malloc(sten->nSub*sizeof(*sten->pdSouth));
  sten->pdEast = malloc(sten->nSub*sizeof(*sten->pdEast));
  sten->pdWest = malloc(sten->nSub*sizeof(*sten->pdWest));
  
  sten->pdNorthGq = malloc(sten->nSub*sizeof(*sten->pdNorthGq));
  sten->pdSouthGq = malloc(sten->nSub*sizeof(*sten->pdSouthGq));
  sten->pdEastGq = malloc(sten->nSub*sizeof(*sten->pdEastGq));
  sten->pdWestGq = malloc(sten->nSub*sizeof(*sten->pdWestGq));
  
  sten->pdNorthGm = malloc(sten->nSub*sizeof(*sten->pdNorthGm));
  sten->pdSouthGm = malloc(sten->nSub*sizeof(*sten->pdSouthGm));
  sten->pdEastGm = malloc(sten->nSub*sizeof(*sten->pdEastGm));
  sten->pdWestGm = malloc(sten->nSub*sizeof(*sten->pdWestGm));
  
  sten->pdDerivX = malloc(sten->nSub*sizeof(*sten->pdDerivX));
  sten->pdDerivY = malloc(sten->nSub*sizeof(*sten->pdDerivY));
  sten->pdDerivXX = malloc(sten->nSub*sizeof(*sten->pdDerivXX));
  sten->pdDerivXY = malloc(sten->nSub*sizeof(*sten->pdDerivXY));
  sten->pdDerivYY = malloc(sten->nSub*sizeof(*sten->pdDerivYY));
  for(int nS=0; nS<sten->nSub; nS++) {
    sten->pdNorth[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdNorth[nS]));
    sten->pdSouth[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSouth[nS]));
    sten->pdEast[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdEast[nS]));
    sten->pdWest[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdWest[nS]));
    
    sten->pdNorthGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdNorthGq[nS]));
    sten->pdSouthGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSouthGq[nS]));
    sten->pdEastGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdEastGq[nS]));
    sten->pdWestGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdWestGq[nS]));
    
    sten->pdNorthGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdNorthGm[nS]));
    sten->pdSouthGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSouthGm[nS]));
    sten->pdEastGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdEastGm[nS]));
    sten->pdWestGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdWestGm[nS]));
    
    sten->pdDerivX[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivX[nS]));
    sten->pdDerivY[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivY[nS]));
    sten->pdDerivXX[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivXX[nS]));
    sten->pdDerivXY[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivXY[nS]));
    sten->pdDerivYY[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivYY[nS]));
    findPredVecs2D(sten,nS);
  }
  /* Allocate room for all substencil and smoothness stuff */
  sten->gamma = malloc(sten->nSub*sizeof(*sten->gamma));
  /* Fill in smoothness helpers */
  findGammas2D(sten);
  /* Return stencil struct */
  *a_sten = sten;
}

void GPStencil2DCopy(GPStencil2D src, GPStencil2D *a_sten)
{
  GPStencil2D sten = malloc(sizeof(*sten));
  sten->rad = src->rad;
  sten->nSub = src->nSub;
  sten->l = src->l;
  sten->eps = src->eps;
  sten->bpow = src->bpow;
  sten->nCells = src->nCells;
  /* Fill in offsets */
  sten->xOff = malloc(sten->nCells*sizeof(*sten->xOff));
  sten->yOff = malloc(sten->nCells*sizeof(*sten->yOff));
  for(int nC=0; nC<sten->nCells; nC++) {
    sten->xOff[nC] = src->xOff[nC];
    sten->yOff[nC] = src->yOff[nC];
  }
  /* Find substencils in terms of the global stencil offsets */
  sten->subSize = malloc(sten->nSub*sizeof(*sten->subSize));
  sten->subIdx = malloc(4*sizeof(*sten->subIdx));
  for(int nS=0; nS<sten->nSub; nS++) { sten->subSize[nS] = src->subSize[nS]; }
  for(int nS=0; nS<4; nS++) {
    sten->subIdx[nS] = malloc(sten->subSize[2+nS]*sizeof(*sten->subIdx[nS]));
    for(int nC=0; nC<sten->subSize[2+nS]; nC++) {
      sten->subIdx[nS][nC] = src->subIdx[nS][nC];
    }
  }
  /* Find local prediction vectors */
  sten->pdNorth = malloc(sten->nSub*sizeof(*sten->pdNorth));
  sten->pdSouth = malloc(sten->nSub*sizeof(*sten->pdSouth));
  sten->pdEast = malloc(sten->nSub*sizeof(*sten->pdEast));
  sten->pdWest = malloc(sten->nSub*sizeof(*sten->pdWest));
  
  sten->pdNorthGq = malloc(sten->nSub*sizeof(*sten->pdNorthGq));
  sten->pdSouthGq = malloc(sten->nSub*sizeof(*sten->pdSouthGq));
  sten->pdEastGq = malloc(sten->nSub*sizeof(*sten->pdEastGq));
  sten->pdWestGq = malloc(sten->nSub*sizeof(*sten->pdWestGq));
  
  sten->pdNorthGm = malloc(sten->nSub*sizeof(*sten->pdNorthGm));
  sten->pdSouthGm = malloc(sten->nSub*sizeof(*sten->pdSouthGm));
  sten->pdEastGm = malloc(sten->nSub*sizeof(*sten->pdEastGm));
  sten->pdWestGm = malloc(sten->nSub*sizeof(*sten->pdWestGm));
  
  sten->pdDerivX = malloc(sten->nSub*sizeof(*sten->pdDerivX));
  sten->pdDerivY = malloc(sten->nSub*sizeof(*sten->pdDerivY));
  sten->pdDerivXX = malloc(sten->nSub*sizeof(*sten->pdDerivXX));
  sten->pdDerivXY = malloc(sten->nSub*sizeof(*sten->pdDerivXY));
  sten->pdDerivYY = malloc(sten->nSub*sizeof(*sten->pdDerivYY));
  sten->gamma = malloc(sten->nSub*sizeof(*sten->gamma));
  for(int nS=0; nS<sten->nSub; nS++) {
    sten->pdNorth[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdNorth[nS]));
    sten->pdSouth[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSouth[nS]));
    sten->pdEast[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdEast[nS]));
    sten->pdWest[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdWest[nS]));
    
    sten->pdNorthGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdNorthGq[nS]));
    sten->pdSouthGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSouthGq[nS]));
    sten->pdEastGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdEastGq[nS]));
    sten->pdWestGq[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdWestGq[nS]));
    
    sten->pdNorthGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdNorthGm[nS]));
    sten->pdSouthGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdSouthGm[nS]));
    sten->pdEastGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdEastGm[nS]));
    sten->pdWestGm[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdWestGm[nS]));
    
    sten->pdDerivX[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivX[nS]));
    sten->pdDerivY[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivY[nS]));
    sten->pdDerivXX[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivXX[nS]));
    sten->pdDerivXY[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivXY[nS]));
    sten->pdDerivYY[nS] = malloc(sten->subSize[nS]*sizeof(*sten->pdDerivYY[nS]));
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      sten->pdNorth[nS][nC] = src->pdNorth[nS][nC];
      sten->pdSouth[nS][nC] = src->pdSouth[nS][nC];
      sten->pdEast[nS][nC] = src->pdEast[nS][nC];
      sten->pdWest[nS][nC] = src->pdWest[nS][nC];
      
      sten->pdNorthGq[nS][nC] = src->pdNorthGq[nS][nC];
      sten->pdSouthGq[nS][nC] = src->pdSouthGq[nS][nC];
      sten->pdEastGq[nS][nC] = src->pdEastGq[nS][nC];
      sten->pdWestGq[nS][nC] = src->pdWestGq[nS][nC];
      
      sten->pdNorthGm[nS][nC] = src->pdNorthGm[nS][nC];
      sten->pdSouthGm[nS][nC] = src->pdSouthGm[nS][nC];
      sten->pdEastGm[nS][nC] = src->pdEastGm[nS][nC];
      sten->pdWestGm[nS][nC] = src->pdWestGm[nS][nC];
      
      sten->pdDerivX[nS][nC] = src->pdDerivX[nS][nC];
      sten->pdDerivY[nS][nC] = src->pdDerivY[nS][nC];
      sten->pdDerivXX[nS][nC] = src->pdDerivXX[nS][nC];
      sten->pdDerivXY[nS][nC] = src->pdDerivXY[nS][nC];
      sten->pdDerivYY[nS][nC] = src->pdDerivYY[nS][nC];
    }
    sten->gamma[nS] = src->gamma[nS];
  }
  *a_sten = sten;
}

void DestroyGPStencil2D(GPStencil2D *a_sten)
{
  if(*a_sten) {
    GPStencil2D sten = *a_sten;
    free(sten->xOff);
    free(sten->yOff);
    free(sten->subSize);
    free(sten->gamma);
    for(int nS=0; nS<4; nS++) { free(sten->subIdx[nS]); }
    free(sten->subIdx);

    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdNorth[nS]); }
    free(sten->pdNorth);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdSouth[nS]); }
    free(sten->pdSouth);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdEast[nS]); }
    free(sten->pdEast);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdWest[nS]); }
    free(sten->pdWest);
    
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdNorthGq[nS]); }
    free(sten->pdNorthGq);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdSouthGq[nS]); }
    free(sten->pdSouthGq);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdEastGq[nS]); }
    free(sten->pdEastGq);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdWestGq[nS]); }
    free(sten->pdWestGq);
    
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdNorthGm[nS]); }
    free(sten->pdNorthGm);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdSouthGm[nS]); }
    free(sten->pdSouthGm);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdEastGm[nS]); }
    free(sten->pdEastGm);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdWestGm[nS]); }
    free(sten->pdWestGm);
    
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdDerivX[nS]); }
    free(sten->pdDerivX);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdDerivY[nS]); }
    free(sten->pdDerivY);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdDerivXX[nS]); }
    free(sten->pdDerivXX);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdDerivXY[nS]); }
    free(sten->pdDerivXY);
    for(int nS=0; nS<sten->nSub; nS++) { free(sten->pdDerivYY[nS]); }
    free(sten->pdDerivYY);

    free(*a_sten);
  }
}

/* Smoothness calculations */
static inline double normSq(int n, double vec[n])
{
  double vDv = 0.;
  for(int i=0; i<n; i++) { vDv += vec[i]*vec[i]; }
  return vDv;
}

static void GPStencil2DUpdateSmoothness(GPStencil2D sten, int nComp, double * restrict vals, double * restrict omega)
{
  /* Evaluate smoothness indicators */
  double tau[nComp]; memset(tau,0,nComp*sizeof(*tau));
  double beta[sten->nSub*nComp];
  /* Central (sub) stencils */
  for(int nS=0; nS<2; nS++) {
    double dx[nComp], dy[nComp], dxx[nComp], dxy[nComp], dyy[nComp];
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdDerivX[nS],1,0.,dx,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdDerivY[nS],1,0.,dy,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdDerivXX[nS],1,0.,dxx,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdDerivXY[nS],1,0.,dxy,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdDerivYY[nS],1,0.,dyy,1);
    for(int nC=0; nC<nComp; nC++) {
      beta[nS*nComp+nC] = dx[nC]*dx[nC] + dy[nC]*dy[nC] + dxx[nC]*dxx[nC] + dxy[nC]*dxy[nC] + dyy[nC]*dyy[nC];
      if(nS == 1) { tau[nC] = fabs(beta[nC] - beta[nS*nComp+nC]); }
    }
  }
  /* Biased sub-stencils */
  for(int nS=2; nS<sten->nSub; nS++) {
    double subVals[nComp*sten->subSize[nS]];
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      memcpy(&subVals[nComp*nC],&vals[nComp*sten->subIdx[nS-2][nC]],nComp*sizeof(double));
    }
    double dx[nComp], dy[nComp], dxx[nComp], dxy[nComp], dyy[nComp];
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdDerivX[nS],1,0.,dx,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdDerivY[nS],1,0.,dy,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdDerivXX[nS],1,0.,dxx,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdDerivXY[nS],1,0.,dxy,1);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdDerivYY[nS],1,0.,dyy,1);
    for(int nC=0; nC<nComp; nC++) {
      beta[nS*nComp+nC] = dx[nC]*dx[nC] + dy[nC]*dy[nC] + dxx[nC]*dxx[nC] + dxy[nC]*dxy[nC] + dyy[nC]*dyy[nC];
      tau[nC] += fabs(beta[nC] - beta[nS*nComp+nC]);
    }
  }
  /* Scale tau */
  for(int nC=0; nC<nComp; nC++) { tau[nC] /= 5.; }
  /* Form WENO weights */
  double tot[nComp]; memset(tot,0,nComp*sizeof(*tot));
  for(int nS=0; nS<sten->nSub; nS++) {
    for(int nC=0; nC<nComp; nC++) {
      omega[nS*nComp+nC] = sten->gamma[nS]*(1.+pow(tau[nC]/(beta[nS*nComp+nC]+sten->eps),sten->bpow));
      tot[nC] += omega[nS*nComp+nC];
    }
  }
  /* Modify weights to promote order switching */
  for(int nC=0; nC<nComp; nC++) { omega[nC] /= (tot[nC]*sten->gamma[0]); }
  for(int nS=1; nS<sten->nSub; nS++) {
    for(int nC=0; nC<nComp; nC++) {
      omega[nS*nComp+nC] = omega[nS*nComp+nC]/tot[nC] - omega[nC]*sten->gamma[nS];
    }
  }
}

/* Face evaluation functions */
void GPStencil2DEvalX(GPStencil2D sten, int nComp, double * restrict vals, double * restrict eastVal, double * restrict westVal)
{
  /* Evaluate smoothness indicators */
  double omega[sten->nSub*nComp];
  GPStencil2DUpdateSmoothness(sten, nComp, vals, omega);
  /* Zero out */
  memset(eastVal,0,nComp*sizeof(*eastVal));
  memset(westVal,0,nComp*sizeof(*westVal));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<2; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdEast[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,eastVal);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdWest[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,westVal);
  }
  /* Evaluate biased predictions */
  for(int nS=2; nS<sten->nSub; nS++) {
    double subVals[nComp*sten->subSize[nS]];
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      memcpy(&subVals[nComp*nC],&vals[nComp*sten->subIdx[nS-2][nC]],nComp*sizeof(double));
    }
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdEast[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,eastVal);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdWest[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,westVal);
  }
}

void GPStencil2DEvalY(GPStencil2D sten, int nComp, double * restrict vals, double * restrict northVal, double * restrict southVal)
{
  /* Evaluate smoothness indicators */
  double omega[sten->nSub*nComp];
  GPStencil2DUpdateSmoothness(sten, nComp, vals, omega);
  /* Zero out */
  memset(northVal,0,nComp*sizeof(*northVal));
  memset(southVal,0,nComp*sizeof(*southVal));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<2; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdNorth[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,northVal);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdSouth[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,southVal);
  }
  /* Evaluate biased predictions */
  for(int nS=2; nS<sten->nSub; nS++) {
    double subVals[nComp*sten->subSize[nS]];
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      memcpy(&subVals[nComp*nC],&vals[nComp*sten->subIdx[nS-2][nC]],nComp*sizeof(double));
    }
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdNorth[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,northVal);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdSouth[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,southVal);
  }
}

void GPStencil2DEvalGqX(GPStencil2D sten, int nComp, double * restrict vals,
                        double * restrict eastValP, double * restrict eastValM,
                        double * restrict westValP, double * restrict westValM)
{
  /* Evaluate smoothness indicators */
  double omega[sten->nSub*nComp];
  GPStencil2DUpdateSmoothness(sten, nComp, vals, omega);
  /* Zero out */
  memset(eastValP,0,nComp*sizeof(*eastValP));
  memset(eastValM,0,nComp*sizeof(*eastValM));
  memset(westValP,0,nComp*sizeof(*westValP));
  memset(westValM,0,nComp*sizeof(*westValM));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<2; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdEastGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,eastValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdEastGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,eastValM);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdWestGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,westValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdWestGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,westValM);
  }
  /* Evaluate biased predictions */
  for(int nS=2; nS<sten->nSub; nS++) {
    double subVals[nComp*sten->subSize[nS]];
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      memcpy(&subVals[nComp*nC],&vals[nComp*sten->subIdx[nS-2][nC]],nComp*sizeof(double));
    }
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdEastGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,eastValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdEastGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,eastValM);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdWestGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,westValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdWestGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,westValM);
  }
}

void GPStencil2DEvalGqY(GPStencil2D sten, int nComp, double * restrict vals,
                        double * restrict northValP, double * restrict northValM,
                        double * restrict southValP, double * restrict southValM)
{
  /* Evaluate smoothness indicators */
  double omega[sten->nSub*nComp];
  GPStencil2DUpdateSmoothness(sten, nComp, vals, omega);
  /* Zero out */
  memset(northValP,0,nComp*sizeof(*northValP));
  memset(northValM,0,nComp*sizeof(*northValM));
  memset(southValP,0,nComp*sizeof(*southValP));
  memset(southValM,0,nComp*sizeof(*southValM));
  /* Evaluate central predictions */
  double tmp[nComp];
  for(int nS=0; nS<2; nS++) {
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdNorthGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,northValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdNorthGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,northValM);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdSouthGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,southValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,vals,nComp,sten->pdSouthGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,southValM);
  }
  /* Evaluate biased predictions */
  for(int nS=2; nS<sten->nSub; nS++) {
    double subVals[nComp*sten->subSize[nS]];
    for(int nC=0; nC<sten->subSize[nS]; nC++) {
      memcpy(&subVals[nComp*nC],&vals[nComp*sten->subIdx[nS-2][nC]],nComp*sizeof(double));
    }
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdNorthGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,northValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdNorthGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,northValM);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdSouthGq[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,southValP);
    c_dgemv('n',nComp,sten->subSize[nS],1.,subVals,nComp,sten->pdSouthGm[nS],1,0.,tmp,1);
    diag_gemv(nComp,&omega[nS*nComp],tmp,southValM);
  }
}

/* Prediction vector helper */
static void findPredVecs2D(GPStencil2D sten, int nSub)
{
  int nCells = sten->subSize[nSub];
  /* Turn stencil into double prec coords */
  double x[nCells], y[nCells];
  for(int n=0; n<nCells; n++) {
    int i = nSub<2 ? n : sten->subIdx[nSub-2][n];
    x[n] = ((double) sten->xOff[i]);
    y[n] = ((double) sten->yOff[i]);
  }
  /* Face centered prediction */
  findPredictor(nCells,sten->l,0.,0.5,x,y,sten->pdNorth[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,0.5,0.,x,y,sten->pdEast[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,0.,-0.5,x,y,sten->pdSouth[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,-0.5,0.,x,y,sten->pdWest[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  /* Gauss quadrature positive pt */
  findPredictor(nCells,sten->l,1./(2.*sqrt(3.)),0.5,x,y,sten->pdNorthGq[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,0.5,1./(2.*sqrt(3.)),x,y,sten->pdEastGq[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,1./(2.*sqrt(3.)),-0.5,x,y,sten->pdSouthGq[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,-0.5,1./(2.*sqrt(3.)),x,y,sten->pdWestGq[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  /* Gauss quadrature minus pt */
  findPredictor(nCells,sten->l,-1./(2.*sqrt(3.)),0.5,x,y,sten->pdNorthGm[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,0.5,-1./(2.*sqrt(3.)),x,y,sten->pdEastGm[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,-1./(2.*sqrt(3.)),-0.5,x,y,sten->pdSouthGm[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  findPredictor(nCells,sten->l,-0.5,-1./(2.*sqrt(3.)),x,y,sten->pdWestGm[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Pnt,NULL);
  /* Derivative prediction */
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdDerivX[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Deriv[0],NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdDerivY[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2Deriv[1],NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdDerivXX[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2SecDeriv[0],NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdDerivXY[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2SecDeriv[1],NULL);
  findPredictor(nCells,sten->l,0.,0.,x,y,sten->pdDerivYY[nSub],
                sten->kFuncs.covInt,sten->kFuncs.smpAv2SecDeriv[2],NULL);
}

/* Substencil helpers */
static void findDiamondSubStencils2D(GPStencil2D sten)
{
  /* Count number of cells in each substencil */
  int bRad = sten->rad>=2 ? 2 : 1;
  for(int n=0; n<sten->nCells; n++) {
    int x=sten->xOff[n], y=sten->yOff[n];
    if(abs(x)+abs(y)<=bRad) {
      /* North biased */
      if(y>=abs(x)) { sten->subSize[2]++; }
      /* South biased */
      if(-y>=abs(x)) { sten->subSize[3]++; }
      /* East biased */
      if(x>=abs(y)) { sten->subSize[4]++; }
      /* West biased */
      if(-x>=abs(y)) { sten->subSize[5]++; }
    }
  }
  for(int nS=0; nS<4; nS++) {
    sten->subIdx[nS] = malloc(sten->subSize[nS+2]*sizeof(*sten->subIdx[nS+2]));
  }
  int nN=0, nS=0, nE=0, nW=0;
  for(int n=0; n<sten->nCells; n++) {
    int x=sten->xOff[n], y=sten->yOff[n];
    if(abs(x)+abs(y)<=bRad) {
      /* North biased */
      if(y>=abs(x)) { sten->subIdx[0][nN++] = n; }
      /* South biased */
      if(-y>=abs(x)) { sten->subIdx[1][nS++] = n; }
      /* East biased */
      if(x>=abs(y)) { sten->subIdx[2][nE++] = n; }
      /* West biased */
      if(-x>=abs(y)) { sten->subIdx[3][nW++] = n; }
    }
  }
}

static void findCircleSubStencils2D(GPStencil2D sten)
{
  /* Count number of cells in each substencil */
  double bRad = (sten->rad+0.5)*(sten->rad+0.5);
  for(int n=0; n<sten->nCells; n++) {
    int x=sten->xOff[n], y=sten->yOff[n];
    double cRad = (double) x*x + y*y;
    if(cRad<bRad) {
      /* North biased */
      if(y>=abs(x)) { sten->subSize[2]++; }
      /* South biased */
      if(-y>=abs(x)) { sten->subSize[3]++; }
      /* East biased */
      if(x>=abs(y)) { sten->subSize[4]++; }
      /* West biased */
      if(-x>=abs(y)) { sten->subSize[5]++; }
    }
  }
  for(int nS=0; nS<4; nS++) {
    sten->subIdx[nS] = malloc(sten->subSize[nS+2]*sizeof(*sten->subIdx[nS+2]));
  }
  int nN=0, nS=0, nE=0, nW=0;
  for(int n=0; n<sten->nCells; n++) {
    int x=sten->xOff[n], y=sten->yOff[n];
    double cRad = (double) x*x + y*y;
    if(cRad<bRad) {
      /* North biased */
      if(y>=abs(x)) { sten->subIdx[0][nN++] = n; }
      /* South biased */
      if(-y>=abs(x)) { sten->subIdx[1][nS++] = n; }
      /* East biased */
      if(x>=abs(y)) { sten->subIdx[2][nE++] = n; }
      /* West biased */
      if(-x>=abs(y)) { sten->subIdx[3][nW++] = n; }
    }
  }
}

/* Optimal weight formulation */
void findGammas2D(GPStencil2D sten)
{
  double gHi = 0.8;
  /* Recall, no small central substencil for radius 1 */
  double gLo = sten->rad>1 ? 0.8 : 0.0;
  /* Set just the CWENO weights of the low order scheme */
  sten->gamma[1] = gLo;
  for(int nS=2; nS<sten->nSub; nS++) {
    sten->gamma[nS] = (1.-gLo)/4.;
  }
  /* Large central stencil */
  sten->gamma[0] = gHi;
  for(int nS=1; nS<sten->nSub; nS++) {
    sten->gamma[nS] *= (1-gHi);
  }
}
