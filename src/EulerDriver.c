/* File: EulerDriver.c
 * Author: Ian May
 * Purpose: General purpose driver for the GP-WENO code that will initialize
 *          from an input file and CLI arguments
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "Options.h"
#include "Mesh.h"
#include "Vec.h"
#include "Riemann.h"
#include "Stencil.h"
#include "Solver.h"
#include "SolWriter.h"

int main(int argc, char **argv)
{
  /* Get input file and call routine to fill the options struct */
  Options opt;
  char infile[50];
  int startArg = 1;
  if(argc>2) {
    if(0 == strcmp(argv[1],"-infile")) {
      snprintf(infile,50,"%s",argv[2]);
      startArg = 3;
    } else {
      sprintf(infile,"default.init");
    }
    printf("Using input file: %s\n", infile);
  }
  OptionsSetup(infile,startArg,argc,argv,&opt);
  if(opt.pp_verbose>0) { OptionsPrint(&opt); }
  /* Create mesh, stencil, and solver contexts */
  Mesh mesh;
  CreateMesh(opt.gr_nx,opt.gr_ny,opt.gp_rad,
             opt.gr_xlo,opt.gr_xup,opt.gr_ylo,opt.gr_yup,
             opt.bc_n,opt.bc_e,opt.bc_s,opt.bc_w,
             opt.bc_nSub,opt.bc_eSub,opt.bc_sSub,opt.bc_wSub,
             opt.bc_northTr,opt.bc_eastTr,opt.bc_southTr,opt.bc_westTr,&mesh);
  GPStencil1D sten1D=NULL;
  GPStencil2D sten2D=NULL;
  CreateGPStencil1D(opt.gp_rad,opt.gp_lFac,opt.gp_eps,opt.gp_betaPow,opt.gp_kernel,&sten1D);
  if(opt.gp_shape != GPDD) {
    CreateGPStencil2D(opt.gp_rad,opt.gp_lFac,opt.gp_eps,opt.gp_betaPow,opt.gp_shape,opt.gp_kernel,&sten2D);
  }
  GPSolver solver;
  CreateGPSolver(mesh,opt.eqType,opt.so_dt,opt.so_tf,opt.so_nProp,opt.so_fProp,
                 opt.so_fluxRec,opt.so_gaussFlux,opt.so_polyWeno,opt.so_bhFlux,
                 sten1D,sten2D,opt.so_rsx,opt.so_rsy,opt.so_ccx,opt.so_ccy,
                 opt.so_src,opt.so_tsType,&solver);
  SolWriter writer;
  const char *varNames[] = {"/dens","/momx","/momy","/momz","/etot"};
  const char *auxNames[] = {"/velx","/vely","/velz","/pres","/eint","/sdsp"};
  char resStr[8];
  sprintf(resStr,"%04d_%d_", opt.gr_nx,opt.gp_rad);
  strcat(opt.pp_fBase,resStr);
  CreateSolWriter(mesh,opt.so_nComp,solver->nAux,opt.pp_fBase,varNames,auxNames,&writer);
  /* Set IBCs */
  GPSolverSetIC(solver, opt.so_ic);
  GPSolverSetNorthBC(solver, opt.so_bcN);
  GPSolverSetEastBC(solver, opt.so_bcE);
  GPSolverSetSouthBC(solver, opt.so_bcS);
  GPSolverSetWestBC(solver, opt.so_bcW);
  /* Save initial condition for error testing if needed */
  CellVec Uinit=NULL, Auxinit=NULL;
  if(opt.pp_compLast) {
    CellVecDuplicate(solver->U, &Uinit);
    CellVecDuplicate(solver->auxVar, &Auxinit);
  }
  /* Solve */
  for(int nT=0; nT<opt.so_nStep; nT++) {
    if(nT%opt.pp_plotFreq == 0) {
      SolWriterOutput(writer, mesh, solver->U, solver->auxVar, nT, solver->t);
    }
    bool done = false;
    if(opt.so_tsType==RK2) {
      done = GPSolverRK2Step(solver,opt.so_cfl);
    } else if(opt.so_tsType==RK3) {
      done = GPSolverRK3Step(solver,opt.so_cfl);
    } else if(opt.so_tsType==RK4) {
      done = GPSolverRK4Step(solver,opt.so_cfl);
    } else if(opt.so_tsType==RK7) {
      done = GPSolverRK7Step(solver,opt.so_cfl);
    }
    if(opt.pp_verbose>1) {
      printf("Solver time = %e   dt = %e\n", solver->t, solver->dt/2);
    }
    if(done) {
      SolWriterOutput(writer, mesh, solver->U, solver->auxVar, nT+1, solver->t);
      break;
    }
  }
  /* Report errors if desired */
  if(opt.pp_compLast) {
    double l1Err=0, linfErr=0;
    for(int nX=0; nX<Uinit->nX; nX++) {
      for(int nY=0; nY<Uinit->nY; nY++) {
        for(int nC=0; nC<Uinit->nC; nC++) {
          Uinit->v[cellLex(Uinit,nX,nY)+nC] -= solver->U->v[cellLex(Uinit,nX,nY)+nC];
        }
        if(Auxinit) {
          for(int nC=0; nC<Auxinit->nC; nC++) {
            Auxinit->v[cellLex(Auxinit,nX,nY)+nC] -= solver->auxVar->v[cellLex(Auxinit,nX,nY)+nC];
          }
        }
        double diff = fabs(Uinit->v[cellLex(Uinit,nX,nY)]); /* Density */
        l1Err += diff;
        linfErr = linfErr<diff ? diff : linfErr;
      }
    }
    /* Write error information out */
    printf("L1 error (density): %e, Linf error (density): %e, for (dx,dy)=(%e,%e)\n",
           l1Err*mesh->dx*mesh->dy, linfErr, mesh->dx, mesh->dy);
    SolWriterOutput(writer, mesh, Uinit, Auxinit, -1, solver->t);
    FILE *fConv = fopen(opt.pp_convFile,"a");
    fprintf(fConv,"%d %d %e %e %e\n",
            opt.gr_nx,opt.gp_rad,opt.gp_lFac*mesh->dx,l1Err*mesh->dx*mesh->dy,linfErr);
    fclose(fConv);
  }
  /* Clean up everything */
  DestroySolWriter(&writer);
  DestroyCellVec(&Uinit);
  DestroyCellVec(&Auxinit);
  DestroyGPSolver(&solver);
  DestroyGPStencil1D(&sten1D);
  DestroyGPStencil2D(&sten2D);
  DestroyMesh(&mesh);
  return 0;
}
