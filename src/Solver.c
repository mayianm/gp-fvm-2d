/* File: Solver.c
 * Author: Ian May
 * Purpose: Combine mesh, stencil, and riemann solvers to evolve a solution through
 *          time.
 */

#define MIN(a,b) (a)<(b) ? (a) : (b)
#define FABMIN(a,b) fabs(a)<fabs(b) ? (a) : (b)
#define MAX(a,b) (a)>(b) ? (a) : (b)
#define FABMAX(a,b) fabs(a)>fabs(b) ? (a) : (b)

#define OMPCHUNK 16

#include <omp.h>
#include <string.h>

#include "definition.h"
#include "LapackWrappers.h"
#include "Vec.h"
#include "EOS.h"
#include "ConsChar.h"
#include "Solver.h"

static double gl_ab5[] = {-0.906179845938663992797627,
                          -0.538469310105683091036314,
                           0.,
                           0.538469310105683091036314,
                           0.906179845938663992797627};

static double gl_wt5[] =  {0.236926885056189087514264,
                           0.478628670499366468041292,
                           0.568888888888888888888889,
                           0.478628670499366468041292,
                           0.236926885056189087514264};

void CreateGPSolver(Mesh a_mesh, EQType a_eqType, double a_dt, double a_tF,
                    int a_nProp, double *a_fProp,
                    bool a_fluxRec, bool a_gaussFlux, bool a_polyWeno, bool a_bhFlux,
                    GPStencil1D a_sten1D, GPStencil2D a_sten2D,
                    double (*a_riemX)(double*,double*,double*,double*),
                    double (*a_riemY)(double*,double*,double*,double*),
                    void (*a_charSysX)(double*,double*,double*,double*),
                    void (*a_charSysY)(double*,double*,double*,double*),
                    void (*a_srcFunc)(double,double,double*,double*),
                    TSType tsType, GPSolver *a_solver)
{
  GPSolver solver = malloc(sizeof(*solver));
  solver->eqType = a_eqType;
  solver->nC = solver->eqType==ADVECT ? 1 : (solver->eqType==EULER ? 5 : 8);
  solver->nAux = solver->eqType==EULER ? 6 : 0;
  solver->maxThreads = omp_get_max_threads();
  solver->dt = a_dt;
  solver->tF = a_tF;
  solver->t = 0;
  solver->fluxRec = a_fluxRec;
  solver->gaussFlux = a_gaussFlux;
  solver->polyWeno = a_polyWeno;
  solver->bhFlux = a_bhFlux;
  solver->mesh = a_mesh;
  solver->h = solver->mesh->dx<solver->mesh->dy ? solver->mesh->dx : solver->mesh->dy;
  solver->sten1D = a_sten1D;
  solver->sten2D = a_sten2D;
  solver->auxVar = NULL;
  solver->dimByDim = a_sten2D==NULL;
  if(a_nProp>0) {
    solver->fProp = malloc(a_nProp*sizeof(*solver->fProp));
  } else {
    solver->fProp = NULL;
  }
  for(int nP=0; nP<a_nProp; nP++) { solver->fProp[nP] = a_fProp[nP]; }
  solver->riemX = a_riemX;
  solver->riemY = a_riemY;
  solver->charSysX = a_charSysX;
  solver->charSysY = a_charSysY;
  solver->srcFunc = a_srcFunc;
  solver->northBC = NULL;
  solver->southBC = NULL;
  solver->eastBC = NULL;
  solver->westBC = NULL;
  /* Allocate solution vectors */
  CreateCellVec(solver->mesh, solver->nC, &solver->U);
  CreateCellVec(solver->mesh, solver->nC, &solver->K);
  CreateCellVec(solver->mesh, solver->nC, &solver->Kbody);
  CreateFaceVecD(solver->mesh, solver->nC, &solver->Ufa);
  CreateFaceVecC(solver->mesh, solver->nC, &solver->Favg);
  if(solver->fluxRec) {
    CreateFaceVecC(solver->mesh, solver->nC, &solver->Fpnt);
    CreateFaceVecD(solver->mesh, solver->nC, &solver->Ufp);
    if(solver->gaussFlux) {
      CreateFaceVecC(solver->mesh, solver->nC, &solver->FpntGm);
      CreateFaceVecD(solver->mesh, solver->nC, &solver->UfpGm);
    } else {
      solver->FpntGm = NULL; solver->UfpGm = NULL;
    }
  } else {
    solver->Fpnt = NULL; solver->Ufp = NULL;
    solver->FpntGm = NULL; solver->UfpGm = NULL;
  }
  if(solver->nAux>0) {
    CreateCellVec(solver->mesh, solver->nAux, &solver->auxVar);
  } else {
    solver->auxVar = NULL;
  }
  if(tsType == RK4) {
    CreateCellVec(solver->mesh, solver->nC, &solver->K1);
    CreateCellVec(solver->mesh, solver->nC, &solver->K2);
    CreateCellVec(solver->mesh, solver->nC, &solver->K3);
    CreateCellVec(solver->mesh, solver->nC, &solver->K4);
  } else {
    solver->K1 = NULL; solver->K2 = NULL; solver->K3 = NULL; solver->K4 = NULL;
  }
  if(tsType == RK7) {
    for(int nK=0; nK<11; nK++) {
      CreateCellVec(solver->mesh, solver->nC, &solver->K7s[nK]);
    }
  } else {
    for(int nK=0; nK<11; nK++) { solver->K7s[nK] = NULL; }
  }

  *a_solver = solver;
}

void DestroyGPSolver(GPSolver *a_solver)
{
  if(*a_solver) {
    GPSolver solver = *a_solver;
    if(solver->fProp) { free(solver->fProp); }
    DestroyCellVec(&solver->U);
    DestroyFaceVecC(&solver->Favg);
    DestroyFaceVecC(&solver->Fpnt);
    DestroyFaceVecC(&solver->FpntGm);
    DestroyCellVec(&solver->auxVar);
    DestroyCellVec(&solver->K);
    DestroyCellVec(&solver->Kbody);
    DestroyCellVec(&solver->K1);
    DestroyCellVec(&solver->K2);
    DestroyCellVec(&solver->K3);
    DestroyCellVec(&solver->K4);
    for(int nK=0; nK<11; nK++) {
      DestroyCellVec(&solver->K7s[nK]);
    }
    DestroyFaceVecD(&solver->Ufa);
    DestroyFaceVecD(&solver->Ufp);
    DestroyFaceVecD(&solver->UfpGm);
    free(*a_solver);
  }
}

static void updateAux(GPSolver solver, CellVec U)
{
  CellVec aux = solver->auxVar;
  int nX=0,nY=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX,nY)
  for(nX=0; nX<U->nX; nX++) {
    for(nY=0; nY<U->nY; nY++) {
      int uidx = cellLex(U,nX,nY), aidx = cellLex(aux,nX,nY);
      aux->v[aidx+AUX_VELX] = U->v[uidx+VAR_MOMX]/U->v[uidx+VAR_DENS];
      aux->v[aidx+AUX_VELY] = U->v[uidx+VAR_MOMY]/U->v[uidx+VAR_DENS];
      aux->v[aidx+AUX_VELZ] = U->v[uidx+VAR_MOMZ]/U->v[uidx+VAR_DENS];
      aux->v[aidx+AUX_PRES] = igl_pres(solver->fProp[0],&U->v[uidx]);
      aux->v[aidx+AUX_EINT] = igl_eint(&U->v[uidx]);
      aux->v[aidx+AUX_SPSD] = igl_soundspeed(solver->fProp[PROP_GAM],&U->v[uidx]);
    }
  }
}

void GPSolverSetIC(GPSolver solver, void (*ic)(double,double,double*,double*))
{
  Mesh mesh = solver->mesh;
  double **work = malloc(solver->maxThreads*sizeof(*work));
  for(int nT=0; nT<solver->maxThreads; nT++) {
    work[nT] = malloc(solver->nC*sizeof(*work[nT]));
  }
  int nX=0, tIdx=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX,tIdx)
  for(nX=0; nX<mesh->nX; nX++) {
    tIdx = omp_get_thread_num();
    double xc = mesh->xLo + (((double) nX)+0.5)*mesh->dx;
    for(int nY=0; nY<mesh->nY; nY++) {
      double yc = mesh->yLo + (((double) nY)+0.5)*mesh->dy;
      int idx = cellLex(solver->U,nX,nY);
      for(int nC=0; nC<solver->nC; nC++) { solver->U->v[idx+nC] = 0; }
      for(int abX=0; abX<5; abX++) {
        double x = xc + gl_ab5[abX]*mesh->dx/2;
        for(int abY=0; abY<5; abY++) {
          double y = yc + gl_ab5[abY]*mesh->dy/2;
          ic(x,y,solver->fProp,work[tIdx]);
          for(int nC=0; nC<solver->nC; nC++) {
            solver->U->v[idx+nC] += work[tIdx][nC]*gl_wt5[abX]*gl_wt5[abY]/4.;
          }
        }
      }
    }
  }
  for(int nT=0; nT<solver->maxThreads; nT++) { free(work[nT]); }
  free(work);
  if(solver->nAux>0) { updateAux(solver,solver->U); }
}

void GPSolverSetNorthBC(GPSolver solver, void(*a_northBC)(double,double,double,double*,double*))
{
  solver->northBC = a_northBC;
}

void GPSolverSetSouthBC(GPSolver solver, void(*a_southBC)(double,double,double,double*,double*))
{
  solver->southBC = a_southBC;
}

void GPSolverSetEastBC(GPSolver solver, void(*a_eastBC)(double,double,double,double*,double*))
{
  solver->eastBC = a_eastBC;
}

void GPSolverSetWestBC(GPSolver solver, void(*a_westBC)(double,double,double,double*,double*))
{
  solver->westBC = a_westBC;
}

/* A load of cellvec halo helpers */
/* Periodic exchanges */
static void bc_snPeriodic(GPSolver solver, CellVec U, bool halo)
{
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=-U->nHalo; nY<0; nY++) {
    for(int nX=xStart; nX<xEnd; nX++) {
      int sDst = cellLex(U,nX,nY);
      int sSrc = cellLex(U,nX,U->nY+nY);
      int nDst = cellLex(U,nX,U->nY+U->nHalo+nY);
      int nSrc = cellLex(U,nX,U->nHalo+nY);
      for(int nC=0; nC<solver->nC; nC++) {
        U->v[sDst+nC] = U->v[sSrc+nC];
        U->v[nDst+nC] = U->v[nSrc+nC];
      }
    }
  }
}
static void bc_wePeriodic(GPSolver solver, CellVec U, bool halo)
{
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=-U->nHalo; nX<0; nX++) {
    for(int nY=yStart; nY<yEnd; nY++) {
      int wDst=cellLex(U,nX,nY);
      int wSrc=cellLex(U,U->nX+nX,nY);
      int eDst=cellLex(U,U->nX+U->nHalo+nX,nY);
      int eSrc=cellLex(U,U->nHalo+nX,nY);
      for(int nC=0; nC<solver->nC; nC++) {
        U->v[wDst+nC] = U->v[wSrc+nC];
        U->v[eDst+nC] = U->v[eSrc+nC];
      }
    }
  }
}

/* Inflow conditions */
static void bc_wInflow(GPSolver solver, CellVec U, double t, double yMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=-U->nHalo; nX<0; nX++) {
    double x = mesh->xLo+((double) nX + 0.5)*mesh->dx;
    for(int nY=yStart; nY<yEnd; nY++) {
      double y = mesh->yLo+((double) nY + 0.5)*mesh->dy;
      if(y>yMin) {
        solver->westBC(x,y,t,solver->fProp,&U->v[cellLex(U,nX,nY)]);
      }
    }
  }
}

static void bc_eInflow(GPSolver solver, CellVec U, double t, double yMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=0; nX<U->nHalo; nX++) {
    double x = ((double) nX + 0.5)*mesh->dx;
    for(int nY=yStart; nY<yEnd; nY++) {
      double y = ((double) nY + 0.5)*mesh->dy;
      if(y>yMin) {
        solver->eastBC(x,y,t,solver->fProp,&U->v[cellLex(U,U->nX+nX,nY)]);
      }
    }
  }
}

static void bc_sInflow(GPSolver solver, CellVec U, double t, double xMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=-U->nHalo; nY<0; nY++) {
    double y = ((double) nY + 0.5)*mesh->dy;
    for(int nX=xStart; nX<xEnd; nX++) {
      double x = ((double) nX + 0.5)*mesh->dx;
      if(x>xMin) {
        solver->southBC(x,y,t,solver->fProp,&U->v[cellLex(U,nX,nY)]);
      }
    }
  }
}

static void bc_nInflow(GPSolver solver, CellVec U, double t, double xMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=0; nY<U->nHalo; nY++) {
    double y = ((double) nY + 0.5)*mesh->dy;
    for(int nX=xStart; nX<xEnd; nX++) {
      double x = ((double) nX + 0.5)*mesh->dx;
      if(x>xMin) {
        solver->northBC(x,y,t,solver->fProp,&U->v[cellLex(U,nX,U->nY+nY)]);
      }
    }
  }
}
/* Outflow conditions */
static void bc_wOutflow(GPSolver solver, CellVec U, double yMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=0; nX<U->nHalo; nX++) {
    for(int nY=yStart; nY<yEnd; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int dst=cellLex(U,-nX-1,nY);
        int src=cellLex(U,0,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC];
        }
      }
    }
  }
}

static void bc_eOutflow(GPSolver solver, CellVec U, double yMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=0; nX<U->nHalo; nX++) {
    for(int nY=yStart; nY<yEnd; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int dst=cellLex(U,U->nX+nX,nY);
        int src=cellLex(U,U->nX-1,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC];
        }
      }
    }
  }
}

static void bc_sOutflow(GPSolver solver, CellVec U, double xMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=0; nY<U->nHalo; nY++) {
    for(int nX=xStart; nX<xEnd; nX++) {
      if(((double) nX + 0.5)*mesh->dx>xMin) {
        int dst=cellLex(U,nX,-nY-1);
        int src=cellLex(U,nX,0);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC];
        }
      }
    }
  }
}

static void bc_nOutflow(GPSolver solver, CellVec U, double xMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=0; nY<U->nHalo; nY++) {
    for(int nX=xStart; nX<xEnd; nX++) {
      if(((double) nX + 0.5)*mesh->dx>xMin) {
        int dst=cellLex(U,nX,U->nY+nY);
        int src=cellLex(U,nX,U->nY-1);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC];
        }
      }
    }
  }
}
/* Reflecting conditions */
static void bc_wReflect(GPSolver solver, CellVec U, double yMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=0; nX<U->nHalo; nX++) {
    for(int nY=yStart; nY<yEnd; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int dst=cellLex(U,-nX-1,nY);
        int src=cellLex(U,0,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC]*(nC==VAR_MOMX ? -1 : 1);
        }
      }
    }
  }
}

static void bc_eReflect(GPSolver solver, CellVec U, double yMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int yStart = halo ? -U->nHalo : 0;
  int yEnd = halo ? U->nY+U->nHalo : U->nY;
  for(int nX=0; nX<U->nHalo; nX++) {
    for(int nY=yStart; nY<yEnd; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int dst=cellLex(U,U->nX+nX,nY);
        int src=cellLex(U,U->nX-1,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC]*(nC==VAR_MOMX ? -1 : 1);
        }
      }
    }
  }
}

static void bc_sReflect(GPSolver solver, CellVec U, double xMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=0; nY<U->nHalo; nY++) {
    for(int nX=xStart; nX<xEnd; nX++) {
      if(((double) nX + 0.5)*mesh->dx > xMin) {
        int dst=cellLex(U,nX,-nY-1);
        int src=cellLex(U,nX,0);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC]*(nC==VAR_MOMY ? -1 : 1);
        }
      }
    }
  }
}

static void bc_nReflect(GPSolver solver, CellVec U, double xMin, bool halo)
{
  Mesh mesh = solver->mesh;
  int xStart = halo ? -U->nHalo : 0;
  int xEnd = halo ? U->nX+U->nHalo : U->nX;
  for(int nY=0; nY<U->nHalo; nY++) {
    for(int nX=xStart; nX<xEnd; nX++) {
      if(((double) nX + 0.5)*mesh->dx>xMin) {
        int dst=cellLex(U,nX,U->nY+nY);
        int src=cellLex(U,nX,U->nY-1);
        for(int nC=0; nC<solver->nC; nC++) {
          U->v[dst+nC] = U->v[src+nC]*(nC==VAR_MOMY ? -1 : 1);
        }
      }
    }
  }
}
/* Wrappers to call right BCs on right parts of boundaries */
static void cellWBC(GPSolver solver, CellVec U, double t, double yMin, bool halo, BCType bcType)
{
  if(bcType==PERIODIC) {
    bc_wePeriodic(solver,U,halo);
  } else if(bcType==INFLOW) {
    bc_wInflow(solver,U,t,yMin,halo);
  } else if(bcType==OUTFLOW) {
    bc_wOutflow(solver,U,yMin,halo);
  } else if(bcType==REFLECTING) {
    bc_wReflect(solver,U,yMin,halo);
  } else if(bcType==MIXED) {
    cellWBC(solver,U,t,yMin,halo,solver->mesh->bcWestSub[0]);
    cellWBC(solver,U,t,solver->mesh->bcWestTr,halo,solver->mesh->bcWestSub[1]);
  } else {
    printf("Unrecognized West boundary condition\n");
  }
}
static void cellEBC(GPSolver solver, CellVec U, double t, double yMin, bool halo, BCType bcType)
{
  if(bcType==INFLOW) {
    bc_eInflow(solver,U,t,yMin,halo);
  } else if(bcType==OUTFLOW) {
    bc_eOutflow(solver,U,yMin,halo);
  } else if(bcType==REFLECTING) {
    bc_eReflect(solver,U,yMin,halo);
  } else if(bcType==MIXED) {
    cellEBC(solver,U,t,yMin,halo,solver->mesh->bcEastSub[0]);
    cellEBC(solver,U,t,solver->mesh->bcEastTr,halo,solver->mesh->bcEastSub[1]);
  } else if(bcType!=PERIODIC) {
    printf("Unrecognized East boundary condition\n");
  }
}
static void cellSBC(GPSolver solver, CellVec U, double t, double xMin, bool halo, BCType bcType)
{
  Mesh mesh = solver->mesh;
  if(bcType==INFLOW) {
    bc_sInflow(solver,U,t,xMin,halo);
  } else if(bcType==OUTFLOW) {
    bc_sOutflow(solver,U,xMin,halo);
  } else if(bcType==REFLECTING) {
    bc_sReflect(solver,U,xMin,halo);
  } else if(bcType==MIXED) {
    cellSBC(solver,U,t,xMin,halo,mesh->bcSouthSub[0]);
    cellSBC(solver,U,t,mesh->bcSouthTr,halo,mesh->bcSouthSub[1]);
  } else {
    printf("Unrecognized South boundary condition\n");
  }
}
static void cellNBC(GPSolver solver, CellVec U, double t, double xMin, bool halo, BCType bcType)
{
  Mesh mesh = solver->mesh;
  if(bcType==INFLOW) {
    bc_nInflow(solver,U,t,xMin,halo);
  } else if(bcType==OUTFLOW) {
    bc_nOutflow(solver,U,xMin,halo);
  } else if(bcType==REFLECTING) {
    bc_nReflect(solver,U,xMin,halo);
  } else if(bcType==MIXED) {
    cellNBC(solver,U,t,xMin,halo,mesh->bcNorthSub[0]);
    cellNBC(solver,U,t,mesh->bcNorthTr,halo,mesh->bcNorthSub[1]);
  } else {
    printf("Unrecognized South boundary condition\n");
  }
}
/* Call right BC functions in the right order */
static void cellVecHalo(GPSolver solver, CellVec U, double t)
{
  Mesh mesh = solver->mesh;
  double xMin = mesh->xLo-mesh->xUp;
  double yMin = mesh->yLo-mesh->yUp;
  /* Have to check periodicity first to know order for halo regions */
  if(mesh->bcSouth==PERIODIC) {
    cellWBC(solver,U,t,yMin,false,mesh->bcWest);
    cellEBC(solver,U,t,yMin,false,mesh->bcEast);
    bc_snPeriodic(solver,U,true);
  } else if(mesh->bcWest==PERIODIC) {
    cellSBC(solver,U,t,xMin,false,mesh->bcSouth);
    cellNBC(solver,U,t,xMin,false,mesh->bcNorth);
    bc_wePeriodic(solver,U,true);
  } else {
    cellSBC(solver,U,t,xMin,false,mesh->bcSouth);
    cellNBC(solver,U,t,xMin,false,mesh->bcNorth);
    cellWBC(solver,U,t,yMin,true,mesh->bcWest);
    cellEBC(solver,U,t,yMin,true,mesh->bcEast);
  }
}

static void faceBCN(GPSolver solver, Mesh mesh, FaceVecD F, double t, double xMin, BCType bcType)
{
  if(bcType==INFLOW) {
    for(int nX=0; nX<mesh->nX; nX++) {
      double x = ((double) nX + 0.5)*mesh->dx;
      if(x>xMin) {
        solver->northBC(x,mesh->yUp,t,solver->fProp,&F->vS[faceLexNS(F,nX,mesh->nY)]);
      }
    }
  } else if(bcType==OUTFLOW) {
    for(int nX=0; nX<mesh->nX; nX++) {
      if(((double) nX + 0.5)*mesh->dx > xMin) {
        int nSrc = faceLexNS(F,nX,mesh->nY-1);
        int sDst = faceLexNS(F,nX,mesh->nY);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vS[sDst+nC] = F->vN[nSrc+nC];
        }
      }
    }
  } else if(bcType==REFLECTING) {
    for(int nX=0; nX<mesh->nX; nX++) {
      if(((double) nX + 0.5)*mesh->dx > xMin) {
        int nSrc = faceLexNS(F,nX,mesh->nY-1);
        int sDst = faceLexNS(F,nX,mesh->nY);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vS[sDst+nC] = F->vN[nSrc+nC]*(nC==VAR_MOMY ? -1 : 1);
        }
      }
    }
  } else if(bcType==MIXED) {
    faceBCN(solver,mesh,F,t,xMin,mesh->bcNorthSub[0]);
    faceBCN(solver,mesh,F,t,mesh->bcNorthTr,mesh->bcNorthSub[1]);
  }
}
static void faceBCS(GPSolver solver, Mesh mesh, FaceVecD F, double t, double xMin, BCType bcType)
{
  if(bcType==INFLOW) {
    for(int nX=0; nX<mesh->nX; nX++) {
      double x = ((double) nX + 0.5)*mesh->dx;
      if(x>xMin) {
        solver->southBC(x,mesh->yLo,t,solver->fProp,&F->vN[faceLexNS(F,nX,-1)]);
      }
    }
  } else if(bcType==OUTFLOW) {
    for(int nX=0; nX<mesh->nX; nX++) {
      if(((double) nX + 0.5)*mesh->dx > xMin) {
        int sSrc = faceLexNS(F,nX,0);
        int nDst = faceLexNS(F,nX,-1);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vN[nDst+nC] = F->vS[sSrc+nC];
        }
      }
    }
  } else if(bcType==REFLECTING) {
    for(int nX=0; nX<mesh->nX; nX++) {
      if(((double) nX + 0.5)*mesh->dx > xMin) {
        int sSrc = faceLexNS(F,nX,0);
        int nDst = faceLexNS(F,nX,-1);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vN[nDst+nC] = F->vS[sSrc+nC]*(nC==VAR_MOMY ? -1 : 1);
        }
      }
    }
  } else if(bcType==MIXED) {
    faceBCS(solver,mesh,F,t,xMin,mesh->bcSouthSub[0]);
    faceBCS(solver,mesh,F,t,mesh->bcSouthTr,mesh->bcSouthSub[1]);
  }
}
static void faceBCW(GPSolver solver, Mesh mesh, FaceVecD F, double t, double yMin, BCType bcType)
{
  if(bcType==INFLOW) {
    for(int nY=0; nY<mesh->nY; nY++) {
      double y = ((double) nY + 0.5)*mesh->dy;
      if(y>yMin) {
        solver->westBC(mesh->xLo,y,t,solver->fProp,&F->vE[faceLexEW(F,-1,nY)]);
      }
    }
  } else if(bcType==OUTFLOW) {
    for(int nY=0; nY<mesh->nY; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int wSrc = faceLexEW(F,0,nY);
        int eDst = faceLexEW(F,-1,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vE[eDst+nC] = F->vW[wSrc+nC];
        }
      }
    }
  } else if(bcType==REFLECTING) {
    for(int nY=0; nY<mesh->nY; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int wSrc = faceLexEW(F,0,nY);
        int eDst = faceLexEW(F,-1,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vE[eDst+nC] = F->vW[wSrc+nC]*(nC==VAR_MOMX ? -1 : 1);
        }
      }
    }
  } else if(bcType==MIXED) {
    faceBCW(solver,mesh,F,t,yMin,mesh->bcWestSub[0]);
    faceBCW(solver,mesh,F,t,mesh->bcWestTr,mesh->bcWestSub[1]);
  }
}
static void faceBCE(GPSolver solver, Mesh mesh, FaceVecD F, double t, double yMin, BCType bcType)
{
  if(bcType==INFLOW) {
    for(int nY=0; nY<mesh->nY; nY++) {
      double y = ((double) nY + 0.5)*mesh->dy;
      if(y>yMin) {
        solver->eastBC(mesh->xUp,y,t,solver->fProp,&F->vW[faceLexEW(F,mesh->nX,nY)]);
      }
    }
  } else if(bcType==OUTFLOW) {
    for(int nY=0; nY<mesh->nY; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int eSrc = faceLexEW(F,mesh->nX-1,nY);
        int wDst = faceLexEW(F,mesh->nX,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vW[wDst+nC] = F->vE[eSrc+nC];
        }
      }
    }
  } else if(bcType==REFLECTING) {
    for(int nY=0; nY<mesh->nY; nY++) {
      if(((double) nY + 0.5)*mesh->dy>yMin) {
        int eSrc = faceLexEW(F,mesh->nX-1,nY);
        int wDst = faceLexEW(F,mesh->nX,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          F->vW[wDst+nC] = F->vE[eSrc+nC]*(nC==VAR_MOMX ? -1 : 1);
        }
      }
    }
  } else if(bcType==MIXED) {
    faceBCE(solver,mesh,F,t,yMin,mesh->bcEastSub[0]);
    faceBCE(solver,mesh,F,t,mesh->bcEastTr,mesh->bcEastSub[1]);
  }
}
/* This also feels clunky */
static void faceVecHalo(GPSolver solver, FaceVecD U, double t)
{
  Mesh mesh = solver->mesh;
  /* North boundary */
  if(mesh->bcNorth==PERIODIC) {
    for(int nX=0; nX<mesh->nX; nX++) {
      int nDst = faceLexNS(U,nX,-1),       nSrc = faceLexNS(U,nX,mesh->nY-1);
      int sDst = faceLexNS(U,nX,mesh->nY), sSrc = faceLexNS(U,nX,0);
      for(int nC=0; nC<solver->nC; nC++) {
        U->vN[nDst+nC] = U->vN[nSrc+nC];
        U->vS[sDst+nC] = U->vS[sSrc+nC];
      }
    }
  } else {
    faceBCN(solver,mesh,U,t,mesh->xLo-mesh->xUp,mesh->bcNorth);
  }
  /* South boundary */
  faceBCS(solver,mesh,U,t,mesh->xLo-mesh->xUp,mesh->bcSouth);
  /* West boundary */
  if(mesh->bcWest==PERIODIC) {
    for(int nY=0; nY<mesh->nY; nY++) {
      int eDst = faceLexEW(U,-1,nY),       eSrc = faceLexEW(U,mesh->nX-1,nY);
      int wDst = faceLexEW(U,mesh->nX,nY), wSrc = faceLexEW(U,0,nY);
      for(int nC=0; nC<solver->nC; nC++) {
        U->vE[eDst+nC] = U->vE[eSrc+nC];
        U->vW[wDst+nC] = U->vW[wSrc+nC];
      }
    }
  } else {
    faceBCW(solver,mesh,U,t,mesh->yLo-mesh->yUp,mesh->bcWest);
  }
  /* East boundary */
  faceBCE(solver,mesh,U,t,mesh->yLo-mesh->yUp,mesh->bcEast);
}

static inline double dotProd(int nC, double p[nC], double q[nC])
{
  double sum = 0.;
  
  for(int n=VAR_MOMX; n<=VAR_MOMZ && n<nC; n++) { sum += p[n]*q[n]; }
  sum += p[VAR_DENS]*q[VAR_DENS];
  for(int n=VAR_MOMZ+1; n<nC; n++) { sum += p[n]*q[n]; }
  
  return sum;
}

static void reconFaceMulti(GPSolver solver, CellVec U, FaceVecD Uf)
{
  GPStencil2D sten=solver->sten2D;
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<U->nX; nX++) {
    double VL[solver->nC*solver->nC], VR[solver->nC*solver->nC];
    double consVals[sten->nCells*solver->nC], charVals[sten->nCells*solver->nC];
    double mVec[solver->nC], pVec[solver->nC];
    for(int nY=0; nY<U->nY; nY++) {
      int idxNS=faceLexNS(Uf,nX,nY), idxEW=faceLexEW(Uf,nX,nY);
      /* Fill stencil values in */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&consVals[nI*U->nC],&U->v[cellLex(U,nX+sten->xOff[nI],nY+sten->yOff[nI])],U->nC*sizeof(double));
      }
      /* Get x-direction characteristic variables */
      solver->charSysX(&U->v[cellLex(U,nX,nY)],VR,VL,solver->fProp);
      c_dgemm('n','n',solver->nC,sten->nCells,solver->nC,1.,VL,solver->nC,consVals,solver->nC,0.,charVals,solver->nC);
      /* Find prediction */
      GPStencil2DEvalX(sten,solver->nC,charVals,pVec,mVec);
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pVec,1,0.,&Uf->vE[idxEW],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mVec,1,0.,&Uf->vW[idxEW],1);
      /* Get y-direction characteristic variables */
      solver->charSysY(&U->v[cellLex(U,nX,nY)],VR,VL,solver->fProp);
      c_dgemm('n','n',solver->nC,sten->nCells,solver->nC,1.,VL,solver->nC,consVals,solver->nC,0.,charVals,solver->nC);
      /* Find prediction */
      GPStencil2DEvalY(sten,solver->nC,charVals,pVec,mVec);
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pVec,1,0.,&Uf->vN[idxNS],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mVec,1,0.,&Uf->vS[idxNS],1);
    }
  }
}

static void reconFaceMultiGq(GPSolver solver, CellVec U, FaceVecD UfpGp, FaceVecD UfpGm)
{
  GPStencil2D sten=solver->sten2D;
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<U->nX; nX++) {
    double VL[solver->nC*solver->nC], VR[solver->nC*solver->nC];
    double consVals[sten->nCells*solver->nC], charVals[sten->nCells*solver->nC];
    double pfpg[solver->nC], pfmg[solver->nC], mfpg[solver->nC], mfmg[solver->nC];
    for(int nY=0; nY<U->nY; nY++) {
      int idxNS=faceLexNS(UfpGp,nX,nY), idxEW=faceLexEW(UfpGp,nX,nY);
      /* Fill stencil values in */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&consVals[nI*U->nC],&U->v[cellLex(U,nX+sten->xOff[nI],nY+sten->yOff[nI])],U->nC*sizeof(double));
      }
      /* Get x-direction characteristic variables */
      solver->charSysX(&U->v[cellLex(U,nX,nY)],VR,VL,solver->fProp);
      c_dgemm('n','n',solver->nC,sten->nCells,solver->nC,1.,VL,solver->nC,consVals,solver->nC,0.,charVals,solver->nC);
      /* Find prediction */
      GPStencil2DEvalGqX(sten,solver->nC,charVals,pfpg,pfmg,mfpg,mfmg);
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pfpg,1,0.,&UfpGp->vE[idxEW],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pfmg,1,0.,&UfpGm->vE[idxEW],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mfpg,1,0.,&UfpGp->vW[idxEW],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mfmg,1,0.,&UfpGm->vW[idxEW],1);
      /* Get y-direction characteristic variables */
      solver->charSysY(&U->v[cellLex(U,nX,nY)],VR,VL,solver->fProp);
      c_dgemm('n','n',solver->nC,sten->nCells,solver->nC,1.,VL,solver->nC,consVals,solver->nC,0.,charVals,solver->nC);
      /* Find prediction */
      GPStencil2DEvalGqY(sten,solver->nC,charVals,pfpg,pfmg,mfpg,mfmg);
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pfpg,1,0.,&UfpGp->vN[idxNS],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pfmg,1,0.,&UfpGm->vN[idxNS],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mfpg,1,0.,&UfpGp->vS[idxNS],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mfmg,1,0.,&UfpGm->vS[idxNS],1);
    }
  }
}

static void reconFaceDD(GPSolver solver, CellVec U, FaceVecD Uf)
{
  GPStencil1D sten = solver->sten1D;
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<U->nX; nX++) {
    double VL[solver->nC*solver->nC], VR[solver->nC*solver->nC];
    double consVals[sten->nCells*solver->nC], charVals[sten->nCells*solver->nC];
    double mVec[solver->nC], pVec[solver->nC];
    for(int nY=0; nY<U->nY; nY++) {
      int idxNS=faceLexNS(Uf,nX,nY), idxEW=faceLexEW(Uf,nX,nY);
      /* Fill stencil values in */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&consVals[nI*U->nC],&U->v[cellLex(U,nX+nI-sten->rad,nY)],U->nC*sizeof(double));
      }
      /* Get x-direction characteristic variables */
      solver->charSysX(&U->v[cellLex(U,nX,nY)],VR,VL,solver->fProp);
      c_dgemm('n','n',solver->nC,sten->nCells,solver->nC,1.,VL,solver->nC,consVals,solver->nC,0.,charVals,solver->nC);
      /* Find prediction */
      GPStencil1DEvalPM(sten,solver->nC,charVals,pVec,mVec);
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pVec,1,0.,&Uf->vE[idxEW],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mVec,1,0.,&Uf->vW[idxEW],1);
      /* Fill stencil values in */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&consVals[nI*U->nC],&U->v[cellLex(U,nX,nY+nI-sten->rad)],U->nC*sizeof(double));
      }
      /* Get y-direction characteristic variables */
      solver->charSysY(&U->v[cellLex(U,nX,nY)],VR,VL,solver->fProp);
      c_dgemm('n','n',solver->nC,sten->nCells,solver->nC,1.,VL,solver->nC,consVals,solver->nC,0.,charVals,solver->nC);
      /* Find prediction */
      GPStencil1DEvalPM(sten,solver->nC,charVals,pVec,mVec);
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pVec,1,0.,&Uf->vN[idxNS],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mVec,1,0.,&Uf->vS[idxNS],1);
    }
  }
}

static void fillStenPWX(CellVec U, int nX, int nY, double *vals)
{
  for(int nC=0; nC<U->nC; nC++) {
    for(int nI=0; nI<7; nI++) {
      vals[7*nC+nI] = U->v[cellLex(U,nX+nI-3,nY)+nC];
    }
  }
}

static void fillStenPWY(CellVec U, int nX, int nY, double *vals)
{
  for(int nC=0; nC<U->nC; nC++) {
    for(int nI=0; nI<7; nI++) {
      vals[7*nC+nI] = U->v[cellLex(U,nX,nY+nI-3)+nC];
    }
  }
}

static inline void pwBetas(double *u, double *bt)
{
  bt[0] = u[0]*(547*u[0] - 3882*u[1] + 4642*u[2] - 1854*u[3]) +
    u[1]*(7043*u[1] - 17246*u[2] + 7042*u[3]) +
    u[2]*(11003*u[2] - 9402*u[3]) + 2107*u[3]*u[3];
  bt[1] = u[1]*(267*u[1] - 1642*u[2] + 1602*u[3] - 494*u[4]) +
    u[2]*(2843*u[2] - 5966*u[3] + 1922*u[4]) +
    u[3]*(3443*u[3] - 2522*u[4]) + 547*u[4]*u[4];
  bt[2] = u[2]*(547*u[2] - 2522*u[3] + 1922*u[4] - 494*u[5]) +
    u[3]*(3443*u[3] - 5966*u[4] + 1602*u[5]) +
    u[4]*(2843*u[4] - 1642*u[5]) + 267*u[5]*u[5];
  bt[3] = u[3]*(2107*u[3] - 9402*u[4] + 7042*u[5] - 1854*u[6]) +
    u[4]*(11003*u[4] - 17246*u[5] + 4642*u[6]) +
    u[5]*(7043*u[5] - 3882*u[6]) + 547*u[6]*u[6];
}

static inline void pwOmegas(double *gam, double *bt, double *om)
{
  double tot = 0, tau = fabs(bt[0]+3*bt[1]-3*bt[2]-bt[3]);
  for(int n=0; n<4; n++) {
    om[n] = gam[n]*(1.+pow(tau/(1.e-8 + bt[n]), 2));
    tot += om[n];
  }
  for(int n=0; n<4; n++) { om[n] /= tot; }
}

static void reconFacePW(GPSolver solver, CellVec U, FaceVecD Uf)
{
  /* Substencil weights */
  double wtp0[] = {-1./4., 13./12., -23./12., 25./12.};
  double wtp1[] = {1./12., -5./12.,  13./12.,   1./4.};
  double wtp2[] = {-1./12., 7./12.,   7./12., -1./12.};
  double wtp3[] = {1./4. , 13./12.,  -5./12.,  1./12.};
  double gmp[] = {1./35., 12./35., 18./35., 4./35.};

  double wtm0[] = {1./12. , -5./12.,  13./12.,  1./4.};
  double wtm1[] = {-1./12., 7./12.,   7./12., -1./12.};
  double wtm2[] = {1./4., 13./12.,  -5./12.,   1./12.};
  double wtm3[] = {25./12., -23./12., 13./12., -1./4.};
  double gmm[] = {4./35., 18./35., 12./35., 1./35.};
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<U->nX; nX++) {
    double VL[solver->nC*solver->nC], VR[solver->nC*solver->nC];
    double consVals[7*solver->nC], charVals[7*solver->nC];
    double mVec[solver->nC], pVec[solver->nC];
    /* Smoothness indicators, nonlinear weights and substen values */
    double bt[4], om[4], Q[4];
    for(int nY=0; nY<U->nY; nY++) {
      int cellIdx = cellLex(U,nX,nY);
      int idxNS=faceLexNS(Uf,nX,nY), idxEW=faceLexEW(Uf,nX,nY);
      /* Get x-characteristics */
      solver->charSysX(&U->v[cellIdx],VR,VL,solver->fProp);
      fillStenPWX(U, nX, nY, consVals);
      c_dgemm('n','t',7,solver->nC,solver->nC,1.,consVals,7,VL,solver->nC,0.,charVals,7);
      for(int nC=0; nC<solver->nC; nC++) {
        pwBetas(&charVals[nC*7],bt);
        /* Plus face */
        Q[0]=dotProd(4,wtp0,&charVals[nC*7]); Q[1]=dotProd(4,wtp1,&charVals[nC*7+1]);
        Q[2]=dotProd(4,wtp2,&charVals[nC*7+2]); Q[3]=dotProd(4,wtp3,&charVals[nC*7+3]);
        pwOmegas(gmp, bt, om);
        pVec[nC] = dotProd(4,om,Q);
        /* Minus face */
        Q[0]=dotProd(4,wtm0,&charVals[nC*7]); Q[1]=dotProd(4,wtm1,&charVals[nC*7+1]);
        Q[2]=dotProd(4,wtm2,&charVals[nC*7+2]); Q[3]=dotProd(4,wtm3,&charVals[nC*7+3]);
        pwOmegas(gmm, bt, om);
        mVec[nC] = dotProd(4,om,Q);
      }
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mVec,1,0.,&Uf->vW[idxEW],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pVec,1,0.,&Uf->vE[idxEW],1);
      /* Get y-characteristics */
      solver->charSysY(&U->v[cellIdx],VR,VL,solver->fProp);
      fillStenPWY(U, nX, nY, consVals);
      c_dgemm('n','t',7,solver->nC,solver->nC,1.,consVals,7,VL,solver->nC,0.,charVals,7);
      for(int nC=0; nC<solver->nC; nC++) {
        pwBetas(&charVals[nC*7],bt);
        /* Plus face */
        Q[0]=dotProd(4,wtp0,&charVals[nC*7]); Q[1]=dotProd(4,wtp1,&charVals[nC*7+1]);
        Q[2]=dotProd(4,wtp2,&charVals[nC*7+2]); Q[3]=dotProd(4,wtp3,&charVals[nC*7+3]);
        pwOmegas(gmp, bt, om);
        pVec[nC] = dotProd(4,om,Q);
        /* Minus face */
        Q[0]=dotProd(4,wtm0,&charVals[nC*7]); Q[1]=dotProd(4,wtm1,&charVals[nC*7+1]);
        Q[2]=dotProd(4,wtm2,&charVals[nC*7+2]); Q[3]=dotProd(4,wtm3,&charVals[nC*7+3]);
        pwOmegas(gmm, bt, om);
        mVec[nC] = dotProd(4,om,Q);
      }
      /* Project back to conservative variables */
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,mVec,1,0.,&Uf->vS[idxNS],1);
      c_dgemv('n',solver->nC,solver->nC,1.,VR,solver->nC,pVec,1,0.,&Uf->vN[idxNS],1);
    }
  }
}

static void extendFace(GPSolver solver, FaceVecD U)
{
  /* Fill extended halo regions appropriately */
  if(solver->mesh->bcWest != PERIODIC) {
    for(int nY=0; nY<=U->nY; nY++) {
      for(int nX=0; nX<U->nHalo; nX++) {
        int lnSrc = faceLexNS(U,0,nY-1),     rnSrc = faceLexNS(U,U->nX-1,nY-1);
        int lnDst = faceLexNS(U,-1-nX,nY-1), rnDst = faceLexNS(U,U->nX+nX,nY-1);
        int lsSrc = faceLexNS(U,0,nY),       rsSrc = faceLexNS(U,U->nX-1,nY);
        int lsDst = faceLexNS(U,-1-nX,nY),   rsDst = faceLexNS(U,U->nX+nX,nY);
        for(int nC=0; nC<U->nC; nC++) {
          U->vN[lnDst+nC] = U->vN[lnSrc+nC];
          U->vN[rnDst+nC] = U->vN[rnSrc+nC];
          U->vS[lsDst+nC] = U->vS[lsSrc+nC];
          U->vS[rsDst+nC] = U->vS[rsSrc+nC];
        }
      }
    }
  } else {
    for(int nY=0; nY<=U->nY; nY++) {
      for(int nX=0; nX<U->nHalo; nX++) {
        int lnSrc = faceLexNS(U,U->nX-1-nX,nY-1),rnSrc = faceLexNS(U,nX,nY-1);
        int lnDst = faceLexNS(U,-1-nX,nY-1),     rnDst = faceLexNS(U,U->nX+nX,nY-1);
        int lsSrc = faceLexNS(U,U->nX-1-nX,nY),  rsSrc = faceLexNS(U,nX,nY);
        int lsDst = faceLexNS(U,-1-nX,nY),       rsDst = faceLexNS(U,U->nX+nX,nY);
        for(int nC=0; nC<U->nC; nC++) {
          U->vN[lnDst+nC] = U->vN[lnSrc+nC];
          U->vN[rnDst+nC] = U->vN[rnSrc+nC];
          U->vS[lsDst+nC] = U->vS[lsSrc+nC];
          U->vS[rsDst+nC] = U->vS[rsSrc+nC];
        }
      }
    }
  }
  if(solver->mesh->bcSouth != PERIODIC) {
    for(int nX=0; nX<=U->nX; nX++) {
      for(int nY=0; nY<U->nHalo; nY++) {
        int beSrc = faceLexEW(U,nX-1,0),     teSrc = faceLexEW(U,nX-1,U->nY-1);
        int beDst = faceLexEW(U,nX-1,-1-nY), teDst = faceLexEW(U,nX-1,U->nY+nY);
        int bwSrc = faceLexEW(U,nX,0),       twSrc = faceLexEW(U,nX,U->nY-1);
        int bwDst = faceLexEW(U,nX,-1-nY),   twDst = faceLexEW(U,nX,U->nY+nY);
        for(int nC=0; nC<U->nC; nC++) {
          U->vE[beDst+nC] = U->vE[beSrc+nC];
          U->vE[teDst+nC] = U->vE[teSrc+nC];
          U->vW[bwDst+nC] = U->vW[bwSrc+nC];
          U->vW[twDst+nC] = U->vW[twSrc+nC];
        }
      }
    }
  } else {
    for(int nX=0; nX<=U->nX; nX++) {
      for(int nY=0; nY<U->nHalo; nY++) {
        int beSrc = faceLexEW(U,nX-1,U->nY-1-nY), teSrc = faceLexEW(U,nX-1,nY);
        int beDst = faceLexEW(U,nX-1,-1-nY),      teDst = faceLexEW(U,nX-1,U->nY+nY);
        int bwSrc = faceLexEW(U,nX,U->nY-1-nY),   twSrc = faceLexEW(U,nX,nY);
        int bwDst = faceLexEW(U,nX,-1-nY),        twDst = faceLexEW(U,nX,U->nY+nY);
        for(int nC=0; nC<U->nC; nC++) {
          U->vE[beDst+nC] = U->vE[beSrc+nC];
          U->vE[teDst+nC] = U->vE[teSrc+nC];
          U->vW[bwDst+nC] = U->vW[bwSrc+nC];
          U->vW[twDst+nC] = U->vW[twSrc+nC];
        }
      }
    }
  }
}

static void extendFlux(GPSolver solver, FaceVecC F)
{
  /* Fill extended halo regions appropriately */
  if(solver->mesh->bcWest != PERIODIC) {
    for(int nY=-1; nY<F->nY; nY++) {
      for(int nX=0; nX<F->nHalo; nX++) {
        int lSrc = faceLexCN(F,0,nY),     rSrc = faceLexCN(F,F->nX-1,nY);
        int lDst = faceLexCN(F,-1-nX,nY), rDst = faceLexCN(F,F->nX+nX,nY);
        for(int nC=0; nC<F->nC; nC++) {
          F->vY[lDst+nC] = F->vY[lSrc+nC];
          F->vY[rDst+nC] = F->vY[rSrc+nC];
        }
      }
    }
  } else {
    for(int nY=-F->nHalo; nY<F->nY+F->nHalo; nY++) {
      for(int nX=0; nX<F->nHalo; nX++) {
        int lSrc = faceLexCN(F,F->nX-1-nX,nY), rSrc = faceLexCN(F,nX,nY);
        int lDst = faceLexCN(F,-1-nX,nY),      rDst = faceLexCN(F,F->nX+nX,nY);
        for(int nC=0; nC<F->nC; nC++) {
          F->vY[lDst+nC] = F->vY[lSrc+nC];
          F->vY[rDst+nC] = F->vY[rSrc+nC];
        }
      }
    }
  }
  if(solver->mesh->bcSouth != PERIODIC) {
    for(int nX=-1; nX<F->nX; nX++) {
      for(int nY=0; nY<F->nHalo; nY++) {
        int bSrc = faceLexCE(F,nX,0),       tSrc = faceLexCE(F,nX,F->nY-1);
        int bDst = faceLexCE(F,nX,-1-nY),   tDst = faceLexCE(F,nX,F->nY+nY);
        for(int nC=0; nC<F->nC; nC++) {
          F->vX[bDst+nC] = F->vX[bSrc+nC];
          F->vX[tDst+nC] = F->vX[tSrc+nC];
        }
      }
    }
  } else {
    for(int nX=-F->nHalo; nX<F->nX+F->nHalo; nX++) {
      for(int nY=0; nY<F->nHalo; nY++) {
        int bSrc = faceLexCE(F,nX,F->nY-1-nY), tSrc = faceLexCE(F,nX,nY);
        int bDst = faceLexCE(F,nX,-1-nY),      tDst = faceLexCE(F,nX,F->nY+nY);
        for(int nC=0; nC<F->nC; nC++) {
          F->vX[bDst+nC] = F->vX[bSrc+nC];
          F->vX[tDst+nC] = F->vX[tSrc+nC];
        }
      }
    }
  }
}

static void stateCorrection(GPSolver solver, FaceVecD Ufa, FaceVecD Ufp)
{
  extendFace(solver,Ufa);
  GPStencil1D sten = solver->sten1D;
  /* Get cell centered riemann states from the cell average values */
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<solver->mesh->nX; nX++) {
    double vals[sten->nCells*solver->nC];
    for(int nY=0; nY<solver->mesh->nY; nY++) {
      /* Lex indices */
      int idxX=faceLexNS(Ufa,nX,nY), idxY=faceLexEW(Ufa,nX,nY);
      /* West faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vW[faceLexEW(Ufa,nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vW[idxY]);
      /* East faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vE[faceLexEW(Ufa,nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vE[idxY]);
      /* North faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vN[faceLexNS(Ufa,nX+nI-sten->rad,nY)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vN[idxX]);
      /* South faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vS[faceLexNS(Ufa,nX+nI-sten->rad,nY)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vS[idxX]);
    }
  }
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<solver->mesh->nX; nX++) {
    double vals[sten->nCells*solver->nC];
    /* north, nY-1 and nY */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vN[faceLexNS(Ufa,nX+nI-sten->rad,-1)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vN[faceLexNS(Ufa,nX,-1)]);
    /* south, nY and nY+1 */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vS[faceLexNS(Ufa,nX+nI-sten->rad,Ufa->nY)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vS[faceLexNS(Ufa,nX,Ufa->nY)]);
  }
  int nY=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nY)
  for(nY=0; nY<solver->mesh->nY; nY++) {
    double vals[sten->nCells*solver->nC];
    /* east, nX-1 and nX */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vE[faceLexEW(Ufa,-1,nY+nI-sten->rad)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vE[faceLexEW(Ufa,-1,nY)]);
    /* west, nX and nX+1 */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vW[faceLexEW(Ufa,Ufa->nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalCenter(sten,solver->nC,vals,&Ufp->vW[faceLexEW(Ufa,Ufa->nX,nY)]);
  }
}

static void bhStateCorrection(GPSolver solver, FaceVecD Ufa, FaceVecD Ufp)
{
  extendFace(solver,Ufa);
  /* Get cell centered riemann states from the cell average values */
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<solver->mesh->nX; nX++) {
    for(int nY=0; nY<solver->mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        /* Lex indices */
        int m2=faceLexEW(Ufa,nX,nY-2)+nC, m1=faceLexEW(Ufa,nX,nY-1)+nC;
        int id=faceLexEW(Ufa,nX,nY)+nC;
        int p1=faceLexEW(Ufa,nX,nY+1)+nC, p2=faceLexEW(Ufa,nX,nY+2)+nC;
        /* West faces */
        Ufp->vW[id] = Ufa->vW[id] -
          (-3.*Ufa->vW[m2]/640. + 29.*Ufa->vW[m1]/480. - 107.*Ufa->vW[id]/960. + 29.*Ufa->vW[p1]/480. - 3.*Ufa->vW[p2]/640.);
        /* East faces */
        Ufp->vE[id] = Ufa->vE[id] -
          (-3.*Ufa->vE[m2]/640. + 29.*Ufa->vE[m1]/480. - 107.*Ufa->vE[id]/960. + 29.*Ufa->vE[p1]/480. - 3.*Ufa->vE[p2]/640.);
        /* Indices */
        m2=faceLexNS(Ufa,nX-2,nY)+nC, m1=faceLexNS(Ufa,nX-1,nY)+nC;
        id=faceLexNS(Ufa,nX,nY)+nC;
        p1=faceLexNS(Ufa,nX+1,nY)+nC, p2=faceLexNS(Ufa,nX+2,nY)+nC;
        /* North faces */
        Ufp->vN[id] = Ufa->vN[id] -
          (-3.*Ufa->vN[m2]/640. + 29.*Ufa->vN[m1]/480. - 107.*Ufa->vN[id]/960. + 29.*Ufa->vN[p1]/480. - 3.*Ufa->vN[p2]/640.);
        /* South faces */
        Ufp->vS[id] = Ufa->vS[id] -
          (-3.*Ufa->vS[m2]/640. + 29.*Ufa->vS[m1]/480. - 107.*Ufa->vS[id]/960. + 29.*Ufa->vS[p1]/480. - 3.*Ufa->vS[p2]/640.);
      }
    }
  }

#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<solver->mesh->nX; nX++) {
    for(int nC=0; nC<solver->nC; nC++) {
        /* Indices */
        int m2=faceLexNS(Ufa,nX-2,-1)+nC, m1=faceLexNS(Ufa,nX-1,-1)+nC;
        int id=faceLexNS(Ufa,nX,-1)+nC;
        int p1=faceLexNS(Ufa,nX+1,-1)+nC, p2=faceLexNS(Ufa,nX+2,-1)+nC;
        /* north, nY-1 and nY */
        Ufp->vN[id] = Ufa->vN[id] -
          (-3.*Ufa->vN[m2]/640. + 29.*Ufa->vN[m1]/480. - 107.*Ufa->vN[id]/960. + 29.*Ufa->vN[p1]/480. - 3.*Ufa->vN[p2]/640.);
        /* Indices */
        m2=faceLexNS(Ufa,nX-2,Ufa->nY)+nC, m1=faceLexNS(Ufa,nX-1,Ufa->nY)+nC;
        id=faceLexNS(Ufa,nX,Ufa->nY)+nC;
        p1=faceLexNS(Ufa,nX+1,Ufa->nY)+nC, p2=faceLexNS(Ufa,nX+2,Ufa->nY)+nC;
        /* south, nY and nY+1 */
        Ufp->vS[id] = Ufa->vS[id] -
          (-3.*Ufa->vS[m2]/640. + 29.*Ufa->vS[m1]/480. - 107.*Ufa->vS[id]/960. + 29.*Ufa->vS[p1]/480. - 3.*Ufa->vS[p2]/640.);
    }
  }
  int nY=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nY)
  for(nY=0; nY<solver->mesh->nY; nY++) {
    for(int nC=0; nC<solver->nC; nC++) {
      /* west, nX and nX+1 */
      int m2=faceLexEW(Ufa,Ufa->nX,nY-2)+nC, m1=faceLexEW(Ufa,Ufa->nX,nY-1)+nC;
      int id=faceLexEW(Ufa,Ufa->nX,nY)+nC;
      int p1=faceLexEW(Ufa,Ufa->nX,nY+1)+nC, p2=faceLexEW(Ufa,Ufa->nX,nY+2)+nC;
      Ufp->vW[id] = Ufa->vW[id] -
        (-3.*Ufa->vW[m2]/640. + 29.*Ufa->vW[m1]/480. - 107.*Ufa->vW[id]/960. + 29.*Ufa->vW[p1]/480. - 3.*Ufa->vW[p2]/640.);
      /* east, nX-1 and nX */
      m2=faceLexEW(Ufa,-1,nY-2)+nC, m1=faceLexEW(Ufa,-1,nY-1)+nC;
      id=faceLexEW(Ufa,-1,nY)+nC;
      p1=faceLexEW(Ufa,-1,nY+1)+nC, p2=faceLexEW(Ufa,-1,nY+2)+nC;
      Ufp->vE[id] = Ufa->vE[id] -
        (-3.*Ufa->vE[m2]/640. + 29.*Ufa->vE[m1]/480. - 107.*Ufa->vE[id]/960. + 29.*Ufa->vE[p1]/480. - 3.*Ufa->vE[p2]/640.);
    }
  }
}

static void reconGqStates(GPSolver solver, FaceVecD Ufa, FaceVecD Ufp, FaceVecD UfpGm)
{
  extendFace(solver,Ufa);
  GPStencil1D sten = solver->sten1D;
  /* Get Riemann states at GQ points from the cell average values */
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<solver->mesh->nX; nX++) {
    double vals[sten->nCells*solver->nC];
    for(int nY=0; nY<solver->mesh->nY; nY++) {
      /* Lex indices */
      int idxX=faceLexNS(Ufa,nX,nY), idxY=faceLexEW(Ufa,nX,nY);
      /* West faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vW[faceLexEW(Ufa,nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vW[idxY],&UfpGm->vW[idxY]);
      /* East faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vE[faceLexEW(Ufa,nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vE[idxY],&UfpGm->vE[idxY]);
      /* North faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vN[faceLexNS(Ufa,nX+nI-sten->rad,nY)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vN[idxX],&UfpGm->vN[idxX]);
      /* South faces */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vS[faceLexNS(Ufa,nX+nI-sten->rad,nY)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vS[idxX],&UfpGm->vS[idxX]);
    }
  }
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<solver->mesh->nX; nX++) {
    double vals[sten->nCells*solver->nC];
    /* north, nY-1 and nY */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Ufa->vN[faceLexNS(Ufa,nX+nI-sten->rad,-1)],solver->nC*sizeof(double));
      }
    GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vN[faceLexNS(Ufa,nX,-1)],&UfpGm->vN[faceLexNS(Ufa,nX,-1)]);
    /* south, nY and nY+1 */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vS[faceLexNS(Ufa,nX+nI-sten->rad,Ufa->nY)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vS[faceLexNS(Ufa,nX,Ufa->nY)],&UfpGm->vS[faceLexNS(Ufa,nX,Ufa->nY)]);
  }
  int nY=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nY)
  for(nY=0; nY<solver->mesh->nY; nY++) {
    double vals[sten->nCells*solver->nC];
    /* east, nX-1 and nX */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vE[faceLexEW(Ufa,-1,nY+nI-sten->rad)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vE[faceLexEW(Ufa,-1,nY)],&UfpGm->vE[faceLexEW(Ufa,-1,nY)]);
    /* west, nX and nX+1 */
    for(int nI=0; nI<sten->nCells; nI++) {
      memcpy(&vals[nI*solver->nC],&Ufa->vW[faceLexEW(Ufa,Ufa->nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
    }
    GPStencil1DEvalGqPM(sten,solver->nC,vals,&Ufp->vW[faceLexEW(Ufa,Ufa->nX,nY)],&UfpGm->vW[faceLexEW(Ufa,Ufa->nX,nY)]);
  }
}

static void reconFlux(GPSolver solver, FaceVecC Fpnt, FaceVecC Favg)
{
  extendFlux(solver,Fpnt);
  GPStencil1D sten = solver->sten1D;
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=-1; nX<solver->mesh->nX; nX++) {
    double vals[sten->nCells*solver->nC];
    for(int nY=-1; nY<solver->mesh->nY; nY++) {
      /* Skip cell at (-1,-1) */
      if(nX==-1 && nY==-1) { continue; }
      /* y-direction fluxes */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Fpnt->vY[faceLexCN(Fpnt,nX+nI-sten->rad,nY)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalAvg(sten,solver->nC,vals,&Favg->vY[faceLexCN(Favg,nX,nY)]);
      /* x-direction fluxes */
      for(int nI=0; nI<sten->nCells; nI++) {
        memcpy(&vals[nI*solver->nC],&Fpnt->vX[faceLexCE(Fpnt,nX,nY+nI-sten->rad)],solver->nC*sizeof(double));
      }
      GPStencil1DEvalAvg(sten,solver->nC,vals,&Favg->vX[faceLexCE(Favg,nX,nY)]);
    }
  }
}

static void bhReconFlux(GPSolver solver, FaceVecC Fpnt, FaceVecC Favg)
{
  extendFlux(solver,Fpnt);
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=-1; nX<solver->mesh->nX; nX++) {
    for(int nY=-1; nY<solver->mesh->nY; nY++) {
      /* Skip cell at (-1,-1) */
      if(nX==-1 && nY==-1) { continue; }
      /* y-direction fluxes */
      for(int nC=0; nC<solver->nC; nC++) {
        int m2=faceLexCN(Favg,nX-2,nY)+nC, m1=faceLexCN(Favg,nX-1,nY)+nC;
        int id=faceLexCN(Favg,nX,nY)+nC;
        int p1=faceLexCN(Favg,nX+1,nY)+nC, p2=faceLexCN(Favg,nX+2,nY)+nC;
        Favg->vY[id] = Fpnt->vY[id] +
          (-Fpnt->vY[m2]/12. + 4.*Fpnt->vY[m1]/3. - 5.*Fpnt->vY[id]/2. + 4.*Fpnt->vY[p1]/3. - Fpnt->vY[p2]/12.)/24. +
          (Fpnt->vY[m2] - 4.*Fpnt->vY[m1] + 6.*Fpnt->vY[id] - 4.*Fpnt->vY[p1] + Fpnt->vY[p2])/1920.;
        /* x-direction fluxes */
        m2=faceLexCE(Favg,nX,nY-2)+nC, m1=faceLexCE(Favg,nX,nY-1)+nC;
        id=faceLexCE(Favg,nX,nY)+nC;
        p1=faceLexCE(Favg,nX,nY+1)+nC, p2=faceLexCE(Favg,nX,nY+2)+nC;
        Favg->vX[id] = Fpnt->vX[id] +
          (-Fpnt->vX[m2]/12. + 4.*Fpnt->vX[m1]/3. - 5.*Fpnt->vX[id]/2. + 4.*Fpnt->vX[p1]/3. - Fpnt->vX[p2]/12.)/24. +
          (Fpnt->vX[m2] - 4.*Fpnt->vX[m1] + 6.*Fpnt->vX[id] - 4.*Fpnt->vX[p1] + Fpnt->vX[p2])/1920.;
      }
    }
  }
}

static void integrateFlux(GPSolver solver, FaceVecC Fpnt, FaceVecC FpntGm, FaceVecC Favg)
{
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=-1; nX<solver->mesh->nX; nX++) {
    for(int nY=-1; nY<solver->mesh->nY; nY++) {
      /* Skip cell at (-1,-1) */
      if(nX==-1 && nY==-1) { continue; }
      /* Sum fluxes */
      int idxCN = faceLexCN(Favg,nX,nY);
      int idxCE = faceLexCE(Favg,nX,nY);
      for(int nC=0; nC<solver->nC; nC++) {
        Favg->vY[idxCN+nC] = (Fpnt->vY[idxCN+nC]+FpntGm->vY[idxCN+nC])/2.;
        Favg->vX[idxCE+nC] = (Fpnt->vX[idxCE+nC]+FpntGm->vX[idxCE+nC])/2.;
      }
    }
  }
}

static void evalSrc(GPSolver solver, CellVec U, CellVec S)
{
  Mesh mesh = solver->mesh;
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    double xc = mesh->xLo+((double) nX + 0.5)*mesh->dx;
    for(int nY=0; nY<mesh->nY; nY++) {
      double yc = mesh->yLo+((double) nY + 0.5)*mesh->dy;
      solver->srcFunc(xc,yc,&U->v[cellLex(U,nX,nY)],&S->v[cellLex(U,nX,nY)]);
    }
  }
}

static double evalRHS(GPSolver solver, CellVec U, double t)
{
  FaceVecD Ufa = solver->Ufa;
  FaceVecD Ufp = solver->fluxRec ? solver->Ufp : Ufa;
  FaceVecD UfpGm = solver->gaussFlux ? solver->UfpGm : NULL;
  FaceVecC Favg = solver->Favg;
  FaceVecC Fpnt = solver->fluxRec ? solver->Fpnt : Favg;
  FaceVecC FpntGm = solver->gaussFlux ? solver->FpntGm : NULL;
  /* Exchange halo data */
  cellVecHalo(solver,U,t);
  /* Reconstruct face values */
  if(solver->dimByDim) {
    if(!solver->polyWeno) {
      reconFaceDD(solver,U,Ufa);
    } else {
      reconFacePW(solver,U,Ufa);
    }
    faceVecHalo(solver,Ufa,t);
    /* Change from face averages to face centered */
    if(solver->fluxRec) {
      if(solver->gaussFlux) {
        reconGqStates(solver,Ufa,Ufp,UfpGm);
      } else if(solver->bhFlux) {
        bhStateCorrection(solver,Ufa,Ufp);
      } else {
        stateCorrection(solver,Ufa,Ufp);
      }
    }
  } else {
    if(!solver->gaussFlux) {
      reconFaceMulti(solver,U,Ufp); 
    } else {
      reconFaceMultiGq(solver,U,Ufp,UfpGm); 
    }
  }
  /* Solve all Riemann problems */
  faceVecHalo(solver,Ufp,t);
  if(solver->gaussFlux) {
    faceVecHalo(solver,UfpGm,t);
  }
  double maxVel=0;
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX) reduction(max:maxVel)
  for(nX=0; nX<U->nX; nX++) {
    double v = solver->riemY(solver->fProp,
                             &Ufp->vN[faceLexNS(Ufp,nX,-1)],
                             &Ufp->vS[faceLexNS(Ufp,nX,0)],
                             &Fpnt->vY[faceLexCS(Fpnt,nX,0)]);
    maxVel = v>maxVel ? v : maxVel;
    if(solver->gaussFlux) {
      v = solver->riemY(solver->fProp,
                        &UfpGm->vN[faceLexNS(UfpGm,nX,-1)],
                        &UfpGm->vS[faceLexNS(UfpGm,nX,0)],
                        &FpntGm->vY[faceLexCS(FpntGm,nX,0)]);
      maxVel = v>maxVel ? v : maxVel;
    }
    for(int nY=0; nY<U->nY; nY++) {
      v = solver->riemX(solver->fProp,
                        &Ufp->vE[faceLexEW(Ufp,nX,nY)],
                        &Ufp->vW[faceLexEW(Ufp,nX+1,nY)],
                        &Fpnt->vX[faceLexCE(Fpnt,nX,nY)]);
      maxVel = v>maxVel ? v : maxVel;
      if(solver->gaussFlux) {
        v = solver->riemX(solver->fProp,
                          &UfpGm->vE[faceLexEW(UfpGm,nX,nY)],
                          &UfpGm->vW[faceLexEW(UfpGm,nX+1,nY)],
                          &FpntGm->vX[faceLexCE(FpntGm,nX,nY)]);
        maxVel = v>maxVel ? v : maxVel;
      }
      v = solver->riemY(solver->fProp,
                        &Ufp->vN[faceLexNS(Ufp,nX,nY)],
                        &Ufp->vS[faceLexNS(Ufp,nX,nY+1)],
                        &Fpnt->vY[faceLexCN(Fpnt,nX,nY)]);
      maxVel = v>maxVel ? v : maxVel;
      if(solver->gaussFlux) {
        v = solver->riemY(solver->fProp,
                          &UfpGm->vN[faceLexNS(UfpGm,nX,nY)],
                          &UfpGm->vS[faceLexNS(UfpGm,nX,nY+1)],
                          &FpntGm->vY[faceLexCN(FpntGm,nX,nY)]);
        maxVel = v>maxVel ? v : maxVel;
      }
    }
  }
  int nY=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nY) reduction(max:maxVel)
  for(nY=0; nY<U->nY; nY++) {
    double v = solver->riemX(solver->fProp,
                             &Ufp->vE[faceLexEW(Ufp,-1,nY)],
                             &Ufp->vW[faceLexEW(Ufp,0,nY)],
                             &Fpnt->vX[faceLexCW(Fpnt,0,nY)]);
    maxVel = v>maxVel ? v : maxVel;
    if(solver->gaussFlux) {
      v = solver->riemX(solver->fProp,
                        &UfpGm->vE[faceLexEW(UfpGm,-1,nY)],
                        &UfpGm->vW[faceLexEW(UfpGm,0,nY)],
                        &FpntGm->vX[faceLexCW(FpntGm,0,nY)]);
      maxVel = v>maxVel ? v : maxVel;
    }
  }
  /* Reconstruct fluxes */
  if(solver->fluxRec) {
    if(solver->gaussFlux) {
      integrateFlux(solver,Fpnt,FpntGm,Favg);
    } else if(solver->bhFlux) {
      bhReconFlux(solver,Fpnt,Favg);
    } else {
      reconFlux(solver,Fpnt,Favg);
    }
  }
  return maxVel;
}

/* Only written for RK7 */
static bool formKn(GPSolver solver, int stage, double cfl, double *dt, double cn, double *ai, CellVec U, CellVec *K, CellVec Kbody)
{
  Mesh mesh = solver->mesh;
  FaceVecC Favg=solver->Favg;
  bool done = false;
  if(stage == 0) {
    double maxVel = evalRHS(solver,U,solver->t);
    *dt = MIN(cfl*solver->h/maxVel,solver->dt);
    if((*dt+solver->t) > solver->tF) {
      *dt = solver->tF-solver->t;
      done = true;
    }
  } else { /* fill in rhs */
    int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
    for(nX=0; nX<mesh->nX; nX++) {
      for(int nY=0; nY<mesh->nY; nY++) {
        int idx = cellLex(U,nX,nY);
        for(int nC=0; nC<solver->nC; nC++) {
          solver->K->v[idx+nC] = U->v[idx+nC];
          for(int nS=0; nS<stage; nS++) {
            solver->K->v[idx+nC] += ai[nS]*K[nS]->v[idx+nC];
          }
        }
      }
    }
    evalRHS(solver,solver->K,solver->t+cn*(*dt));
  }
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K[stage]->v[cellLex(U,nX,nY)+nC] =
          (*dt)*((Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC])/mesh->dy +
                 (Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC])/mesh->dx +
                 Kbody->v[cellLex(U,nX,nY)+nC]);
      }
    }
  }

  return done;
}

bool GPSolverRK7Step(GPSolver solver, double cfl)
{
  Mesh mesh = solver->mesh;
  CellVec U = solver->U, Kbody=solver->Kbody;
  /* Butcher tableau hacked in */
  double c[] = {0.,2./27.,1./9.,1./6.,5./12.,1./2.,5./6.,1./6.,2./3.,1./3.,1.};
  double a1[] =  {2./27.};
  double a2[] =  {1./36.,      1./12.};
  double a3[] =  {1./24.,      0,           1./8.};
  double a4[] =  {5./12.,      0,          -25./16., 25./16.};
  double a5[] =  {1./20.,      0,           0,        1./4.,      1./5.};
  double a6[] =  {-25./108.,   0,           0,      125./108.,  -65./27.,   125./54.};
  double a7[] =  {31./300.,    0,           0,        0,         61./225.,   -2./9.,     13./900.};
  double a8[] =  {2.,          0,           0,      -53./6.,    704./45.,   -107./9.,    67./90.,    3.};
  double a9[] =  {-91./108.,   0,           0,       23./108., -976./135.,   311./54.,  -19./60.,   17./6.,  -1./12.};
  double a10[] = {2383./4100., 0,           0,     -341./164., 4496./1025., -301./82., 2133./4100., 45./82., 45./164., 18./41.};
  double *a[] = {a1,a2,a3,a4,a5,a6,a7,a8,a9,a10};
  double b[] = {41./840.,0,0,0,0,34./105.,9./35.,9./35.,9./280.,9./280.,41./840.};
  double dt = -1.;
  /* Stage zero sets dt and decides if stepping is done */
  bool done = formKn(solver, 0, cfl, &dt, c[0], NULL, solver->U, solver->K7s, Kbody);
  for(int nS=1; nS<11; nS++) {
    formKn(solver, nS, cfl, &dt, c[nS], a[nS-1], solver->U, solver->K7s, Kbody);
  }
  /* Note: not all stages need to be added together to form the next step */
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        U->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] +
          b[0]*solver->K7s[0]->v[cellLex(U,nX,nY)+nC] + 
          b[5]*solver->K7s[5]->v[cellLex(U,nX,nY)+nC] + 
          b[6]*solver->K7s[6]->v[cellLex(U,nX,nY)+nC] + 
          b[7]*solver->K7s[7]->v[cellLex(U,nX,nY)+nC] + 
          b[8]*solver->K7s[8]->v[cellLex(U,nX,nY)+nC] + 
          b[9]*solver->K7s[9]->v[cellLex(U,nX,nY)+nC] + 
          b[10]*solver->K7s[10]->v[cellLex(U,nX,nY)+nC];
      }
    }
  }
  solver->t += dt;
  if(solver->nAux>0) {
    updateAux(solver,U);
  }
  solver->dt = 2.*dt;
  return done;
}

bool GPSolverRK2Step(GPSolver solver, double cfl)
{
  Mesh mesh = solver->mesh;
  CellVec U=solver->U, K=solver->K, Kbody=solver->Kbody;
  FaceVecC Favg=solver->Favg;
  /* Form K1 and set time step size */
  double maxVel = evalRHS(solver,U,solver->t);
  double dt = MIN(cfl*solver->h/maxVel,solver->dt);
  bool done = false;
  if((dt+solver->t) > solver->tF) {
    dt = solver->tF-solver->t;
    done = true;
  }
  
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] +
          (dt/mesh->dy)*(Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC]) +
          (dt/mesh->dx)*(Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC]) +
          dt*Kbody->v[cellLex(U,nX,nY)+nC];
      }
    }
  }
  /* Form U(t+dt) */
  evalRHS(solver,K,solver->t+dt/2.);
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        U->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC]/2. +
          K->v[cellLex(U,nX,nY)+nC]/2. +
          (dt/(2.*mesh->dy))*(Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC]) +
          (dt/(2.*mesh->dx))*(Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC]) +
          dt*Kbody->v[cellLex(U,nX,nY)+nC]/2.;
      }
    }
  }
  solver->t += dt;
  if(solver->nAux>0) {
    updateAux(solver,U);
  }
  solver->dt = 2.*dt;
  if(isnan(maxVel)) { done = true; }
  return done;
}

bool GPSolverRK3Step(GPSolver solver, double cfl)
{
  Mesh mesh = solver->mesh;
  CellVec U=solver->U, K=solver->K, Kbody=solver->Kbody;
  FaceVecC Favg=solver->Favg;
  /* Form K1 and set time step size */
  if(solver->srcFunc) { evalSrc(solver,U,Kbody); }
  double maxVel = evalRHS(solver,U,solver->t);
  double dt = MIN(cfl*solver->h/maxVel,solver->dt);
  bool done = false;
  if((dt+solver->t) > solver->tF) {
    dt = solver->tF-solver->t;
    done = true;
  }
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] +
          (dt/mesh->dy)*(Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC]) +
          (dt/mesh->dx)*(Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC]) +
          dt*Kbody->v[cellLex(U,nX,nY)+nC];
      }
    }
  }
  /* Form K2 */
  evalRHS(solver,K,solver->t+dt);
  if(solver->srcFunc) { evalSrc(solver,K,Kbody); }
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K->v[cellLex(U,nX,nY)+nC] = 3.*U->v[cellLex(U,nX,nY)+nC]/4. +
          K->v[cellLex(U,nX,nY)+nC]/4. +
          (dt/(4.*mesh->dy))*(Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC]) +
          (dt/(4.*mesh->dx))*(Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC]) +
          dt*Kbody->v[cellLex(U,nX,nY)+nC]/4.;
      }
    }
  }
  /* Form U(t+dt) */
  evalRHS(solver,K,solver->t+dt/2.);
  if(solver->srcFunc) { evalSrc(solver,K,Kbody); }
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        U->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC]/3. +
          2.*K->v[cellLex(U,nX,nY)+nC]/3. +
          (2.*dt/(3.*mesh->dy))*(Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC]) +
          (2.*dt/(3.*mesh->dx))*(Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC]) +
          2.*dt*Kbody->v[cellLex(U,nX,nY)+nC]/3.;
      }
    }
  }
  if(solver->nAux>0) {
    updateAux(solver,U);
  }
  solver->t += dt;
  solver->dt = 2.*dt;
  if(isnan(maxVel)) { done = true; }
  return done;
}

bool GPSolverRK4Step(GPSolver solver, double cfl)
{
  Mesh mesh = solver->mesh;
  CellVec U=solver->U, K=solver->K, Kbody=solver->Kbody;
  CellVec K1=solver->K1, K2=solver->K2, K3=solver->K3, K4=solver->K4;
  FaceVecC Favg = solver->Favg;
  /* Form K1 and set time step size */
  double maxVel = evalRHS(solver,U,solver->t);
  double dt = MIN(cfl*solver->h/maxVel,solver->dt);
  bool done = false;
  if(solver->t+dt > solver->tF) {
    dt = solver->tF - solver->t;
    done = true;
  }
  int nX=0;
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K1->v[cellLex(U,nX,nY)+nC] =
          dt*((Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC])/mesh->dy +
              (Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC])/mesh->dx +
              Kbody->v[cellLex(U,nX,nY)+nC]);
        K->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] + K1->v[cellLex(U,nX,nY)+nC]/2.;
      }
    }
  }
  /* Form K2 */
  evalRHS(solver,K,solver->t+dt/2.);
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K2->v[cellLex(U,nX,nY)+nC] =
          dt*((Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC])/mesh->dy +
              (Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC])/mesh->dx +
              Kbody->v[cellLex(U,nX,nY)+nC]);
        K->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] + K2->v[cellLex(U,nX,nY)+nC]/2.;
      }
    }
  }
  /* Form K3 */
  evalRHS(solver,K,solver->t+dt/2.);
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K3->v[cellLex(U,nX,nY)+nC] =
          dt*((Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC])/mesh->dy +
              (Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC])/mesh->dx +
              Kbody->v[cellLex(U,nX,nY)+nC]);
        K->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] + K3->v[cellLex(U,nX,nY)+nC];
      }
    }
  }
  /* Form K4 and update U */
  evalRHS(solver,K,solver->t+dt);
#pragma omp parallel for schedule(dynamic,OMPCHUNK) private(nX)
  for(nX=0; nX<mesh->nX; nX++) {
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nC=0; nC<solver->nC; nC++) {
        K4->v[cellLex(U,nX,nY)+nC] =
          dt*((Favg->vY[faceLexCS(Favg,nX,nY)+nC] - Favg->vY[faceLexCN(Favg,nX,nY)+nC])/mesh->dy +
              (Favg->vX[faceLexCW(Favg,nX,nY)+nC] - Favg->vX[faceLexCE(Favg,nX,nY)+nC])/mesh->dx +
              Kbody->v[cellLex(U,nX,nY)+nC]);
        U->v[cellLex(U,nX,nY)+nC] = U->v[cellLex(U,nX,nY)+nC] +
          (K1->v[cellLex(U,nX,nY)+nC] + 2.*K2->v[cellLex(U,nX,nY)+nC] + 2.*K3->v[cellLex(U,nX,nY)+nC] + K4->v[cellLex(U,nX,nY)+nC])/6.;
      }
    }
  }
  if(solver->nAux>0) {
    updateAux(solver,U);
  }
  solver->t += dt;
  solver->dt = 2.*dt;
  if(isnan(maxVel)) { done = true; }
  return done;
}
