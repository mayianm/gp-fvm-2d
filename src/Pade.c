/* File: Pade.c
 * Author: Ian May
 * Purpose: Find prediction vectors given stencil layout, covariance
 *          function, and sample function
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <complex.h>

#include <gsl/gsl_fft_complex.h>

#include <cerf.h>

#include "LapackWrappers.h"
#include "Options.h"
#include "Pade.h"

#define MAX(a,b) ((a)<(b) ? (b) : (a))
#define MIN(a,b) ((a)<(b) ? (a) : (b))
#define FABMIN(a,b) (fabs(a)<fabs(b) ? (a) : (b))

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Covariance and sample vector helpers */
static void covMat(double complex ce, int n, double x[n], double y[n], double *A,
                   double complex(*CovFunc)(double complex,double,double))
{
  for(int i=0; i<n; i++) {
    for(int j=0; j<n; j++) {
      double complex entry = CovFunc(ce, x[i]-x[j], y[i]-y[j]);
      A[2*(j*n+i)] = creal(entry);
      A[2*(j*n+i)+1] = cimag(entry);
    }
  }
}

static void sampleVec(double complex ce, int n, double qx, double qy,
                      double x[n], double y[n], double *T,
                      double complex(*SampleFunc)(double complex,double,double))
{
  for(int k=0; k<n; k++) {
    double complex entry = SampleFunc(ce,qx-x[k],qy-y[k]);
    T[2*k] = creal(entry);
    T[2*k+1] = cimag(entry);
  }
}

static void evalPnt(double complex ce, int nPts, double qx, double qy,
                    double x[nPts], double y[nPts], double sample[2*nPts],
                    double complex(*CovFunc)(double complex,double,double),
                    double complex(*SampleFunc)(double complex,double,double))
{
  int LWORK=16*nPts;
  double WORK[2*LWORK];
  int IPIV[nPts];
  double cov[2*nPts*nPts];
  covMat(ce,nPts,x,y,cov,CovFunc);
  sampleVec(ce,nPts,qx,qy,x,y,sample,SampleFunc);
  c_zsysv('U',nPts,1,cov,nPts,IPIV,sample,nPts,WORK,LWORK);
}

static void evalContour(int nPts, int nContSym, double qx, double qy, double complex *ce,
                        double x[nPts], double y[nPts], double complex cVals[nPts][nContSym+1],
                        double complex(*CovFunc)(double complex,double,double),
                        double complex(*SampleFunc)(double complex,double,double))
{
  double sample[2*nPts];
  for(int nC=0; nC<=nContSym; nC++) {
    evalPnt(ce[nC],nPts,qx,qy,x,y,sample,CovFunc,SampleFunc);
    for(int nP=0; nP<nPts; nP++) {
      cVals[nP][nC] = CMPLX(sample[2*nP],sample[2*nP+1]);
    }
  }
}                      

static void findPade(int nCont, int nPade, double *lSer, double *pdNumer, double *pdDenom)
{
  int M = nPade-1;
  double A[M*M];
  int IPIV[M];
  /* Fill A and b with Taylor series approx. */
  double anorm = 0.;
  for(int i=0; i<M; i++) {
    double rnorm = 0.;
    for(int j=0; j<M; j++) {
      int idx = M+i-j;
      A[M*j + i] = lSer[nCont - idx];
      rnorm += fabs(A[M*j + i]);
    }
    pdDenom[i+1] = -lSer[nCont - (M+i+1)];
    anorm = rnorm>anorm ? rnorm : anorm;
  }
  /* Find Pade denominator coeffs */
  c_dgesv(M,1,A,M,IPIV,&pdDenom[1],M);
  pdDenom[0] = 1.;
  /* Find numerator coefficients */
  for(int nP=0; nP<nPade; nP++) {
    pdNumer[nP] = 0.;
    for(int j=0; j<=nP; j++) {
      int idx = nP-j;
      if(idx>0) { pdNumer[nP] += lSer[nCont-idx]*pdDenom[j]; }
    }
  }
}

/* Horner eval of polynomial, a holds coefficients with const term first */
static double fwdHorner(int n, double x, double *a)
{
  double p = 0.;
  for(int nD=n-1; nD>=0; nD--) { p = p*x + a[nD]; }
  return p;
}

/* Horner eval of polynomial, a holds coefficients with const term last */
static double revHorner(int n, double x, double *a)
{
  double p = 0.;
  for(int nD=0; nD<n; nD++) { p = p*x + a[nD]; }
  return p;
}

void findPredictor(int nPts, double ell, double qx, double qy,
                   double x[nPts], double y[nPts], double *pred,
                   double complex(*CovFunc)(double complex,double,double),
                   double complex(*SampleFunc)(double complex,double,double),
                   const char *fName)
{
  /* Contour density and number of Pade terms is hard coded for now */
  int nCont = 512; /* Points along half-circle in upper half plane */
  int nContSym = nCont/2;
  int nPade = nCont/4 + 1;
  /* Generate contour of epsilon values */
  double complex ce[nContSym+1];
  double cerad=.6, cetheta=2.*M_PI/((double) nCont), epsEff = ell>0. ? 1./(sqrt(2.)*ell) : 0.;
  for(int nC=0; nC<=nContSym; nC++) {
    double th = ((double) nC)*cetheta;
    ce[nC] = CMPLX(cerad*cos(th),cerad*sin(th));
  }
  /* Allocate temporaries */
  double complex cVals[nPts][nContSym+1];
  double fftCont[2*nCont], lSer[nCont], pdNumer[nPade], pdDenom[nPade];
  /* Evaluate predictor along contour */
  evalContour(nPts,nContSym,qx,qy,ce,x,y,cVals,CovFunc,SampleFunc);
  /* Take FFT and find Pade representation for each point */
  for(int nP=0; nP<nPts; nP++) {
    /* Copy contour values into fftCont since FFT is in-place */
    fftCont[0] = creal(cVals[nP][0]);
    fftCont[1] = 0.;
    for(int nC=1; nC<nContSym; nC++) {
      fftCont[2*nC] = creal(cVals[nP][nC]);
      fftCont[2*nC+1] = cimag(cVals[nP][nC]);
    }
    fftCont[2*nContSym] = creal(cVals[nP][nContSym]);
    fftCont[2*nContSym+1] = 0.;
    for(int nC=1; nC<nContSym; nC++) {
      fftCont[2*(nC+nContSym)] = creal(cVals[nP][nContSym-nC]);
      fftCont[2*(nC+nContSym)+1] = -cimag(cVals[nP][nContSym-nC]);
    }
    gsl_fft_complex_radix2_forward(fftCont,1,nCont);
    /* Scale the circle radius out of the Laurent series coeffs */
    for(int nC=0; nC<nCont; nC++) {
      int kp = nC<nContSym ? -nC : nCont-nC;
      lSer[nC] = fftCont[2*nC]*pow(cerad,kp)/nCont;
    }
    lSer[nContSym] = 0.;
    /* Find Pade representation and evaluate at desired ell value */
    findPade(nCont,nPade,lSer,pdNumer,pdDenom);
    if(nP==3 && fName!=NULL) {
      FILE *fp = fopen(fName,"w");
      for(int n=0; n<nPade; n++) { fprintf(fp,"%e %e\n",pdNumer[n],pdDenom[n]); }
      fprintf(fp,"\n");
      fclose(fp);
    }
    double poly=fwdHorner(nContSym, epsEff, lSer), numer=revHorner(nPade, epsEff, pdNumer), denom=revHorner(nPade, epsEff, pdDenom);
    pred[nP] = poly + numer/denom;
  }
}
