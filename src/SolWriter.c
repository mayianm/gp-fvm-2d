/* File: SolWriter.c
 * Author: Ian May
 * Purpose: Open and write HDF5 files throughout the run
 */

#include <string.h>
#include <stdio.h>

#include "Vec.h"
#include "SolWriter.h"

void CreateSolWriter(Mesh mesh, int a_nComps, int a_nAux, const char *a_base,
                     const char **a_varNames, const char **a_auxNames,
                     SolWriter *a_writer)
{
  SolWriter writer = malloc(sizeof(*writer));
  strcpy(writer->baseName, a_base);
  writer->nVars[0] = mesh->nY;
  writer->nVars[1] = mesh->nX;
  writer->nComps = a_nComps;
  writer->nAux = a_nAux;
  writer->nDims = 2;
  writer->extent[0] = mesh->xLo;
  writer->extent[1] = mesh->xUp;
  writer->extent[2] = mesh->yLo;
  writer->extent[3] = mesh->yUp;
  /* Copy in variable names */
  writer->varNames = malloc(writer->nComps*sizeof(*writer->varNames));
  for(int nC=0; nC<writer->nComps; nC++) {
    writer->varNames[nC] =
      malloc((strlen(a_varNames[nC])+1)*sizeof(*writer->varNames[nC]));
    strcpy(writer->varNames[nC],a_varNames[nC]);
  }
  writer->auxNames = malloc(writer->nAux*sizeof(*writer->auxNames));
  for(int nC=0; nC<writer->nAux; nC++) {
    writer->auxNames[nC] =
      malloc((strlen(a_auxNames[nC])+1)*sizeof(*writer->auxNames[nC]));
    strcpy(writer->auxNames[nC],a_auxNames[nC]);
  }
  /* Fill grid information */
  writer->grX = malloc(mesh->nX*sizeof(*writer->grX));
  for(int nX=0; nX<mesh->nX; nX++) { writer->grX[nX] = mesh->xLo + (((double) nX)+0.5)*mesh->dx; }
  writer->grY = malloc(mesh->nY*sizeof(*writer->grY));
  for(int nY=0; nY<mesh->nY; nY++) { writer->grY[nY] = mesh->yLo + (((double) nY)+0.5)*mesh->dy; }
  /* Allocate room for temp solutions */
  writer->solTmp = calloc(mesh->nTot,sizeof(*writer->solTmp));

  *a_writer = writer;
}

void DestroySolWriter(SolWriter *a_writer)
{
  if(*a_writer) {
    SolWriter writer = *a_writer;
    if(writer->varNames) {
      for(int nC=0; nC<writer->nComps; nC++) {
        if(writer->varNames[nC]) { free(writer->varNames[nC]); }
      }
      free(writer->varNames);
    }
    if(writer->auxNames) {
      for(int nC=0; nC<writer->nAux; nC++) {
        if(writer->auxNames[nC]) { free(writer->auxNames[nC]); }
      }
      free(writer->auxNames);
    }
    if(writer->grX) { free(writer->grX); }
    if(writer->grY) { free(writer->grY); }
    if(writer->solTmp) { free(writer->solTmp); }
    free(*a_writer);
  }
}

void SolWriterOutput(SolWriter writer, Mesh mesh, CellVec U, CellVec A, int step, double time)
{
  /* Setup filename and open file */
  char fileName[110], cycle[7];
  strcpy(fileName, writer->baseName);
  sprintf(cycle, "%06d", step);
  strcat(fileName, cycle);
  strcat(fileName, ".h5");
  printf("Writing solution file: %s\n", fileName);
  hid_t file_id = H5Fcreate(fileName, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  /* Write out domain and time info */
  hsize_t nX_t=mesh->nX, nY_t=mesh->nY, four=4, one=1;
  hid_t dataspace_x = H5Screate_simple(1, &nX_t, &nX_t);
  hid_t dataspace_y = H5Screate_simple(1, &nY_t, &nY_t);
  hid_t dataspace_ext = H5Screate_simple(1, &four, &four);
  hid_t dataspace_t = H5Screate_simple(1, &one, &one); /* Feels silly... */
  hid_t dataset_x = H5Dcreate(file_id, "x", H5T_NATIVE_DOUBLE, dataspace_x, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  hid_t dataset_y = H5Dcreate(file_id, "y", H5T_NATIVE_DOUBLE, dataspace_y, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  hid_t dataset_ext = H5Dcreate(file_id, "ext", H5T_NATIVE_DOUBLE, dataspace_ext, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  hid_t dataset_t = H5Dcreate(file_id, "time", H5T_NATIVE_DOUBLE, dataspace_t, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite(dataset_x, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, writer->grX);
  H5Dwrite(dataset_y, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, writer->grY);
  H5Dwrite(dataset_ext, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, writer->extent);
  H5Dwrite(dataset_t, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &time);
  H5Dclose(dataset_x);
  H5Dclose(dataset_y);
  H5Dclose(dataset_ext);
  H5Dclose(dataset_t);
  /* Write out solution variables, have to make temp copy due to slicing */
  hid_t dataspace_id = H5Screate_simple(writer->nDims, writer->nVars, writer->nVars);
  for(int nC=0; nC<writer->nComps; nC++) {
    int n = 0;
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nX=0; nX<mesh->nX; nX++) {
        writer->solTmp[n] = U->v[cellLex(U,nX,nY)+nC];
        n++;
      }
    }
    hid_t dataset_id = H5Dcreate(file_id, writer->varNames[nC], H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, writer->solTmp);
    H5Dclose(dataset_id);
  }
  
  /* Write out auxiliary variables */
  for(int nC=0; nC<writer->nAux; nC++) {
    int n = 0;
    for(int nY=0; nY<mesh->nY; nY++) {
      for(int nX=0; nX<mesh->nX; nX++) {
        writer->solTmp[n] = A->v[cellLex(A,nX,nY)+nC];
        n++;
      }
    }
    hid_t dataset_id = H5Dcreate(file_id, writer->auxNames[nC], H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, writer->solTmp);
    H5Dclose(dataset_id);
  }
  H5Sclose(dataspace_id);
  H5Fclose(file_id);
}
