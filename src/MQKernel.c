/* File: MQKernel.c
 * Author: Ian May
 * Purpose:
 */

#include <stdio.h>
#include <math.h>
#include <complex.h>

#include "MQKernel.h"

/* Quadrature data */
static double gl_ab5[] = {-0.906179845938663992797627,
                          -0.538469310105683091036314,
                           0.,
                           0.538469310105683091036314,
                           0.906179845938663992797627};

static double gl_wt5[] =  {0.236926885056189087514264,
                           0.478628670499366468041292,
                           0.568888888888888888888889,
                           0.478628670499366468041292,
                           0.236926885056189087514264};

/* Covariance functions */
/* Kernel for pointwise data */
double complex MQ_pntCov(double complex ce, double xdkh, double ydkh)
{
  return csqrt(1.+ce*ce*(xdkh*xdkh)) * csqrt(1.+ce*ce*(ydkh*ydkh));
}

/* Kernel antiderivative in first variable */
static double complex antider(double complex ce, double x, double y)
{
  return ( (x-y)*csqrt(ce*ce*(x-y)*(x-y)+1.) + casinh(ce*(x-y))/ce )/2.;
}

/* Kernel for 1D cell-averaged or face-averaged data */
double complex MQ_intCov1D(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex ret = 0.;
  /* Integrate [antider(ce,0.5,y)-antider(ce,-0.5,y)] from dkh-0.5 to dkh+0.5 */
  for(int n=0; n<5; n++) {
    const double pos = dkh+gl_ab5[n]/2.;
    ret += gl_wt5[n]*(antider(ce,0.5,pos)-antider(ce,-0.5,pos));
  }
  return ret/2.; /* Don't forget to scale the weights to unit interval */
}

/* Kernel for 2D cell-averaged data */
double complex MQ_intCov2D(double complex ce, double xdkh, double ydkh)
{
  return MQ_intCov1D(ce,xdkh,0.)*MQ_intCov1D(ce,ydkh,0.);
}

/* 1D Sample functions */
/* Sample 1D cell-averaged data onto point wise predictions */
/* By symmetry this also does the pnt -> av conversion */
double complex MQ_sample1DAv2Pnt(double complex ce, double dkh, double unused)
{
  (void) unused;
  return antider(ce,dkh+0.5,0.)-antider(ce,dkh-0.5,0.);
}

/* Sample 1D pointwise data onto point wise first derivative */
double complex MQ_sample1DPntDeriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex cesq = ce*ce;
  return cesq*dkh/csqrt(cesq*dkh*dkh+1.);
}

/* Sample 1D pointwise data onto pointwise second derivative */
double complex MQ_sample1DPntSecDeriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  double complex cesq=ce*ce;
  double complex den = cesq*dkh*dkh+1.;
  return cesq/csqrt(den*den*den);
}

/* Sample 1D cell-averaged data onto pointwise first derivative */
double complex MQ_sample1DAv2Deriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  return MQ_pntCov(ce,dkh+0.5,0.)-MQ_pntCov(ce,dkh-0.5,0.);
}

/* Sample 1D cell-averaged data onto pointwise second derivative */
double complex MQ_sample1DAv2SecDeriv(double complex ce, double dkh, double unused)
{
  (void) unused;
  return MQ_sample1DPntDeriv(ce,dkh+0.5,0.)-MQ_sample1DPntDeriv(ce,dkh-0.5,0.);
}

/* 2D sample functions, separability of MQ kernel makes these simple */
/* Sample 2D cell-averages onto pointwise predictions */
double complex MQ_sample2DAv2Pnt(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Pnt(ce,xdkh,0.)*MQ_sample1DAv2Pnt(ce,ydkh,0.);
}

/* Sample 2D cell-averages onto pointwise derivative in x */
double complex MQ_sample2DAv2DerivX(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Deriv(ce,xdkh,0.)*MQ_sample1DAv2Pnt(ce,ydkh,0.);
}

/* Sample 2D cell-averages onto pointwise derivative in y */
double complex MQ_sample2DAv2DerivY(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Pnt(ce,xdkh,0.)*MQ_sample1DAv2Deriv(ce,ydkh,0.);
}

/* Sample 2D cell-averages onto pointwise second derivative in x */
double complex MQ_sample2DAv2SecDerivXX(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2SecDeriv(ce,xdkh,0.)*MQ_sample1DAv2Pnt(ce,ydkh,0.);
}

/* Sample 2D cell-averages onto pointwise second derivative in y */
double complex MQ_sample2DAv2SecDerivYY(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Pnt(ce,xdkh,0.)*MQ_sample1DAv2SecDeriv(ce,ydkh,0.);
}

/* Sample 2D cell-averages onto pointwise second mixed derivative */
double complex MQ_sample2DAv2SecDerivXY(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Deriv(ce,xdkh,0.)*MQ_sample1DAv2Deriv(ce,ydkh,0.);
}

/* Specialized sample functions */
/* Electric field */
double complex MQ_sampleElecX(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Pnt(ce,xdkh,0.)*(MQ_pntCov(ce,ydkh-0.5,0.) - MQ_pntCov(ce,ydkh+0.5,0.));
}

double complex MQ_sampleElecY(double complex ce, double xdkh, double ydkh)
{
  return MQ_sample1DAv2Pnt(ce,ydkh,0.)*(MQ_pntCov(ce,xdkh+0.5,0.) - MQ_pntCov(ce,xdkh-0.5,0.));
}
