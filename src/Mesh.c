/* File: Mesh.c
 * Author: Ian May
 * Purpose: Define mesh data structures suitable for GP-WENO FVM methods
 * Notes: The dynamic approach here is overkill for a single uniform grid,
 *        but will allow easier extension to AMR stuff later. At least, it
 *        will enforce the right type of interface.
 */

#include "Mesh.h"

/* Allocate mesh and copy all settings */
void CreateMesh(int a_nX, int a_nY, int a_nHalo,
                double a_xLo, double a_xUp, double a_yLo, double a_yUp,
                BCType a_bcNorth, BCType a_bcEast, BCType a_bcSouth, BCType a_bcWest,
                BCType *a_bcNSub, BCType *a_bcESub, BCType *a_bcSSub,BCType *a_bcWSub,
                double a_bcNTr, double a_bcETr, double a_bcSTr, double a_bcWTr,
                Mesh *a_mesh)
{
  Mesh mesh = malloc(sizeof(*mesh));
  mesh->nX = a_nX;
  mesh->nY = a_nY;
  mesh->nHalo = a_nHalo;
  mesh->nXT = mesh->nX+2*mesh->nHalo;
  mesh->nInt = mesh->nX*mesh->nY;
  mesh->nTot = (mesh->nX+2*mesh->nHalo)*(mesh->nY+2*mesh->nHalo);
  mesh->xLo = a_xLo;
  mesh->xUp = a_xUp;
  mesh->yLo = a_yLo;
  mesh->yUp = a_yUp;
  mesh->dx = (mesh->xUp-mesh->xLo)/((double) mesh->nX);
  mesh->dy = (mesh->yUp-mesh->yLo)/((double) mesh->nY);
  mesh->bcWest = a_bcWest;
  mesh->bcEast = a_bcEast;
  mesh->bcNorth = a_bcNorth;
  mesh->bcSouth = a_bcSouth;
  /* Room for mixed BCs */
  mesh->bcWestSub[0] = a_bcWSub[0];
  mesh->bcWestSub[1] = a_bcWSub[1];
  mesh->bcEastSub[0] = a_bcESub[0];
  mesh->bcEastSub[1] = a_bcESub[1];
  mesh->bcNorthSub[0] = a_bcNSub[0];
  mesh->bcNorthSub[1] = a_bcNSub[1];
  mesh->bcSouthSub[0] = a_bcSSub[0];
  mesh->bcSouthSub[1] = a_bcSSub[1];
  /* Transition points for mixed BCs */
  mesh->bcWestTr = a_bcWTr;
  mesh->bcEastTr = a_bcETr;
  mesh->bcSouthTr = a_bcSTr;
  mesh->bcNorthTr = a_bcNTr;

  *a_mesh = mesh;
}

void DestroyMesh(Mesh *a_mesh)
{
  /* Pretty trivial for now */
  if(*a_mesh) { free(*a_mesh); }
}
