/* File: Riemann.c
 * Author: Ian May
 * Purpose: Hold various Riemann solvers and helper functions
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "Riemann.h"
#include "EOS.h"
#include "definition.h"

#define MAX(a,b) ((a)<(b) ? (b) : (a))
#define FABMAX(a,b) (fabs(a)<fabs(b) ? fabs(b) : fabs(a))
#define MIN(a,b) ((a)>(b) ? (b) : (a))
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Advection equation */
double ADV_ExactX(double *fProp, double *uL, double *uR, double *F)
{
  (void) fProp;
  (void) uR;
  F[0] = uL[0];
  return 1.;
}

double ADV_ExactY(double *fProp, double *uL, double *uR, double *F)
{
  (void) fProp;
  (void) uR;
  (void) uL;
  F[0] = 0.;
  return 0.;
}

/* Euler flux function */
static void eulerFluxX(double *fProp, double *u, double *F)
{
  const double p = igl_pres(fProp[PROP_GAM],u);
  F[VAR_DENS] = u[VAR_MOMX];                             /* rho u */
  F[VAR_MOMX] = u[VAR_MOMX]*u[VAR_MOMX]/u[VAR_DENS] + p; /* rho uu + p */
  F[VAR_MOMY] = u[VAR_MOMX]*u[VAR_MOMY]/u[VAR_DENS];     /* rho uv */
  F[VAR_MOMZ] = u[VAR_MOMX]*u[VAR_MOMZ]/u[VAR_DENS];     /* rho uw */
  F[VAR_ETOT] = u[VAR_MOMX]*(u[VAR_ETOT]+p)/u[VAR_DENS]; /* u(E+p) */
}

static void eulerFluxY(double *fProp, double *u, double *F)
{
  const double p = igl_pres(fProp[PROP_GAM],u);
  F[VAR_DENS] = u[VAR_MOMY];                             /* rho v */
  F[VAR_MOMX] = u[VAR_MOMY]*u[VAR_MOMX]/u[VAR_DENS];     /* rho uv */
  F[VAR_MOMY] = u[VAR_MOMY]*u[VAR_MOMY]/u[VAR_DENS] + p; /* rho vv + p */
  F[VAR_MOMZ] = u[VAR_MOMY]*u[VAR_MOMZ]/u[VAR_DENS];     /* rho vw */
  F[VAR_ETOT] = u[VAR_MOMY]*(u[VAR_ETOT]+p)/u[VAR_DENS]; /* v(E+p) */
}

/* Euler equation speeds and helpers */
static inline double roeAvg(double rhoL, double rhoR, double qL, double qR)
{
  return (qL*sqrt(rhoL)+qR*sqrt(rhoR))/(sqrt(rhoL)+sqrt(rhoR));
}

static inline void roeSpeedX(double *uL, double *uR, double *fProp, double pL, double pR, double *sL, double *sR)
{
  double uTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMX]/uL[VAR_DENS],uR[VAR_MOMX]/uR[VAR_DENS]);
  double vTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMY]/uL[VAR_DENS],uR[VAR_MOMY]/uR[VAR_DENS]);
  double wTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMZ]/uL[VAR_DENS],uR[VAR_MOMZ]/uR[VAR_DENS]);
  double hTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],(uL[VAR_ETOT]+pL)/uL[VAR_DENS],(uR[VAR_ETOT]+pR)/uR[VAR_DENS]);
  double aTil = sqrt((fProp[PROP_GAM]-1)*(hTil-((vTil*vTil+wTil*wTil) + uTil*uTil)/2.));
  *sL = uTil - aTil;
  *sR = uTil + aTil;
}

static inline void roeSpeedY(double *uL, double *uR, double *fProp, double pL, double pR, double *sL, double *sR)
{
  double uTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMX]/uL[VAR_DENS],uR[VAR_MOMX]/uR[VAR_DENS]);
  double vTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMY]/uL[VAR_DENS],uR[VAR_MOMY]/uR[VAR_DENS]);
  double wTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMZ]/uL[VAR_DENS],uR[VAR_MOMZ]/uR[VAR_DENS]);
  double hTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],(uL[VAR_ETOT]+pL)/uL[VAR_DENS],(uR[VAR_ETOT]+pR)/uR[VAR_DENS]);
  double aTil = sqrt((fProp[PROP_GAM]-1)*(hTil-((uTil*uTil+wTil*wTil) + vTil*vTil)/2.));
  *sL = vTil - aTil;
  *sR = vTil + aTil;
}

static inline void einfSpeedX(double *uL, double *uR, double *fProp, double pL, double pR, double *sL, double *sR)
{
  double uTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],uL[VAR_MOMX]/uL[VAR_DENS],uR[VAR_MOMX]/uR[VAR_DENS]);
  double aaL=fProp[PROP_GAM]*pL/uL[VAR_DENS], aaR=fProp[PROP_GAM]*pR/uR[VAR_DENS];
  double aTil = roeAvg(uL[VAR_DENS],uR[VAR_DENS],aaL,aaR);
  double eta = sqrt(uL[VAR_DENS])*sqrt(uR[VAR_DENS])/(2.*pow(sqrt(uL[VAR_DENS])+sqrt(uR[VAR_DENS]),2));
  double dd = aTil + eta*pow(uR[VAR_MOMX]/uR[VAR_DENS]-uL[VAR_MOMX]/uL[VAR_DENS],2);
  *sL = uTil - sqrt(dd);
  *sR = uTil + sqrt(dd);
}

static inline void maxSpeedX(double *uL, double *uR, double *fProp, double pL, double pR, double *sL, double *sR)
{
  double a=MAX(sqrt(fProp[PROP_GAM]*pL/uL[VAR_DENS]),sqrt(fProp[PROP_GAM]*pR/uR[VAR_DENS]));
  *sL = MIN(uL[VAR_MOMX]/uL[VAR_DENS],uR[VAR_MOMX]/uR[VAR_DENS]);
  *sR = MAX(uL[VAR_MOMX]/uL[VAR_DENS],uR[VAR_MOMX]/uR[VAR_DENS]);
  *sL -= a;
  *sR += a;
}

static inline void maxSpeedY(double *uL, double *uR, double *fProp, double pL, double pR, double *sL, double *sR)
{
  double a=MAX(sqrt(fProp[PROP_GAM]*pL/uL[VAR_DENS]),sqrt(fProp[PROP_GAM]*pR/uR[VAR_DENS]));
  *sL = MIN(uL[VAR_MOMY]/uL[VAR_DENS],uR[VAR_MOMY]/uR[VAR_DENS]);
  *sR = MAX(uL[VAR_MOMY]/uL[VAR_DENS],uR[VAR_MOMY]/uR[VAR_DENS]);
  *sL -= a;
  *sR += a;
}

double RS_EulerLLFX(double *fProp, double *uL, double *uR, double *F)
{
  double FT[8];
  double pL = igl_pres(fProp[PROP_GAM],uL);
  double pR = igl_pres(fProp[PROP_GAM],uR);
  /* Wave speed estimates */
  double sL, sR, alpha;
  maxSpeedX(uL,uR,fProp,pL,pR,&sL,&sR);
  alpha = MAX(fabs(sL),fabs(sR));
  /* Evaluate left and right fluxes */
  eulerFluxX(fProp,uL,F);
  eulerFluxX(fProp,uR,FT);
  for(int nC=VAR_DENS; nC<VAR_ETOT; nC++) {
    F[nC] = (F[nC]+FT[nC])/2. - alpha*(uR[nC]-uL[nC])/2.;
  }
  return alpha;
}

double RS_EulerLLFY(double *fProp, double *uL, double *uR, double *F)
{
  double FT[8];
  double pL = igl_pres(fProp[PROP_GAM],uL);
  double pR = igl_pres(fProp[PROP_GAM],uR);
  /* Wave speed estimates */
  double sL, sR, alpha;
  maxSpeedY(uL,uR,fProp,pL,pR,&sL,&sR);
  alpha = MAX(fabs(sL),fabs(sR));
  /* Evaluate left and right fluxes */
  eulerFluxY(fProp,uL,F);
  eulerFluxY(fProp,uR,FT);
  for(int nC=VAR_DENS; nC<VAR_ETOT; nC++) {
    F[nC] = (F[nC]+FT[nC])/2. - alpha*(uR[nC]-uL[nC])/2.;
  }
  return alpha;
}

static inline double hll(double sL, double sR, double fL, double fR, double uL, double uR)
{
  return ((sR*fL - sL*fR) + sL*sR*(uR-uL))/(sR-sL);
}

double RS_EulerHLLX(double *fProp, double *uL, double *uR, double *F)
{
  double FT[8];
  double pL = igl_pres(fProp[PROP_GAM],uL);
  double pR = igl_pres(fProp[PROP_GAM],uR);
  /* Wave speed estimates */
  double sL, sR;
  maxSpeedX(uL,uR,fProp,pL,pR,&sL,&sR);
  /* Find appropriate region and fill in flux vector */
  if(sR>0) {
    eulerFluxX(fProp,uL,F);
    if(sL<0) {
      eulerFluxX(fProp,uR,FT);
      F[VAR_DENS] = hll(sL,sR,F[VAR_DENS],FT[VAR_DENS],uL[VAR_DENS],uR[VAR_DENS]);
      F[VAR_MOMX] = hll(sL,sR,F[VAR_MOMX],FT[VAR_MOMX],uL[VAR_MOMX],uR[VAR_MOMX]);
      F[VAR_MOMY] = hll(sL,sR,F[VAR_MOMY],FT[VAR_MOMY],uL[VAR_MOMY],uR[VAR_MOMY]);
      F[VAR_MOMZ] = hll(sL,sR,F[VAR_MOMZ],FT[VAR_MOMZ],uL[VAR_MOMZ],uR[VAR_MOMZ]);
      F[VAR_ETOT] = hll(sL,sR,F[VAR_ETOT],FT[VAR_ETOT],uL[VAR_ETOT],uR[VAR_ETOT]);
    }
  } else {
    eulerFluxX(fProp,uR,F);
  }

  return MAX(fabs(sL),fabs(sR));
}


double RS_EulerHLLY(double *fProp, double *uL, double *uR, double *F)
{
  double FT[8] = {0.,0.,0.,0.,0.,0.,0.,0.};
  double pL = igl_pres(fProp[PROP_GAM],uL);
  double pR = igl_pres(fProp[PROP_GAM],uR);
  /* Wave speed estimates */
  double sL, sR;
  maxSpeedY(uL,uR,fProp,pL,pR,&sL,&sR);
  /* Find appropriate region and fill in flux vector */
  if(sR>0) {
    eulerFluxY(fProp,uL,F);
    if(sL<0) {
      eulerFluxY(fProp,uR,FT);
      F[VAR_DENS] = hll(sL,sR,F[VAR_DENS],FT[VAR_DENS],uL[VAR_DENS],uR[VAR_DENS]);
      F[VAR_MOMX] = hll(sL,sR,F[VAR_MOMX],FT[VAR_MOMX],uL[VAR_MOMX],uR[VAR_MOMX]);
      F[VAR_MOMY] = hll(sL,sR,F[VAR_MOMY],FT[VAR_MOMY],uL[VAR_MOMY],uR[VAR_MOMY]);
      F[VAR_MOMZ] = hll(sL,sR,F[VAR_MOMZ],FT[VAR_MOMZ],uL[VAR_MOMZ],uR[VAR_MOMZ]);
      F[VAR_ETOT] = hll(sL,sR,F[VAR_ETOT],FT[VAR_ETOT],uL[VAR_ETOT],uR[VAR_ETOT]);
    }
  } else {
    eulerFluxY(fProp,uR,F);
  }
  
  return MAX(fabs(sL),fabs(sR));
}

static inline double hllcSStar(double rhoL, double rhoR, double pL, double pR,
                               double uL, double uR, double sL, double sR)
{
  return ((pR-pL) + (rhoL*uL*(sL-uL) - rhoR*uR*(sR-uR)))/(rhoL*(sL-uL) - rhoR*(sR-uR));
}

static inline double hllcPre(double rho, double u, double s, double sStar)
{
  return rho*(s-u)/(s-sStar);
}

static inline double hllcEnergy(double pre, double rhoK, double EK, double pK, double uK, double sK, double sS)
{
  return pre*(EK/rhoK + (sS-uK)*(sS + pK/(rhoK*(sK-uK)) ) );
}

static inline double hllc(double sK, double uK, double fK, double uS)
{
  return fK + sK*(uS-uK);
}

double RS_EulerHLLCX(double *fProp, double *uL, double *uR, double *F)
{
  double pL = igl_pres(fProp[PROP_GAM],uL);
  double pR = igl_pres(fProp[PROP_GAM],uR);
  /* Wave speed estimates */
  double sL, sR;
  maxSpeedX(uL,uR,fProp,pL,pR,&sL,&sR);
  double sS = hllcSStar(uL[VAR_DENS],uR[VAR_DENS],pL,pR,
                        uL[VAR_MOMX]/uL[VAR_DENS],uR[VAR_MOMX]/uR[VAR_DENS],sL,sR);

  if(0<=sL) {
    eulerFluxX(fProp,uL,F);
  } else if(0<=sS) {
    double pre = hllcPre(uL[VAR_DENS], uL[VAR_MOMX]/uL[VAR_DENS], sL, sS);
    eulerFluxX(fProp,uL,F);
    F[VAR_DENS] += sL*(pre - uL[VAR_DENS]);
    F[VAR_MOMX] += sL*(pre*sS - uL[VAR_MOMX]);
    F[VAR_MOMY] += sL*(pre*uL[VAR_MOMY]/uL[VAR_DENS] - uL[VAR_MOMY]);
    F[VAR_MOMZ] += sL*(pre*uL[VAR_MOMZ]/uL[VAR_DENS] - uL[VAR_MOMZ]);
    double eStar = hllcEnergy(pre,uL[VAR_DENS],uL[VAR_ETOT],pL,
                              uL[VAR_MOMX]/uL[VAR_DENS],sL,sS);
    F[VAR_ETOT] += sL*(eStar - uL[VAR_ETOT]);
  } else if(0<sR) {
    double pre = hllcPre(uR[VAR_DENS], uR[VAR_MOMX]/uR[VAR_DENS], sR, sS);
    eulerFluxX(fProp,uR,F);
    F[VAR_DENS] += sR*(pre - uR[VAR_DENS]);
    F[VAR_MOMX] += sR*(pre*sS - uR[VAR_MOMX]);
    F[VAR_MOMY] += sR*(pre*uR[VAR_MOMY]/uR[VAR_DENS] - uR[VAR_MOMY]);
    F[VAR_MOMZ] += sR*(pre*uR[VAR_MOMZ]/uR[VAR_DENS] - uR[VAR_MOMZ]);
    double eStar = hllcEnergy(pre,uR[VAR_DENS],uR[VAR_ETOT],pR,
                              uR[VAR_MOMX]/uR[VAR_DENS],sR,sS);
    F[VAR_ETOT] += sR*(eStar - uR[VAR_ETOT]);
  } else { /* sR<0 */
    eulerFluxX(fProp,uR,F);
  }
  
  return MAX(fabs(sL),fabs(sR));
}

double RS_EulerHLLCY(double *fProp, double *uL, double *uR, double *F)
{
  double pL = igl_pres(fProp[PROP_GAM],uL);
  double pR = igl_pres(fProp[PROP_GAM],uR);
  /* Wave speed estimates */
  double sL, sR;
  maxSpeedY(uL,uR,fProp,pL,pR,&sL,&sR);
  double sS = hllcSStar(uL[VAR_DENS],uR[VAR_DENS],pL,pR,
                        uL[VAR_MOMY]/uL[VAR_DENS],uR[VAR_MOMY]/uR[VAR_DENS],sL,sR);

  if(0<=sL) {
    eulerFluxY(fProp,uL,F);
  } else if(0<=sS) {
    double pre = hllcPre(uL[VAR_DENS], uL[VAR_MOMY]/uL[VAR_DENS], sL, sS);
    eulerFluxY(fProp,uL,F);
    F[VAR_DENS] += sL*(pre - uL[VAR_DENS]);
    F[VAR_MOMX] += sL*(pre*uL[VAR_MOMX]/uL[VAR_DENS] - uL[VAR_MOMX]);
    F[VAR_MOMY] += sL*(pre*sS - uL[VAR_MOMY]);
    F[VAR_MOMZ] += sL*(pre*uL[VAR_MOMZ]/uL[VAR_DENS] - uL[VAR_MOMZ]);
    double eStar = hllcEnergy(pre,uL[VAR_DENS],uL[VAR_ETOT],pL,
                              uL[VAR_MOMY]/uL[VAR_DENS],sL,sS);
    F[VAR_ETOT] += sL*(eStar - uL[VAR_ETOT]);
  } else if(0<sR) {
    double pre = hllcPre(uR[VAR_DENS], uR[VAR_MOMY]/uR[VAR_DENS], sR, sS);
    eulerFluxY(fProp,uR,F);
    F[VAR_DENS] += sR*(pre - uR[VAR_DENS]);
    F[VAR_MOMX] += sR*(pre*uR[VAR_MOMX]/uR[VAR_DENS] - uR[VAR_MOMX]);
    F[VAR_MOMY] += sR*(pre*sS - uR[VAR_MOMY]);
    F[VAR_MOMZ] += sR*(pre*uR[VAR_MOMZ]/uR[VAR_DENS] - uR[VAR_MOMZ]);
    double eStar = hllcEnergy(pre,uR[VAR_DENS],uR[VAR_ETOT],pR,
                              uR[VAR_MOMY]/uR[VAR_DENS],sR,sS);
    F[VAR_ETOT] += sR*(eStar - uR[VAR_ETOT]);
  } else { /* sR<0 */
    eulerFluxY(fProp,uR,F);
  }
  return MAX(fabs(sL),fabs(sR));
}
