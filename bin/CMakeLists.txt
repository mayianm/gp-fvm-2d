add_executable(EulerDriver ../src/EulerDriver.c)
target_link_libraries(EulerDriver gpweno hdf5_hl hdf5 z lapack blas ${GSL_LIBRARIES} ${GSLCBLAS_LIBRARIES} cerf m gomp gfortran)
