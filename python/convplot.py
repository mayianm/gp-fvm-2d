import numpy as np
import matplotlib.pyplot as plt
import sys

# Read in data files
args = sys.argv[1:]
nconv = int(len(args)/2)
fnames = args[:nconv]
lstr = args[nconv:]
print(fnames)
print(lstr)

data = []
for fstr in fnames:
    data.append(np.loadtxt(fstr))

# Fit lines and report order of accuracy
l1ord = []
linford = []
l1int = []
linfint = []
for rad,d in zip(lstr,data):
    pf = np.polyfit(np.log10(d[:,0]),np.log10(d[:,3]),1)
    l1ord.append(pf[0])
    l1int.append(pf[1])
    pf = np.polyfit(np.log10(d[:,0]),np.log10(d[:,4]),1)
    linford.append(pf[0])
    linfint.append(pf[1])
    print(rad," L1: ",-l1ord[-1]," Linf: ",-linford[-1])
    print("  L1: ",(np.log10(d[0:-1,3])-np.log10(d[1:,3]))/(np.log10(d[1:,0])-np.log10(d[0:-1,0])))
    print("  Li: ",(np.log10(d[0:-1,4])-np.log10(d[1:,4]))/(np.log10(d[1:,0])-np.log10(d[0:-1,0])))

# Plot conv history and line fits
resx = np.linspace(np.log10(data[0][0,0]),np.log10(data[0][0,-1]),10)
fig,(ax1,axinf) = plt.subplots(1,2)
for rad,d,l1o,l1i,lio,lii in zip(lstr,data,l1ord,l1int,linford,linfint):
    ax1.plot(np.log10(d[:,0]),np.log10(d[:,3]),'-o')
    #ax1.plot(resx,l1i+l1o*resx)
    axinf.plot(np.log10(d[:,0]),np.log10(d[:,4]),'-o')
    #axinf.plot(resx,lii+lio*resx)

ax1.grid('both')
ax1.set(xlabel = r'$\log_{10}(N)$', ylabel = r'$\log_{10}(err)$', title='$L_1$ Error')
ax1.legend(lstr)
axinf.grid('both')
axinf.set(xlabel = r'$\log_{10}(N)$', ylabel = r'$\log_{10}(err)$', title='$L_\infty$ Error')
plt.show()
