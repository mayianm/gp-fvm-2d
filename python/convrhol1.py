import numpy as np
import matplotlib.pyplot as plt
import sys

plt.rc('axes', titlesize=15)
plt.rc('axes', labelsize=15)
plt.rc('xtick', labelsize=15)
plt.rc('ytick', labelsize=15)
plt.rc('legend', fontsize=15)

# Read in data files
args = sys.argv[1:]
nconv = int(len(args)/4)
fnames = args[:nconv]
refOrds = args[nconv:2*nconv]
lstr = args[2*nconv:]
print(fnames)
print(lstr)

data = []
for fstr in fnames:
    data.append(np.loadtxt(fstr))

# Fit lines and report order of accuracy
l1ord = []
l1int = []
for rad,d in zip(lstr,data):
    pf = np.polyfit(np.log10(d[:,0]),np.log10(d[:,3]),1)
    l1ord.append(pf[0])
    l1int.append(pf[1])
    print(rad," L1 fit: ",-l1ord[-1])
    print("  L1: ",(np.log10(d[0:-1,3])-np.log10(d[1:,3]))/(np.log10(d[1:,0])-np.log10(d[0:-1,0])))

# Plot conv history and line fits
resx = np.linspace(np.log10(data[0][0,0]),np.log10(data[0][0,-1]),10)
fig,(ax1) = plt.subplots(1,1)
for rad,d,l1o,refOrd in zip(lstr,data,l1ord,refOrds):
    ax1.plot(np.log10(d[:,0]),np.log10(d[:,3]),'-o')
    ax1.plot(np.log10(d[:,0]),np.log10(d[0,3])-int(refOrd)*np.log10(d[:,0]/d[0,0]),'--k')

ax1.grid('both')
ax1.set(xlabel = r'$\log_{10}(N)$', ylabel = r'$\log_{10}\left(||\rho_N-\rho_{ex}||_1\right)$', title = r'$L_1$ Density error')
ax1.legend(lstr)
plt.show()
